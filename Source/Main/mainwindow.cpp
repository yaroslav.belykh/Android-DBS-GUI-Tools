/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    setLayouts();


    connect(ui->action_AboutQt, SIGNAL(triggered()), this, SLOT(OpenAboutQt()));
    connect(ui->action_About, SIGNAL(triggered()), this, SLOT(OpenAbout()));
    connect(ui->action_Exit, SIGNAL(triggered()), this, SLOT(doExit()));
    connect(ui->action_ConnectADB, SIGNAL(triggered()), this, SLOT(doTcpConnect()));

    AppName="ADBGUITools";
    settings = new QSettings("Yamah-BYV", AppName, this);

    tmpDir.setAutoRemove(true);

#if not defined(Q_OS_WIN)
    if (!settings->value("Debug/adb").toString().isEmpty())
        {doDebug.adbDebug=settings->value("Debug/adb").toBool();}
    else
        {doDebug.adbDebug=true;}
    if (settings->value("Debug/classes").toString().isEmpty())
        {doDebug.classDebug=settings->value("Debug/classe").toBool();}
    else
        {doDebug.classDebug=true;}
#endif

    appProcess = new adbprocess(this);
    getDevInfo = new getInfo(&adbCmd, appProcess, &BaseFunction, this);
    //Instruments = new InstrumentsCL(this);

    defCmd();

    wisibleIndex=-1;

}

MainWindow::~MainWindow()
{
    QStringList exitCMD;
    appProcess->adbCMD(adbCmd.adb.stop,exitCMD);
    delete ui;
}

void MainWindow::OpenAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::OpenAbout()
{
    AboutDialog = new about();
    connect(AboutDialog, SIGNAL(closeThisWindow()), this, SLOT(closeAboutWindow()));
    AboutDialog->show();
}

void MainWindow::doExit()
{
    this->close();
    //QApplication::closeAllWindows();
}


void MainWindow::on_pB_refresLiast_clicked()
{
     ui->DeviceList->clear();
     clearAllStatus();
     getStatusOnClick();
}

void MainWindow::on_DeviceList_currentIndexChanged(int index)
{
    //if (wisibleIndex!=-1)
        //{clearStatus(wisibleIndex);}
    if (index!=-1 && ActiveDevices[index].DevStatus!="unknown" && ActiveDevices[index].DevStatus!="offline")
    {
        if (wisibleIndex!=-1)
//            {makeStatusWigets();}
 //       else
        {
            closeStatus(wisibleIndex);
            showInfo(index);
        }
    }
    else
    {
        ui->tb_Status->setIcon(AndroidIcon.iUSB);
        ui->pB_Fastboot->setEnabled(false);
        ui->pB_Reboot->setEnabled(false);
        ui->pB_Recovery->setEnabled(false);
        ui->pB_Shutdown->setEnabled(false);
        ui->twDevice->setVisible(false);
    }
    wisibleIndex=index;
}

void MainWindow::doTcpConnect()
{
    ConnectADBdDialog = new AdbTcpConnect(this);
    connect(ConnectADBdDialog, SIGNAL(setConnectionAddr(QString)), this, SLOT(doConnectTCP(QString)));
    connect(ConnectADBdDialog, SIGNAL(closeThisWindow()), this, SLOT(closeTCPADBdWindow()));
    ConnectADBdDialog->getConnectedDevices(&tcpConnected);
    ConnectADBdDialog->show();
}

void MainWindow::on_tb_Status_clicked()
{
    int index;
    index=ui->DeviceList->currentIndex();
    if (index!=-1)
        {closeStatus(wisibleIndex);}
    if (ui->DeviceList->currentIndex()!=-1)
    {
        ActiveDevices[index].DevInfo.ProductNames.prod_Board.clear();
        ActiveDevices[index].DevInfo.ProductNames.prod_Board.clear();
        getDevInfo->getNameDevSysInfo(&ActiveDevices[index]);
        if (ActiveDevices[index].DevInfo.ProductNames.prod_Board=="" && ActiveDevices[index].DevInfo.ProductNames.prod_Board=="")
        {
            ui->DeviceList->clear();
            clearAllStatus();
            getStatusOnClick();
        }
        else
        {
            showInfo(index);
        }
    }
    else
    {
        ui->DeviceList->clear();
        clearAllStatus();
        getStatusOnClick();
    }
}

void MainWindow::setLayouts()
{
    ui->centralWidget->setLayout(ui->CentralHorizontalLayout);
    ui->tab_Device_General_Info->setLayout(ui->DeviveGenInfo_VL);
    ui->tab_Device_Battery_Info->setLayout(ui->DeviveBatInfo_VL);
    ui->tab_Device_GSM_Info->setLayout(ui->GSMInfo_VL);
    ui->tab_Device_Network_Info->setLayout(ui->DeviceNetworkInfo_VL);
    ui->tab_Device_Disk_Info->setLayout(ui->DeviveDisknfo_VL);
    ui->tab_Device_HardwareInfo->setLayout(ui->HWInfo_VL);
    ui->tab_Device_Screennshot->setLayout(ui->SSInfo_VL);
    ui->tab_Device_APK_Manager->setLayout(ui->APKMan_VLayout);
    ui->tab_Device_File_Manager->setLayout(ui->DeviceFMInfo_VL);
}

void MainWindow::on_pB_Reboot_clicked()
{
    QStringList empList;
    if (ActiveDevices[ui->DeviceList->currentIndex()].DevStatus!="fastboot")
        {appProcess->adbCMD(adbCmd.adb.reboot, empList);}
    else
        {appProcess->adbCMD(adbCmd.fastboot.reboot, empList);}
    QMessageBox::information(this,QObject::tr("Your devise is rebooting"),tr("Please wait for rebooting your device, and press \"OK\" "));
    changeDeviceStatus(ui->DeviceList->currentIndex(), 20000);
}

inline void MainWindow::changeDeviceStatus(int index, int timerTimeOut, int CallAcction)
{
    timer.setInterval(timerTimeOut);
    switch (CallAcction) {
        case 0:
        {
            removeDeviceFromList(index);
            connect(&timer, SIGNAL(timeout()),this, SLOT(getStatusOnCl()));
        }
        break;
        case 1:
            {connect(&timer, SIGNAL(timeout()),this, SLOT(CallNetCfgInfo()));}
        break;
        default:
            {connect(&timer, SIGNAL(timeout()),this, SLOT(getStatusOnCl()));}
        break;
    }
    timer.setSingleShot(true);
    timer.start();
}

void MainWindow::on_pB_Shutdown_clicked()
{
    QStringList empList;
    appProcess->adbCMD(adbCmd.adb.shutdown, empList);
    QMessageBox::information(this,QObject::tr("Your device is turned off."),tr("Please wait for shutdown your device, and press \"OK\" "));
    changeDeviceStatus(ui->DeviceList->currentIndex(), 15000);
}

void MainWindow::on_pB_Recovery_clicked()
{
    QStringList empList;
    appProcess->adbCMD(adbCmd.adb.rebootRecovery, empList);
    QMessageBox::information(this,QObject::tr("Your device is reboot to recovery."),tr("Please wait for rebooting your device, and press \"OK\" "));
    changeDeviceStatus(ui->DeviceList->currentIndex(), 15000);
}

void MainWindow::on_pB_Fastboot_clicked()
{
    QStringList empList;
    if (ActiveDevices[ui->DeviceList->currentIndex()].DevStatus!="fastboot")
        {appProcess->adbCMD(adbCmd.adb.rebootBootloader, empList);}
    else
        {appProcess->adbCMD(adbCmd.fastboot.rebootBootloader, empList);}
    QMessageBox::information(this,QObject::tr("Your device is reboot to fastboot."),tr("Please wait for rebooting your device, and press \"OK\" "));
    changeDeviceStatus(ui->DeviceList->currentIndex(), 15000);
}

inline void MainWindow::removeDeviceFromList(int index)
{
    int curCountList;
    curCountList=ui->DeviceList->count();
    for (int i=index;i<curCountList;i++)
    {
        DeviceWgxStatistic.remove(i);
        ActiveDevices.remove(i);
        if (i<curCountList-1)
        {
            ActiveDevices[i]=ActiveDevices[i+1];
            DeviceWgxStatistic[i]=DeviceWgxStatistic[i+1];
        }
    }
    clearStatus(index);
    ui->DeviceList->removeItem(index);
    getStatusOnClick();
}
