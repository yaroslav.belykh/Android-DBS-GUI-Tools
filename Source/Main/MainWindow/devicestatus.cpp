/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::getStatusOnClick()
{
    QStringList adbDev;
    QStringList fastbootDev;
    ActiveDevices.clear();
    DeviceWgxStatistic.clear();
    AndroidCountClear();
    adbDev << appProcess->adbCMD(adbCmd.adb.devices,adbDev);
    adbDev.removeOne("List of devices attached ");
    adbDev.removeOne("List of devices attached");
    if (ui->DeviceList->count()>0)
        {adbDev=DeviceStringAdd(adbDev);}
    DeviceStringBuild(adbDev, true);
    adbDev.clear();
    fastbootDev << appProcess->fbCMD(adbCmd.fastboot.devices,adbDev);
    if (ui->DeviceList->count()>0)
        {fastbootDev=DeviceStringAdd(fastbootDev);}
    DeviceStringBuild(fastbootDev, false);
    makeStatusWigets();
    showStatusInfo();
    showInfo(ui->DeviceList->currentIndex());
}

inline void MainWindow::DeviceStringBuild(QStringList str, bool isADB)
{
    int index;
    tcpConnected.clear();
    for (int i=0; i<str.count(); i++)
    {
        QStringList strList;
        index = ui->DeviceList->count();
        strList=str.at(i).split(" ", QString::SkipEmptyParts);
        ActiveDevices[index].DevNumber=strList.at(0);
        ActiveDevices[index].DevStatus=strList.at(1);
        if (ActiveDevices[index].DevStatus!="unauthorized" && ActiveDevices[index].DevStatus!="offline" && ActiveDevices[index].DevStatus!="unknown")
        {
            if (strList.count()>2)
                {ActiveDevices[index].DevPath=strList.at(2);}
            ActiveDevices[index].adbMode=isADB;
            ActiveDevices[index].unicDevNumber=true;
            if (str.count()>1)
            {
                for (int j=0; j<str.count(); j++)
                {
                    if (str.at(j).indexOf(ActiveDevices[index].DevNumber)!=-1 && j!=index)
                        {ActiveDevices[index].unicDevNumber=false;}
                }
            }
            getDevInfo->getNameDevSysInfo(&ActiveDevices[index]);
            QString VisName;
            if (ActiveDevices[index].DevStatus!="fastboot")
            {
                VisName=ActiveDevices[index].DevInfo.ProductNames.prod_Brand;
                VisName.append(" ");
                VisName.append(ActiveDevices[index].DevInfo.ProductNames.prod_Device);
                if (ActiveDevices[index].DevStatus=="recovery" && ActiveDevices[index].DevInfo.ProductNames.prod_Device=="")
                {
                    VisName="Recovery ";
                    VisName.append(ActiveDevices[index].DevNumber);
                }
                /*DeviceWgxStatistic[index].inInfoAPKUpdate=false;
                DeviceWgxStatistic[index].TotallyPackagesSpace=0;
                DeviceWgxStatistic[index].usedHiddenPacckahesSpace=0;
                DeviceWgxStatistic[index].UpdSystem=0;
                DeviceWgxStatistic[index].intOK=false;*/
                if (ActiveDevices[index].DevStatus!="recovery")
                    {androidCount.deviceCount++;}
                else
                    {androidCount.recoveryCount++;}
            }
            else
            {
                if (ActiveDevices[index].DevFBInfo.Product!="")
                    {VisName=ActiveDevices[index].DevFBInfo.Product;}
                else
                {
                    VisName="FastBoot ";
                    VisName.append(ActiveDevices[index].DevNumber);
                }
                androidCount.fastbootCount++;
            }
            if (ActiveDevices[index].DevPath!="" || ActiveDevices[index].unicDevNumber)
                {DeviceWgxStatistic[index].tmpDir=ActiveDevices[index].DevNumber;}
            else
            {
                QString tmpStr;
                QStringList tmpStrLst;
                if (!ActiveDevices[index].unicDevNumber)
                    {tmpStrLst=ActiveDevices[index].DevPath.split(":",QString::SkipEmptyParts);}
                else
                    {tmpStrLst=ActiveDevices[index].DevNumber.split(":",QString::SkipEmptyParts);}
                for (int m=0;m<tmpStrLst.count();m++)
                {
                    tmpStr.append(tmpStrLst.at(m));
                    if (m<tmpStrLst.count()-1)
                        {tmpStr.append("_");}
                }
                DeviceWgxStatistic[index].tmpDir=tmpStr;
            }
            if (ActiveDevices[index].DevPath!="" && ActiveDevices[index].DevNumber.indexOf(":")!=-1)
            {
                tcpConnected.append(ActiveDevices[index].DevNumber.split(":",QString::SkipEmptyParts).first());
                ActiveDevices[index].overTCP=true;
                androidCount.tcpCount++;
            }
            else
            {
                if (ActiveDevices[index].DevNumber.indexOf("emulat")==0)
                    {androidCount.emulatorCount++;}
                else
                    {androidCount.usbCount++;}
                ActiveDevices[index].overTCP=false;
            }
            ui->DeviceList->addItem(VisName);
        }
        else
        {
            if (ActiveDevices[index].DevStatus=="unauthorized")
            {
                QMessageBox::warning(this,QObject::tr("This PC was not authorized for debug from you device!"),tr("Please authorize this computer for debugging on your device."));
                androidCount.unauthorizedCount++;
            }
            if (ActiveDevices[index].DevStatus=="offline")
            {
                QMessageBox::warning(this,QObject::tr("Your device status is offline!"),tr("Please reboot you devioce."));
                androidCount.offlineCount++;
            }
        }
        androidCount.allCount++;
    }
}

inline QStringList MainWindow::DeviceStringAdd(QStringList intLst)
{
    QList<int> tmpADList;
    tmpADList=ActiveDevices.keys();
    for (int i=0;i<tmpADList.count();i++)
    {
        if (ActiveDevices[i].DevStatus!="unauthorized")
        {
            for (int j;j<intLst.count();j++)
            {
                if (ActiveDevices[i].unicDevNumber)
                {
                    if (intLst.at(j).indexOf(ActiveDevices[i].DevNumber)!=-1)
                    {
                        intLst.removeAt(j);
                        j=intLst.count()+1;
                    }
                }
                else
                {
                    if (intLst.at(j).indexOf(ActiveDevices[i].DevPath)!=-1)
                    {
                        intLst.removeAt(j);
                        j=intLst.count()+1;
                    }
                }
            }
        }
    }
    return(intLst);
}

void MainWindow::doConnectTCP(QString tcpipAddress)
{
    QStringList param;
    param << tcpipAddress;
    param=appProcess->adbCMD(adbCmd.adb.connectADB, param);
    if (param.count()>0)
    {
        if (param.first().indexOf("connected to")!=-1)
        {
            QMessageBox::information(this,QObject::tr("The connection is successfully established!"),tr("Connected to ")+tcpipAddress);
            getStatusOnClick();
        }
        else
            {QMessageBox::warning(this,QObject::tr("The connection failed"),tr("Unable to connect to ")+tcpipAddress+"\nPerhaps the device is not allowed to debug from this PC or ADBd can not accept the connection.\nTry restarting WIFI and debugging on device.");}
    }
}

void MainWindow::showStatusInfo()
{
    StatusBarCount[tr("All")]->changeValue(QVariant(androidCount.allCount).toString());
    StatusBarCount[tr("USB")]->changeValue(QVariant(androidCount.usbCount).toString());
    StatusBarCount[tr("TCP/IP")]->changeValue(QVariant(androidCount.tcpCount).toString());
    StatusBarCount[tr("Emulator")]->changeValue(QVariant(androidCount.emulatorCount).toString());
    StatusBarCount[tr("Offline")]->changeValue(QVariant(androidCount.offlineCount).toString());
    StatusBarCount[tr("Unknown")]->changeValue(QVariant(androidCount.unknownCount).toString());
    StatusBarCount[tr("Unauthorized")]->changeValue(QVariant(androidCount.unauthorizedCount).toString());
    StatusBarCount[tr("Device")]->changeValue(QVariant(androidCount.deviceCount).toString());
    StatusBarCount[tr("Recovery")]->changeValue(QVariant(androidCount.recoveryCount).toString());
    StatusBarCount[tr("Fastboot")]->changeValue(QVariant(androidCount.fastbootCount).toString());
}
