/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::makeStatusWigets()
{
    QList<int> deviceList;
    deviceList=ActiveDevices.keys();
    for (int i=0;i<deviceList.count();i++)
    {   
        if (!ActiveDevices[deviceList.at(i)].overTCP)
            {ActiveDevices[deviceList.at(i)].TempPath=ActiveDevices[deviceList.at(i)].DevStatus+"_"+ActiveDevices[deviceList.at(i)].DevNumber+"_"+ActiveDevices[deviceList.at(i)].DevPath;}
        else
            {ActiveDevices[deviceList.at(i)].TempPath=ActiveDevices[deviceList.at(i)].DevStatus+"_"+ActiveDevices[deviceList.at(i)].DevNumber;}
        if (!ActiveDevices[deviceList.at(i)].TempPath.isEmpty())
        {
            QDir tempDir;
            tempDir.setPath(tmpDir.path()+"/"+ActiveDevices[deviceList.at(i)].TempPath);
            if (!tempDir.exists())
                {tempDir.mkpath(tempDir.path());}
        }
        if (ActiveDevices[deviceList.at(i)].DevStatus=="device" || ActiveDevices[deviceList.at(i)].DevStatus=="recovery")
        {
            getDevInfo->getNameDevSysInfo(&ActiveDevices[deviceList.at(i)]);

            InfoWGX[deviceList.at(i)].giWGX = new BaseScrollWigetVL(this);
            InfoWGX[deviceList.at(i)].biWGX = new BatteryInformation(this);
            InfoWGX[deviceList.at(i)].diWGX = new BaseScrollWidgetGV(this);
            InfoWGX[deviceList.at(i)].hiWGX = new BaseScrollWigetVL(this);
            InfoWGX[deviceList.at(i)].ssWGX = new Screenshot(this);
            InfoWGX[deviceList.at(i)].fmWGX = new FileManager(this);

            InfoWGX[deviceList.at(i)].giWGX->setObjectName(QStringLiteral("GeneralInformation_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].biWGX->setObjectName(QStringLiteral("BatteryInformation_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].diWGX->setObjectName(QStringLiteral("DiskInfo_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].hiWGX->setObjectName(QStringLiteral("HardwarelInformation_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].ssWGX->setObjectName(QStringLiteral("Screenshot_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].fmWGX->setObjectName(QStringLiteral("FileManager_")+QVariant(deviceList.at(i)).toString());

            makeWgxGInfo(InfoWGX[deviceList.at(i)].giWGX, &ActiveDevices[deviceList.at(i)].DevInfo);
            makeWgxBInfo(InfoWGX[deviceList.at(i)].biWGX, &ActiveDevices[deviceList.at(i)]);
            makeWgxHInfo(InfoWGX[deviceList.at(i)].hiWGX, &ActiveDevices[deviceList.at(i)]);
            makeWgxScrShot(InfoWGX[deviceList.at(i)].ssWGX, &ActiveDevices[deviceList.at(i)]);

        }
        if (ActiveDevices[deviceList.at(i)].DevStatus=="device")
        {
            InfoWGX[deviceList.at(i)].niWGX = new NetInformation(this);
            InfoWGX[deviceList.at(i)].SimCountWGX = new simCount(this);
            InfoWGX[deviceList.at(i)].SimInfoWGX = new BaseScrollWigetVL(this);
            InfoWGX[deviceList.at(i)].alWGX = new apkList(this);

            InfoWGX[deviceList.at(i)].niWGX->setObjectName(QStringLiteral("netInfoWGX_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].SimCountWGX->setObjectName(QStringLiteral("SimCountWGX_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].SimInfoWGX->setObjectName(QStringLiteral("SimInfoWGX_")+QVariant(deviceList.at(i)).toString());
            InfoWGX[deviceList.at(i)].alWGX->setObjectName(QStringLiteral("APKList_")+QVariant(deviceList.at(i)).toString());

            makeWgxNInfo(InfoWGX[deviceList.at(i)].niWGX, &ActiveDevices[deviceList.at(i)]);
            makeWgxSim(&ActiveDevices[deviceList.at(i)].HWInfo.SimCount, &ActiveDevices[deviceList.at(i)].Gsm, InfoWGX[deviceList.at(i)].SimCountWGX, InfoWGX[deviceList.at(i)].SimInfoWGX);
            makeWgxApkInfo(InfoWGX[deviceList.at(i)].alWGX, deviceList.at(i), &ActiveDevices[deviceList.at(i)]);
            connect(InfoWGX[deviceList.at(i)].alWGX, SIGNAL(updateDeviceInfo(int)),this, SLOT(updateDeviceInfo(int)));
        }
    }
}

inline void MainWindow::makeWgxGInfo(BaseScrollWigetVL *giWGX, TSysInfo *ActiveDevice)
{
    giWGX->addItem(tr("Brand"), ActiveDevice->ProductNames.prod_Manufacture);
    giWGX->addItem(tr("Model"), ActiveDevice->ProductNames.prod_Model);
    giWGX->addItem(tr("Android Version"), ActiveDevice->ProductNames.build_Version);
    giWGX->addItem(tr("Custom Number"),ActiveDevice->ProductNames.build_CustomBuildVerion);
    giWGX->addItem(tr("Serial Number"),ActiveDevice->ProductNames.SN);
    giWGX->addItem(tr("Firmware Revision"),ActiveDevice->ProductNames.GSMVersionBB);
    if (!ActiveDevice->ProductNames.build_hwVersion.isEmpty())
        {giWGX->addItem(tr("Hardware Revision"),ActiveDevice->ProductNames.build_hwVersion);}
    else
    {
        if (!ActiveDevice->ProductNames.build_SKU.isEmpty() && ActiveDevice->ProductNames.build_PDA.isEmpty())
            {giWGX->addItem(tr("Hardware Revision"),ActiveDevice->ProductNames.build_SKU);}
        else
        {
            if (ActiveDevice->ProductNames.build_SKU.isEmpty() && !ActiveDevice->ProductNames.build_PDA.isEmpty())
                {giWGX->addItem(tr("Hardware Revision"),ActiveDevice->ProductNames.build_PDA);}
        }
    }
    giWGX->addItem(tr("Build Revision"),ActiveDevice->ProductNames.build_DisplayID);
    giWGX->addItem(tr("GSM Serial Number"),ActiveDevice->ProductNames.GSM_SN);
    giWGX->addItem(tr("Boot Number"),ActiveDevice->ProductNames.boot_SN);
    if (!ActiveDevice->ProductNames.device_SN.isEmpty() && ActiveDevice->ProductNames.ril_sn.isEmpty())
        {giWGX->addItem(tr("Code Serial Number"),ActiveDevice->ProductNames.device_SN);}
    else
    {
        if (ActiveDevice->ProductNames.device_SN.isEmpty() && !ActiveDevice->ProductNames.ril_sn.isEmpty())
            {giWGX->addItem(tr("Code Serial Number"),ActiveDevice->ProductNames.ril_sn);}
    }
    if (ActiveDevice->ProductNames.build_VersionIncremental!=ActiveDevice->ProductNames.build_PDA && ActiveDevice->ProductNames.build_VersionIncremental!=ActiveDevice->ProductNames.prod_Brand)
        {giWGX->addItem(tr("Incremental Version"),ActiveDevice->ProductNames.build_VersionIncremental);}
    giWGX->addItem(tr("Manufacturer"),ActiveDevice->ProductNames.prod_Brand);
    giWGX->addItem(tr("Device"),ActiveDevice->ProductNames.prod_Device);
    giWGX->addItem(tr("Name"),ActiveDevice->ProductNames.prod_Name);
    giWGX->addItem(tr("Board"),ActiveDevice->ProductNames.prod_Board);
    giWGX->addItem(tr("Product"),ActiveDevice->ProductNames.build_Product);
    giWGX->addItem(tr("Type"),ActiveDevice->ProductNames.build_Type);
    giWGX->addItem(tr("Local Region"),ActiveDevice->ProductNames.prod_Region);
    giWGX->addItem(tr("Local Lenguage"),ActiveDevice->ProductNames.prod_Language);
    giWGX->addCloseItem();
}

void MainWindow::makeWgxBInfo(BatteryInformation *biWGX, TSummaryInfo *ActiveDevice)
{
    biWGX->setObjectParam(ActiveDevice, getDevInfo);
}

void MainWindow::makeWgxNInfo(NetInformation *niWGX, TSummaryInfo *ActiveDevice)
{
    niWGX->setObjectParam(ActiveDevice, getDevInfo, &BaseFunction);
}

void MainWindow::makeWgxHInfo(BaseScrollWigetVL *hiWGX, TSummaryInfo *ActiveDevice)
{
    for (int i=1;i<=ActiveDevice->HWInfo.SimCount;i++)
    {
        QString imeiName;
        if (ActiveDevice->HWInfo.SimCount>1)
            {imeiName="IMEI "+QVariant(i).toString();}
        else
            {imeiName="IMEI";}
        hiWGX->addLEItem(imeiName, ActiveDevice->HWInfo.IMEI[i].IMEI,ActiveDevice->HWInfo.IMEI[i].TypeModule);
    }
    hiWGX->addItem("Processor", ActiveDevice->HWInfo.cpu.Processor);
    hiWGX->addItem("Hardware", ActiveDevice->HWInfo.cpu.HardWare);
    hiWGX->addItem("Kernels", ActiveDevice->HWInfo.cpu.Kernels);
    if (ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()<1000000000)
    {
        if (ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()>1000)
        {
            if (ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()/1000>100)
                {hiWGX->addItem("CPU frequency", QVariant(ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()/1000).toString()+tr(" Mhz"));}
            else
                {hiWGX->addItem("CPU frequency", QVariant(ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()/1000).toString()+tr(" Ghz"));}
        }
        else
            {hiWGX->addItem("CPU frequency", ActiveDevice->HWInfo.cpu.cpuFreq);}
    }
    else
        {hiWGX->addItem("CPU frequency", QVariant(ActiveDevice->HWInfo.cpu.cpuFreq.toDouble()/1000000).toString()+tr(" Mhz"));}
    hiWGX->addItem("Kernels On-Line", ActiveDevice->HWInfo.cpu.KernelsOnL);
    hiWGX->addItem("CPU Architecture", ActiveDevice->HWInfo.cpu.cpuArch);
    hiWGX->addItem("CPU Revision", ActiveDevice->HWInfo.cpu.ProcRev);
    hiWGX->addItem("CPU Feature", ActiveDevice->HWInfo.cpu.Features);
    hiWGX->addHorizontalLayout();
    hiWGX->addCloseItem();
}

inline void MainWindow::makeWgxSim(int *SimCount, TGSmInfo *ActiveDevice, simCount *SimCountWGX, BaseScrollWigetVL* SimInfoWGX)
{
    if (*SimCount>0)
    {
        for (int i=1;i<=*SimCount;i++)
        {
            if (i<=int(sizeof(ActiveDevice->InsertSim)))
            {
                int testInt;
                testInt=ActiveDevice->InsertSim;
                for (int j=0;j<i-1;j++)
                    {testInt >>= 1;}
                //if (testInt&0x1)
                SimCountWGX->addItem(i, testInt&0x1);
                //else
                    //{SimCountWGX[i]->setValue(QVariant(i).toString()+" - ",QString::fromUtf8("Green_dot_off"));}
            }
            else
                {SimCountWGX->addItem(i, false);}
            //SimCountWGX[i]->setSizeMaximum(70,70);
            bool isReady;
            if (ActiveDevice->GSMSim[i].SimState=="Ready")
                {isReady=true;}
            else
                {isReady=false;}
            if (ActiveDevice->GSMSim.keys().count()>1)
                {SimInfoWGX->addGSMItem(i, isReady, ActiveDevice->GSMSim[i].SimType, ActiveDevice->GSMSim[i].SimOperator, ActiveDevice->GSMSim[i].SimCountry,
                                        ActiveDevice->GSMSim[i].CSNetworkType, ActiveDevice->GSMSim[i].GSMOperator, ActiveDevice->GSMSim[i].Country ,ActiveDevice->GSMSim[i].isRoaming,
                                        ActiveDevice->GSMSim[i].rPin1,ActiveDevice->GSMSim[i].rPin2, ActiveDevice->GSMSim[i].rPuk1, ActiveDevice->GSMSim[i].rPuk2, ActiveDevice->GSMSim[i].iccid);}
            else
                {SimInfoWGX->addGSMItem(0, ActiveDevice->GSMSim[i].phbReady, ActiveDevice->GSMSim[i].SimType, ActiveDevice->GSMSim[i].SimOperator, ActiveDevice->GSMSim[i].SimCountry,
                                        ActiveDevice->GSMSim[i].CSNetworkType, ActiveDevice->GSMSim[i].GSMOperator, ActiveDevice->GSMSim[i].Country ,ActiveDevice->GSMSim[i].isRoaming,
                                        ActiveDevice->GSMSim[i].rPin1,ActiveDevice->GSMSim[i].rPin2, ActiveDevice->GSMSim[i].rPuk1, ActiveDevice->GSMSim[i].rPuk2, ActiveDevice->GSMSim[i].iccid);}
        }
    }
}

void MainWindow::makeWgxScrShot(Screenshot *ssWGX, TSummaryInfo *ActiveDevice)
{
    ssWGX->setCMDPointer(&adbCmd, &adbApp, &doDebug);
    ssWGX->setObjectParam(ActiveDevice, tmpDir.path());
}

void MainWindow::makeWgxApkInfo(apkList *alWGX, int index, TSummaryInfo *ActiveDevice)
{
    alWGX->setCMDPointer(&adbCmd, &adbApp, &doDebug);
    alWGX->setObjectParam(ActiveDevice, index, tmpDir.path(), &cachePath, &Sql, &BaseFunction);
}

void MainWindow::makeWgxFileManager(FileManager *FM, TSummaryInfo *ActiveDevice)
{
    FM->setCMDPointer(&adbCmd, &adbApp, &doDebug);
    FM->setObjectParam(ActiveDevice);
}

