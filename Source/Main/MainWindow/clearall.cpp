/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::closeStatus(int index)
{
    if (ActiveDevices[index].DevStatus=="device" || ActiveDevices[index].DevStatus=="recovery")
    {
        if (ActiveDevices[index].DevStatus=="device")
        {
            ui->DeviveGenInfo_VL->removeWidget(InfoWGX[index].giWGX);
            ui->DeviveBatInfo_VL->removeWidget(InfoWGX[index].biWGX);
            ui->DeviveDisknfo_VL->removeWidget(InfoWGX[index].diWGX);
            ui->HWInfo_VL->removeWidget(InfoWGX[index].hiWGX);
            ui->SSInfo_VL->removeWidget(InfoWGX[index].ssWGX);
            ui->APKMan_VLayout->removeWidget(InfoWGX[index].alWGX);
            ui->DeviceFMInfo_VL->removeWidget(InfoWGX[index].fmWGX);
        }

        InfoWGX[index].giWGX->hide();
        InfoWGX[index].biWGX->hide();
        InfoWGX[index].diWGX->hide();
        InfoWGX[index].hiWGX->hide();
        InfoWGX[index].ssWGX->hide();
        InfoWGX[index].alWGX->hide();
        InfoWGX[index].fmWGX->hide();

        InfoWGX[index].diWGX->deleteAllItems();

        if (ActiveDevices[index].DevStatus=="device")
        {
            ui->DeviceNetworkInfo_VL->removeWidget(InfoWGX[index].niWGX);
            InfoWGX[index].niWGX->hide();
            ui->GSMInfo_VL->removeWidget(InfoWGX[index].SimCountWGX);
            InfoWGX[index].SimCountWGX->hide();
            ui->GSMInfo_VL->removeWidget(InfoWGX[index].SimInfoWGX);
            InfoWGX[index].SimInfoWGX->hide();
            ui->APKMan_VLayout->removeWidget(InfoWGX[index].alWGX);
            InfoWGX[index].alWGX->hide();
        }
    }
}

void MainWindow::clearStatus(int index)
{
    InfoWGX[index].giWGX->close();
    delete InfoWGX[index].giWGX;
    InfoWGX[index].giWGX=0;
    InfoWGX[index].biWGX->close();
    delete InfoWGX[index].biWGX;
    InfoWGX[index].biWGX=0;
    InfoWGX[index].diWGX->close();
    delete InfoWGX[index].diWGX;
    InfoWGX[index].diWGX=0;
    InfoWGX[index].hiWGX->close();
    delete InfoWGX[index].hiWGX;
    InfoWGX[index].hiWGX=0;
    InfoWGX[index].ssWGX->close();
    delete InfoWGX[index].ssWGX;
    InfoWGX[index].ssWGX=0;
    delete InfoWGX[index].fmWGX;
    InfoWGX[index].fmWGX=0;
    if (ActiveDevices[index].DevStatus=="device")
    {
        InfoWGX[index].SimInfoWGX->close();
        delete InfoWGX[index].SimInfoWGX;
        InfoWGX[index].SimInfoWGX=0;
        InfoWGX[index].SimCountWGX->close();
        delete InfoWGX[index].SimCountWGX;
        InfoWGX[index].SimCountWGX=0;
        InfoWGX[index].niWGX->close();
        delete InfoWGX[index].niWGX;
        InfoWGX[index].niWGX=0;

        disconnect(InfoWGX[index].alWGX, SIGNAL(updateDeviceInfo(int)),this, SLOT(updateDeviceInfo(int)));
        InfoWGX[index].alWGX->close();
        delete InfoWGX[index].alWGX;
        InfoWGX[index].alWGX=0;
    }
}

void MainWindow::clearAllStatus()
{
    QList<int> ActiveDevicesList;
    ActiveDevicesList=InfoWGX.keys();
    for (int i=0;i<ActiveDevicesList.count();i++)
        {clearStatus(ActiveDevicesList.at(i));}
}

void MainWindow::closeAboutWindow()
{
    disconnect(AboutDialog, SIGNAL(closeThisWindow()), this, SLOT(closeAboutWindow()));
    AboutDialog->close();
    delete AboutDialog;
    //AboutDialog=0;
}

void MainWindow::closeTCPADBdWindow()
{
    disconnect(ConnectADBdDialog, SIGNAL(closeThisWindow()), this, SLOT(closeTCPADBdWindow()));
    ConnectADBdDialog->close();
    delete ConnectADBdDialog;
    ConnectADBdDialog=0;
}


void MainWindow::AndroidCountClear()
{
    androidCount.allCount=0;
    androidCount.deviceCount=0;
    androidCount.emulatorCount=0;
    androidCount.fastbootCount=0;
    androidCount.offlineCount=0;
    androidCount.recoveryCount=0;
    androidCount.tcpCount=0;
    androidCount.unauthorizedCount=0;
    androidCount.unknownCount=0;
    androidCount.usbCount=0;
}
