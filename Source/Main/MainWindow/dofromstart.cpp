/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"

void MainWindow::defCmd()
{
    adbCmd.adb=defCmdADB();
    adbCmd.fastboot=defCmdFastboot();
    adbApp=defApp();
    AndroidIcon=defIcons();
    appProcess->setCMDPointer(&adbCmd, &adbApp, doDebug.classDebug);
    setAppPath();
    saveConfigAppPath();

    setCacheDir();

    createStatusBar();
}

inline Tadb MainWindow::defCmdADB()
{
    Tadb defCmd;
    defCmd.amAcction << "shell" << "am";
    defCmd.backup << "backup" << "-f";
    defCmd.connectADB << "connect";
    defCmd.cat << "shell" << "cat";
    defCmd.copy << "shell" << "cp" << "-r";
    defCmd.devices << "devices" << "-l";
    defCmd.diskspace << "shell" << "df";
    defCmd.diskUsage << "shell" << "du";
    defCmd.dumpsys << "shell" << "dumpsys";
    defCmd.fdisk << "shell" <<"fdisk" << "-l";
    defCmd.instAPK << "install";
    defCmd.keyevent << "shell" << "input" << "keyevent";
    defCmd.listDir << "shell" << "ls";
    defCmd.listDirL << "shell" << "ls" << "-l";
    defCmd.lsAPK << "shell" << "pm" << "list" << "packages";
    defCmd.mkdir << "shell" << "mkdir" << "-p";
    defCmd.mount << "shell" << "mount";
    defCmd.mv << "shell" << "mv";
    defCmd.netcfg << "shell" << "netcfg";
    defCmd.pm << "shell" << "pm";
    defCmd.sysinfo << "shell" << "getprop";
    defCmd.pull << "pull" << "-p" << "-a";
    defCmd.push << "push" << "-p";
    defCmd.restore << "restore";
    defCmd.root << "root";
    defCmd.screenshot << "shell" << "screencap" << "-p";
    defCmd.shellCMD << "shell";
    defCmd.start << "start-server";
    defCmd.stop << "kill-server";
    defCmd.getState << "get-state";
    defCmd.reboot << "reboot";
    defCmd.rebootBootloader << "reboot" << "bootloader";
    defCmd.rebootRecovery << "reboot" << "recovery";
    defCmd.rm << "shell" << "rm" << "-r";
    defCmd.shutdown << "shell" << "shutdown";
    defCmd.svc << "shell" << "svc";
    defCmd.tcpip << "tcpip" << "5555";
    defCmd.uninstAPK << "uninstall";
    defCmd.unroot << "unroot";
    defCmd.version << "version";
    defCmd.help << "help";
    return(defCmd);
}

inline Tadbfb MainWindow::defCmdFastboot()
{
    Tadbfb defCmd;
    defCmd.devices << "devices" << "-l";
    defCmd.getvar << "getvar" << "all";
    defCmd.flashAll << "flashall";
    defCmd.flash << "flash";
    defCmd.reboot << "reboot";
    defCmd.rebootBootloader << "reboot-bootloader";
    defCmd.update << "update";
    defCmd.help << "help";
    return(defCmd);
}

inline tAdbApp MainWindow::defApp()
{
    tAdbApp defCmd;
    defCmd.adb=settings->value("ApplicationsPath/adbPath").toString();
    defCmd.fastboot=settings->value("ApplicationsPath/fastbootPath").toString();
    return(defCmd);
}

inline tAndroidIcon MainWindow::defIcons()
{
    tAndroidIcon VedroIcon;
    VedroIcon.iSystem.addFile(QStringLiteral(":/Status/System"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iPreload.addFile(QStringLiteral(":/Status/Preload"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iFastBoot.addFile(QStringLiteral(":/Status/Fastboot"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iRecovery.addFile(QStringLiteral(":/Status/Recovery"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iNormal.addFile(QStringLiteral(":/Status/Normal"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iUSB.addFile(QStringLiteral(":/Status/Connected_USB"), QSize(), QIcon::Normal, QIcon::Off);
    VedroIcon.iWIFI.addFile(QStringLiteral(":/Status/Connected_WIFI"), QSize(), QIcon::Normal, QIcon::Off);
    return(VedroIcon);
}

inline void MainWindow::setAppPath()
{
    QString adbPath;
    QStringList adbServer;
    bool StartAdbServer;
    QString tempADB;
    tempADB=adbApp.adb;
    StartAdbServer=false;
    QString adbName;
    QStringList fastbootName;
#if defined(Q_OS_WIN)
    adbName="adb.exe";
    fastbootName << "fastboot.exe";
#else
    adbName="adb";
    fastbootName << "fastboot" << "fastboot-android";
#endif
#if defined(Q_OS_WIN) || defined(Q_OS_LINUX)
    bool testsubdirpath;
    testsubdirpath=false;
#endif
#if defined(Q_OS_LINUX)
    int i;
    i=0;
#endif
    while (StartAdbServer==false)
    {
#if not defined(Q_OS_WIN)
        if (doDebug.adbDebug)
            {qDebug() << "Test path to adb: "+adbApp.adb;}
#endif
        adbServer=appProcess->adbCMD(adbCmd.adb.start, adbServer);
        if (adbServer.count()!=0)
        {
            for (int i=0; i< adbServer.count(); i++)
            {
                if (adbServer.at(i).indexOf("daemon started successfully")!=-1)
                        {StartAdbServer=true;}
            }
        }
        else
        {
            QStringList ADBversion;
            ADBversion=appProcess->adbCMD(adbCmd.adb.version, ADBversion);
            if (ADBversion.count()!=0)
            {
                for (int i=0; i< ADBversion.count(); i++)
                {
                    if (ADBversion.at(i).indexOf("Android Debug Bridge version")!=-1)
                            {StartAdbServer=true;}
                }
            }
        }
        if (!StartAdbServer)
        {
            //adbApp->adb=tempADB;
#if defined(Q_OS_WIN)
            if (!testsubdirpath)
            {
                QString SDP;
                SDP=QCoreApplication::applicationDirPath();
                adbApp->adb=SDP+"/adb/"+adbName;
                adbPath=SDP+"/adb/";
                testsubdirpath=true;
            }
            else
            {
#elif defined(Q_OS_LINUX)
            if (!testsubdirpath)
            {
                QStringList SDP;
                SDP << "/opt/bin" << "/usr/local/bin" << "/opt/android/sdk/platform-tools" << "/opt/android" << QDir::homePath()+"/bin" << QDir::homePath()+"/android/sdk/platform-tools";
                if (i<SDP.count())
                {
                    adbApp.adb=SDP.at(i)+"/"+adbName;
                    adbPath=SDP.at(i);
                    i++;
                }
                else
                    {testsubdirpath=true;}
            }
            else
            {
#endif
            QMessageBox::critical(NULL,QObject::tr("Critical!"),tr("ADB is not present in environment variables and current folder are not contains ADB-tools"));
            adbApp.adb=QFileDialog::getOpenFileName(0, tr("Please chose adb executable file"), QDir::homePath());
            adbPath.clear();
#if defined(Q_OS_WIN) || defined(Q_OS_LINUX)
            }
#endif
        }
    }
    if (adbPath.isEmpty())
    {
        if (adbApp.adb.count()>1)
        {
            QStringList tmpSttrlist;
            tmpSttrlist=adbApp.adb.split("/");
            for (int i=0;i<adbApp.adb.count("/");i++)
                {adbPath=adbPath+tmpSttrlist.at(i)+"/";}
        }
    }
    StartAdbServer=false;
    adbServer.clear();
    //bool testADB;
    //testADB=false;
#if not defined(Q_OS_LINUX)
    int i;
#endif
    i=-1;
    while (!StartAdbServer && i<fastbootName.count())
    {
#if not defined(Q_OS_WIN)
        if (doDebug.adbDebug)
            {qDebug() << "Test path to fastboot: "+adbApp.fastboot;}
#endif
        adbServer=appProcess->fbCMD(adbCmd.fastboot.help, adbServer);
        if (!adbServer.isEmpty())
        {
            if (adbServer.first()=="usage: fastboot [ <option> ] <command>" )
                {StartAdbServer=true;}
        }
        if (!StartAdbServer)
        {
            if (i<fastbootName.count())
            {
                i++;
                adbApp.fastboot=adbPath+fastbootName.at(i);
            }
            else
            {
                QMessageBox::critical(NULL,QObject::tr("Critical!"),tr("Fastboot is not present in environment variables and current folder are not contains ADB-tools"));
                adbApp.fastboot=QFileDialog::getOpenFileName(0, tr("Please choose fastboot executable file"), QDir::homePath());
            }
       }
    }
}

inline void MainWindow::saveConfigAppPath()
{
    if (adbApp.adb!=settings->value("ApplicationsPath/adbPath").toString())
        {settings->setValue("ApplicationsPath/adbPath",adbApp.adb);}
    if (adbApp.fastboot!=settings->value("ApplicationsPath/fastbootPath").toString())
        {settings->setValue("ApplicationsPath/fastbootPath",adbApp.fastboot);}

#if not defined(Q_OS_WIN)
    if (settings->value("Debug/adb").toString().isEmpty())
        {settings->setValue("Debug/adb", doDebug.adbDebug);}
    if (settings->value("Debug/classes").toString().isEmpty())
        {settings->setValue("Debug/classes", doDebug.classDebug);}
#endif
    settings->sync();
}

void MainWindow::setCacheDir()
{
    QDir cacheDir;
    cachePath=SysTools.cacheDir(AppName);
    cacheDir.setPath(cachePath);
    if (!cacheDir.exists())
    {
        cacheDir.mkpath(cachePath);
        cacheDir.mkpath(cachePath+QDir::separator()+"ApkIcons");
    }
    else
    {
        cacheDir.setPath(cachePath+QDir::separator()+"ApkIcons");
        if (!cacheDir.exists())
            {cacheDir.mkpath(cachePath+QDir::separator()+"ApkIcons");}
    }
    Sql.setCachePath(cachePath);
}

inline void MainWindow::createStatusBar()
{
    AndroidCountClear();
    QStringList statBarContent;
    statBarContent << tr("All") << tr("USB") << tr("TCP/IP") << tr("Emulator") << tr("Offline") << tr("Unknown") << tr("Unauthorized") << tr("Device") << tr("Recovery") << tr("Fastboot");
    for (int i=0;i<statBarContent.count();i++)
    {
        //StatusBarCount[statBarContent.at(i)]=new StatusBarFrame(statusBar());
        StatusBarCount[statBarContent.at(i)]=new doubleLabel(statusBar());
        StatusBarCount[statBarContent.at(i)]->setObjectName(QStringLiteral("SBF_")+statBarContent.at(i));
        //StatusBarCount[statBarContent.at(i)]->setName(statBarContent.at(i));
        StatusBarCount[statBarContent.at(i)]->setValue(statBarContent.at(i)+":", "0");
        StatusBarCount[statBarContent.at(i)]->setFontSize(8);
        StatusBarCount[statBarContent.at(i)]->setLabelSizeMinimum(4);
        statusBar()->addWidget(StatusBarCount[statBarContent.at(i)]);
    }
}
