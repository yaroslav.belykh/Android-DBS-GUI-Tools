/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::showInfo(int index)
{
    ui->pB_Fastboot->setEnabled(true);
    ui->pB_Reboot->setEnabled(true);
    if (ActiveDevices[index].DevStatus=="device" || ActiveDevices[index].DevStatus=="recovery")
    {
        ui->pB_Recovery->setEnabled(true);
        ui->pB_Shutdown->setEnabled(true);
        getDevInfo->getDiskSpaceInfo(&ActiveDevices[index]);
    }
    if (ActiveDevices[index].DevStatus=="device")
    {
        if (ActiveDevices[index].overTCP)
            {this->setWindowIcon(AndroidIcon.iWIFI);}
        else
            {this->setWindowIcon(AndroidIcon.iUSB);}
        ui->tb_Status->setIcon(AndroidIcon.iSystem);
        ui->twDevice->setVisible(true);
        showGeneralInfo(InfoWGX[index].giWGX, ui->DeviveGenInfo_VL);
        shoWBatteryInfo(InfoWGX[index].biWGX, ui->DeviveBatInfo_VL);
        showDiskInfo(&ActiveDevices[index], InfoWGX[index].diWGX, ui->DeviveDisknfo_VL);
        showSimInfo(ActiveDevices[index].HWInfo.SimCount, InfoWGX[index].SimCountWGX, InfoWGX[index].SimInfoWGX);
        showNetCfgInfo(InfoWGX[index].niWGX);
        showHardwareInfo(&ActiveDevices[index].HWInfo.Memory,InfoWGX[index].hiWGX,ui->HWInfo_VL);
        showScreenShotInfo(InfoWGX[index].ssWGX, ui->SSInfo_VL);
        showAPKInfo(InfoWGX[index].alWGX, ui->APKMan_VLayout);
        showFileManager(InfoWGX[index].fmWGX, ui->DeviceFMInfo_VL);
        InfoWGX[index].WGXb=true;
    }
    if (ActiveDevices[index].DevStatus=="fastboot")
    {
        ui->tb_Status->setIcon(AndroidIcon.iFastBoot);
        ui->pB_Recovery->setEnabled(false);
        ui->pB_Shutdown->setEnabled(false);
        ui->twDevice->setVisible(false);
    }
}

void MainWindow::showGeneralInfo(BaseScrollWigetVL *giWGX, QVBoxLayout *thisGrid)
{
    thisGrid->addWidget(giWGX);
    giWGX->show();
}

void MainWindow::shoWBatteryInfo(BatteryInformation *biWGX, QVBoxLayout *thisGrid)
{
    biWGX->getBatteryInfo();
    biWGX->showBatteryInfo();
    thisGrid->addWidget(biWGX);
    biWGX->show();
}

void MainWindow::showDiskInfo(TSummaryInfo *ActiveDevice, BaseScrollWidgetGV *diWGX, QVBoxLayout *thisGrid)
{
    thisGrid->addWidget(diWGX);
    diWGX->show();
    QStringList mapList;
    mapList=ActiveDevice->DiskSpace.keys();
    for (int i=0;i<mapList.count();i++)
    {
        TDiskSpaceInt diInt;
        QString diName;
        int diValue;
        diInt=getIntDIValue(ActiveDevice->DiskSpace[mapList.at(i)]);
        if (mapList.at(i)!="sdcard external")
            {diName=BaseFunction.AllFirstSymbolToUpper(mapList.at(i)," ");}
        else
            {diName=BaseFunction.AllFirstSymbolToUpper(mapList.at(i)," ").remove(mapList.at(i).indexOf("ernal"),5);}
        if (!ActiveDevice->DiskSpace[mapList.at(i)].dfSize.isEmpty() && diInt.dfSize-diInt.dfUsed==0 && diInt.dfFree!=0)
            {diValue=diInt.dfSize-diInt.dfFree;}
        else
            {diValue=diInt.dfUsed;}
        if (mapList.at(i)=="data")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 0, 0);}
        if (mapList.at(i)=="sdcard")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 0, 1);}
        if (mapList.at(i)=="sdcard external")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 0, 2);}
        if (mapList.at(i)=="system")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 1, 0);}
        if (mapList.at(i)=="cache")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 1, 1);}
        if (mapList.at(i)=="root")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, 1, 2);}
        if (mapList.at(i)!="data" && mapList.at(i)!="sdcard" && mapList.at(i)!="sdcard external" && mapList.at(i)!="system" && mapList.at(i)!="cache" && mapList.at(i)!="root")
            {diWGX->addItem(diName, ActiveDevice->DiskSpace[mapList.at(i)].dfSize, ActiveDevice->DiskSpace[mapList.at(i)].dfUsed, ActiveDevice->DiskSpace[mapList.at(i)].dfFree, diInt.dfSize, diValue, -1, -1);}
    }
    diWGX->addCloseItems();
}

void MainWindow::showSimInfo(int SimCount, simCount *SimCountWGX, BaseScrollWigetVL *SimInfoWGX)
{
    if (SimCount>0)
    {
        ui->GSMInfo_VL->insertWidget(0, SimCountWGX);
        ui->GSMInfo_VL->insertWidget(1, SimInfoWGX);
    }
}

void MainWindow::showNetCfgInfo(NetInformation* niWGX)
{
    niWGX->getNetWorkInfo();
    niWGX->showNetWorkInfo();
    ui->DeviceNetworkInfo_VL->addWidget(niWGX);
    niWGX->show();
}

void MainWindow::showHardwareInfo(tMemory *ActiveDevice, BaseScrollWigetVL *hiWGX, QVBoxLayout *thisGrid)
{
    int memPos;
    hiWGX->clearVariablesWigets();
    memPos=0;
    hiWGX->addMemoryInfo("Memory", QVariant(ActiveDevice->TotalMem/1000000).toString()+" MB",
                         QVariant((ActiveDevice->TotalMem-ActiveDevice->FreeMem)/1000000).toString()+" MB",
                         QVariant(ActiveDevice->FreeMem/1000000).toString()+" MB",
                         QVariant(ActiveDevice->CachedMem/1000000).toString()+" MB",
                         ActiveDevice->TotalMem, ActiveDevice->TotalMem-ActiveDevice->FreeMem, &memPos);
    if (ActiveDevice->TotalSwap>0)
    {
        hiWGX->addMemoryInfo("Swap", QVariant(ActiveDevice->TotalSwap/1000000).toString()+" MB",
                             QVariant((ActiveDevice->TotalSwap-ActiveDevice->FreeSwap)/1000000).toString()+" MB",
                             QVariant(ActiveDevice->FreeSwap/1000000).toString()+" MB",
                             QVariant(ActiveDevice->CachedSwap/1000000).toString()+" MB",
                             ActiveDevice->TotalSwap, ActiveDevice->TotalSwap-ActiveDevice->FreeSwap, &memPos);
    }
    else
        {hiWGX->addMemoryInfo("Swap", "0", "0", "0", "0", 0, 0, &memPos);}
    thisGrid->addWidget(hiWGX);
}


void MainWindow::showScreenShotInfo(Screenshot *ssWGX, QVBoxLayout *thisGrid)
{
    ssWGX->makescreenshot();
    thisGrid->addWidget(ssWGX);
    ssWGX->show();
}

void MainWindow::showAPKInfo(apkList *alWGX, QVBoxLayout *thisGrid)
{
    thisGrid->addWidget(alWGX);
    alWGX->show();
    alWGX->getAPKList();
}

void MainWindow::showFileManager(FileManager *fmWGX, QVBoxLayout *thisGrid, bool reFrash)
{
     if (reFrash)
     {
        thisGrid->addWidget(fmWGX);
        fmWGX->show();
     }
}

void MainWindow::updateDeviceInfo(int index)
{
    InfoWGX[index].biWGX->getBatteryInfo();
    InfoWGX[index].biWGX->showBatteryInfo();
}

/********************************************* Inline additional functions *********************************************/

inline TDiskSpaceInt MainWindow::getIntDIValue(TDiskSpace dfds)
{
    QString DIMinValue;
    TDiskSpaceInt outData;
    if (dfds.dfSize.indexOf("G")!=-1)
    {
        outData.dfSize=dfds.dfSize.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,30.0);
        if (outData.dfSize>999999999)
        {
            DIMinValue="K";
            outData.dfSize=dfds.dfSize.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);
        }
        else
            {DIMinValue="";}
    }
    else
    {
        if (dfds.dfSize.indexOf("M")!=-1)
        {
            outData.dfSize=dfds.dfSize.split("M",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);
            DIMinValue="";
        }
        else
        {
            if (dfds.dfSize.indexOf("K")!=-1)
            {
                outData.dfSize=dfds.dfSize.split("K",QString::SkipEmptyParts).first().toDouble()*pow(2.0,10.0);
                DIMinValue="";
            }
            else
            {
                outData.dfSize=dfds.dfSize.toDouble();
                DIMinValue="";
            }
        }
    }
    if (DIMinValue=="")
    {
        if (dfds.dfUsed.indexOf("G")!=-1)
            {outData.dfUsed=dfds.dfUsed.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,30.0);}
        else
        {
            if (dfds.dfUsed.indexOf("M")!=-1)
                {outData.dfUsed=dfds.dfUsed.split("M",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);}
            else
            {
                if (dfds.dfUsed.indexOf("K")!=-1)
                    {outData.dfUsed=dfds.dfUsed.split("K",QString::SkipEmptyParts).first().toDouble()*pow(2.0,10.0);}
                else
                    {outData.dfUsed=dfds.dfUsed.toInt();}
            }
        }
        if (dfds.dfFree.indexOf("G")!=-1)
            {outData.dfFree=dfds.dfFree.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,30.0);}
        else
        {
            if (dfds.dfFree.indexOf("M")!=-1)
                {outData.dfFree=dfds.dfFree.split("M",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);}
            else
            {
                if (dfds.dfFree.indexOf("K")!=-1)
                    {outData.dfFree=dfds.dfFree.split("K",QString::SkipEmptyParts).first().toDouble()*pow(2.0,10.0);}
                else
                    {outData.dfFree=dfds.dfFree.toInt();}
            }
        }
    }
    else
    {
        if (dfds.dfUsed.indexOf("G")!=-1)
            {outData.dfUsed=dfds.dfUsed.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);}
        else
        {
            if (dfds.dfUsed.indexOf("M")!=-1)
                {outData.dfUsed=dfds.dfUsed.split("M",QString::SkipEmptyParts).first().toDouble()*pow(2.0,10.0);}
            else
            {
                if (dfds.dfUsed.indexOf("K")!=-1)
                    {outData.dfUsed=dfds.dfUsed.split("",QString::SkipEmptyParts).first().toDouble();}
            }
        }
        if (dfds.dfFree.indexOf("G")!=-1)
            {outData.dfFree=dfds.dfFree.split("G",QString::SkipEmptyParts).first().toDouble()*pow(2.0,20.0);}
        else
        {
            if (dfds.dfFree.indexOf("M")!=-1)
                {outData.dfFree=dfds.dfFree.split("M",QString::SkipEmptyParts).first().toDouble()*pow(2.0,10.0);}
            else
            {
                if (dfds.dfFree.indexOf("K")!=-1)
                    {outData.dfFree=dfds.dfFree.split("",QString::SkipEmptyParts).first().toDouble();}
            }
        }
    }
    return(outData);
}

