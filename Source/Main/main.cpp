/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator translator;

    QCoreApplication::setApplicationName("ADBGUITools");
    QCoreApplication::setOrganizationName("Yamah-BYV");
    QCoreApplication::setOrganizationDomain("adbguitools.belykh.me");

    QString appPath;
    QString TransL;
    appPath=QCoreApplication::applicationDirPath();
#if defined(Q_OS_LINUX)
    if (appPath=="/usr/bin")
        {TransL="/usr/share/ADSTool";}
    else
    {
        if (appPath=="/usr/local/bin")
            {TransL="/usr/local/share/ADSTool";}
        else
            {
#endif
        TransL="../Android-DBS-GUI-Tools";
#if defined(Q_OS_LINUX)
        }
    }
#endif

    translator.load(TransL+"/Translations/ADSTool-"+QLocale::system().name());
    a.installTranslator(&translator);

    MainWindow w;
    w.show();

    return a.exec();
}
