/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Main/about.h"
#include "ui_about.h"

about::about(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::about)
{
    ui->setupUi(this);

    this->setLayout(ui->verticalLayout);
    ui->tab_license->setLayout(ui->vLayout_Licence);
    ui->tab_authors->setLayout(ui->vLayout_Authors);
    ui->tab_about->setLayout(ui->vLayout_About);

    licenceTxt=uncompressFile(":/License/license.qz");
    ui->tE->setText(licenceTxt);
    QString nickName;
#if defined(Q_OS_LINUX)
    nickName=tr("Yamah");
#elif defined(Q_OS_WIN)
    nickName=tr("YadoFitiy PlyusCH");
#endif
    ui->tE_A->clear();
    ui->tE_A->setText("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\
                      <html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\
                      p, li { white-space: pre-wrap; }</style></head>\
<body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">"+tr("Author")+":</span></p>\
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"mailto:yaroslav.belykh@gmail.com\"><span style=\" font-size:14pt; font-weight:600; text-decoration: underline; color:#0000ff;\">"+tr("Yaroslav Belykh")+"</span></a></p>\
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">aka </span><span style=\" font-size:14pt; font-style:italic;\">"+nickName+"</span></p>\
<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;\"><br /></p>\
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">"+tr("Thanks:")+"</span></p>\
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:600;\">"+tr("Sergey Jamoytel")+"</span></p>\
<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">"+tr("for the RPM for Rosa Fresh")+"</span></p></body></html>");
}

about::~about()
{
    delete ui;
}

QString about::uncompressFile(QString fPath)
{
    QFile licenseFile;
    QString outStr;
    licenseFile.setFileName(fPath);
    if (licenseFile.open(QIODevice::ReadOnly))
    {
        outStr=QString(qUncompress(licenseFile.readAll()));
        licenseFile.close();
    }
    return(outStr);
}

void about::on_pushButton_clicked()
{
    this->close();
    emit closeThisWindow();
}
