/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Base/adbprocess.h"

adbprocess::adbprocess(QObject *parent) : QObject(parent)
{
    adbProc = new QProcess(this);
}

adbprocess::~adbprocess()
{
    delete adbProc;
    adbProc=0;
}

QStringList adbprocess::adbCMD(QStringList cmdList, QStringList cmdParam, QString DevNumber, QString DevPath, bool unicDevNumber, bool printDebugVal)
{
#if not defined(Q_OS_WIN)
    if (printDebugVal && isDebug && cmdList!=procCmd->adb.version)
        {qDebug() << parentName+":" << "adbProcess read ADB data:" << adbApp->adb+" "+StrList2Str(cmdList)+" "+StrList2Str(cmdParam);}
#endif
    QStringList outCMD;
    QStringList ProgramArguments;
    QByteArray cmdStdOut;
    QByteArray cmdErrOut;
    QString errorStr;
    errorStr="";
    if (cmdList==procCmd->adb.devices || cmdList==procCmd->adb.start || cmdList==procCmd->adb.stop || cmdList==procCmd->adb.version || cmdList==procCmd->adb.root || cmdList==procCmd->adb.unroot)
        {adbProc->start(adbApp->adb, ProgramArguments << cmdList);}
    else
    {
        if (cmdList==procCmd->adb.tcpip || cmdList==procCmd->adb.connectADB)
            {adbProc->start(adbApp->adb, ProgramArguments << cmdList << cmdParam);}
        else
        {
            QStringList devAcc;
            if (unicDevNumber)
                {devAcc << "-s" << DevNumber;}
            else
                {devAcc << "-s" << DevPath;}
            adbProc->start(adbApp->adb, ProgramArguments << devAcc << cmdList << cmdParam);
        }
    }
    adbProc->waitForStarted();
    //if (cmdList!=procCmd->adb.pull && cmdList!=procCmd->adb.push && cmdList!=procCmd->adb.backup && cmdList!=procCmd->adb.restore)
    if (cmdList!=procCmd->adb.backup && cmdList!=procCmd->adb.restore)
    {
        adbProc->waitForFinished();
        cmdStdOut=adbProc->readAllStandardOutput();
        cmdErrOut=adbProc->readAllStandardError();
        errorStr=adbProc->errorString();
    }
    else
    {
        if (cmdList==procCmd->adb.backup || cmdList==procCmd->adb.restore)
        {
            QMessageBox* msgBox = new QMessageBox(NULL);
            msgBox->setAttribute(Qt::WA_DeleteOnClose);
            //msgBox->setStandardButtons(QMessageBox::Ok);
            msgBox->setWindowTitle( tr("Confirm the operation!") );
            msgBox->setText( tr("Now unlock your device and confirm the backup or restore operation.\nPlease wait for finish operation and press \"OK\" button.\nThis window will be closed automatically.") );
            msgBox->setIcon(QMessageBox::Question);
            msgBox->setModal(false);
            msgBox->open(NULL, SLOT(msgBoxClosed(QAbstractButton*)) );
            cmdStdOut=adbProc->readAllStandardOutput();
            cmdErrOut=adbProc->readAllStandardError();
            adbProc->waitForFinished();
            msgBox->close();
        }
    }
    if (QString(cmdStdOut).indexOf(": not found")!=-1 && cmdList.at(0)=="shell")
    {
        ProgramArguments.clear();
        QStringList devAcc;
        if (unicDevNumber)
            {devAcc << "-s" << DevNumber;}
        else
            {devAcc << "-s" << DevPath;}
        cmdStdOut.clear();
        cmdErrOut.clear();
        cmdList.insert(1, "busybox");
        adbProc->start(adbApp->adb, ProgramArguments << devAcc << cmdList << cmdParam);
        adbProc->waitForStarted();
        adbProc->waitForFinished();
        cmdStdOut=adbProc->readAllStandardOutput();
        cmdErrOut=adbProc->readAllStandardError();
        if (QString(cmdStdOut).indexOf("not found")!=-1)
            {cmdStdOut.clear();}
    }
    if (QString(cmdStdOut).indexOf("\r\n")==-1)
        {outCMD.append(QString(cmdStdOut).split("\n",QString::SkipEmptyParts));}
    else
        {outCMD.append(QString(cmdStdOut).split("\r\n",QString::SkipEmptyParts));}
    if ((errorStr.indexOf("No such file or directory")!=-1) || (errorStr.indexOf("Process failed to start")!=-1) || (errorStr.indexOf("not found")!=-1))
        {outCMD.append(errorStr);}
    return(outCMD);
}

QStringList adbprocess::fbCMD(QStringList cmdList, QStringList cmdParam, QString DevNumber, QString DevPath, bool unicDevNumber, bool printDebugVal)
{
#if not defined(Q_OS_WIN)
    if (printDebugVal && isDebug && cmdList!=procCmd->fastboot.help)
        {qDebug() << parentName+":" << "adbProcess read Fastboot data:" << adbApp->fastboot+" "+StrList2Str(cmdList)+" "+StrList2Str(cmdParam);}
#endif
    QStringList outCMD;
    QStringList ProgramArguments;
    QByteArray cmdStdOut;
    QByteArray cmdErrOut;
    QString errorStr;
    if (cmdList==procCmd->fastboot.devices)
        {adbProc->start(adbApp->fastboot, ProgramArguments << cmdList);}
    else
    {
        QStringList devAcc;
        devAcc << "-s";
        if (unicDevNumber)
            {devAcc << DevNumber;}
        else
            {devAcc << DevPath;}
        adbProc->start(adbApp->fastboot, ProgramArguments << devAcc << cmdList << cmdParam);
    }
    adbProc->waitForStarted();
    adbProc->waitForFinished();
    cmdStdOut=adbProc->readAllStandardOutput();
    cmdErrOut=adbProc->readAllStandardError();
    errorStr=adbProc->errorString();
    if (cmdStdOut=="")
        {cmdStdOut=cmdErrOut;}
    outCMD=QString(cmdStdOut).split("\n",QString::SkipEmptyParts);
    if ((errorStr.indexOf("No such file or directory")!=-1) || (errorStr.indexOf("Process failed to start")!=-1) || (errorStr.indexOf("not found")!=-1))
        {outCMD.append("Error_NF");}
    return outCMD;
}


void adbprocess::setCMDPointer(tCMD *newCmd, tAdbApp *newAdbApp, bool printDebug, QString ParName)
{
    adbApp=newAdbApp;
    if (adbApp->adb.isEmpty())
#if defined(Q_OS_WIN)
        {adbApp->adb="adb.exe";}
#else
        {adbApp->adb="adb";}
#endif
    if (adbApp->fastboot.isEmpty())
#if defined(Q_OS_WIN)
        {adbApp->fastboot="fastboot.exe";}
#else
        {adbApp->fastboot="fastboot";}
#endif
    procCmd=newCmd;
    parentName=ParName;
    isDebug=printDebug;
#if not defined(Q_OS_WIN)
    if (isDebug)
        {qDebug() << parentName+":" << "Class adbProcess was created";}
#endif
}

QString adbprocess::StrList2Str(QStringList strList)
{
    QString Str;
    Str="";
    if (strList.count()>0)
    {
        Str.append(strList.at(0));
        for (int i=1;i<strList.count();i++)
            {Str.append(" "+strList.at(i));}
    }
    return(Str);
}
