/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Base/getinfo.h"

//getInfo::getInfo(QObject *parent) : QObject(parent)
getInfo::getInfo(tCMD *newCMD, adbprocess *newAppProcess, basefunction *newBaseFunction, QObject *parent) : QObject(parent)
{
    appProcess=newAppProcess;
    BaseFunction=newBaseFunction;
    appCmd=newCMD;
}

void getInfo::getNameDevSysInfo(TSummaryInfo *ActiveDevice)
{
    QStringList rawSysInfo;
    if (ActiveDevice->DevInfo.ProductNames.prod_Board=="" && ActiveDevice->DevInfo.ProductNames.prod_Board=="")
    {
        rawSysInfo=getDevSysInfoRaw(ActiveDevice);
        if (ActiveDevice->DevStatus=="device")
        {
            ActiveDevice->DevInfo.ProductNames.prod_Board=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.board]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Brand=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.brand]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Device=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.device]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Manufacture=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.manufacturer]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Model=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.model]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Name=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.name]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_Product=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.product]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_Version=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.version.release]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_SdkVersion=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.version.sdk]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_HidVersion=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.hidden_ver]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_hwVersion=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.hw.build.version]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_Type=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.characteristics]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_VersionIncremental=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.version.incremental]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Region=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.locale.region]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.prod_Language=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.locale.language]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.boot_SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.boot.serialno]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.device_SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.device.sn]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_SKU=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.sku]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_PDA=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.PDA]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.ril_sn=splitNeedInfoFromRawInfo(rawSysInfo,"[ril.serialnumber]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.serialno]", ": ", "[]");
            if (!splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.serial]", ": ", "[]").isEmpty())
                {ActiveDevice->DevInfo.ProductNames.GSM_SN=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.serial]", ": ", "[]").split(" ",QString::SkipEmptyParts).first();}
            ActiveDevice->DevInfo.ProductNames.GSMVersionBB=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.version.baseband]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_DisplayID=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.display.id]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_ID=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build..id]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.build_CustomBuildVerion=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.custom.build.version]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.ExternalSD=splitNeedInfoFromRawInfo(rawSysInfo,"[external_sd_path]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNames.InternalSD=splitNeedInfoFromRawInfo(rawSysInfo,"[internal_sd_path]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNetInfo.HostName=splitNeedInfoFromRawInfo(rawSysInfo,"[net.hostname]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNetInfo.wifiIf=splitNeedInfoFromRawInfo(rawSysInfo,"[wifi.interface]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNetInfo.BtName=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.custom.bluetooth.name]", ": ", "[]");
            ActiveDevice->DevInfo.ProductNetInfo.WifiAPName=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.custom.wifiAP.name]", ": ", "[]");
        }
        else
        {
            if (ActiveDevice->DevStatus=="recovery")
            {
                ActiveDevice->DevInfo.ProductNames.prod_Board=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.board", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Brand=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.brand", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Device=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.device", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Manufacture=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.manufacturer", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Model=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.model", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Name=splitNeedInfoFromRawInfo(rawSysInfo,"ro.product.name", "=");
                ActiveDevice->DevInfo.ProductNames.build_Product=splitNeedInfoFromRawInfo(rawSysInfo,"ro.build.product", "=");
                ActiveDevice->DevInfo.ProductNames.build_Version=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.version.release]", "=");
                ActiveDevice->DevInfo.ProductNames.build_Type=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.build.characteristics]", ": ", "[]");
                ActiveDevice->DevInfo.ProductNames.prod_Region=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.locale.region]", "=");
                ActiveDevice->DevInfo.ProductNames.prod_Language=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.product.locale.language]", "=");
                ActiveDevice->DevInfo.ProductNames.device_SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.device.sn]", ": ", "[]");
                ActiveDevice->DevInfo.ProductNames.boot_SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.boot.serialno]", "=");
                ActiveDevice->DevInfo.ProductNames.SN=splitNeedInfoFromRawInfo(rawSysInfo,"[ro.serialno]", ": ", "[]");
            }
            if (ActiveDevice->DevStatus=="fastboot")
            {
                for (int i=0;i<rawSysInfo.count();i++)
                {
                    if (rawSysInfo.at(i).indexOf("partition-type")!=-1 || rawSysInfo.at(i).indexOf("partition-size")!=-1)
                    {
                        QString partName;
                        partName=rawSysInfo.at(i).split(":",QString::SkipEmptyParts).at(1);
                        if (rawSysInfo.at(i).indexOf("partition-type")!=-1)
                            {ActiveDevice->DevFBInfo.Partitions[partName].partType=rawSysInfo.at(i).split(": ",QString::SkipEmptyParts).last();}
                        if (rawSysInfo.at(i).indexOf("partition-size")!=-1)
                            {ActiveDevice->DevFBInfo.Partitions[partName].partSize=rawSysInfo.at(i).split(": ",QString::SkipEmptyParts).last();}
                    }
                    else
                    {
                        if (rawSysInfo.at(i).indexOf("kerne:")!=-1 )
                            {ActiveDevice->DevFBInfo.Kernel=rawSysInfo.at(i).split("kernel: ",QString::SkipEmptyParts).last();}
                        if (rawSysInfo.at(i).indexOf("product:")!=-1 )
                            {ActiveDevice->DevFBInfo.Product=rawSysInfo.at(i).split("product: ",QString::SkipEmptyParts).last();}
                        if (rawSysInfo.at(i).indexOf("version:")!=-1 )
                            {ActiveDevice->DevFBInfo.Version=rawSysInfo.at(i).split("version: ",QString::SkipEmptyParts).last();}
                    }
                }
            }
        }
        if (ActiveDevice->DevStatus=="device" || ActiveDevice->DevStatus=="recovery")
        {
            if (splitNeedInfoFromRawInfo(rawSysInfo,"[wlan.driver.status]", ": ", "[]")=="ok")
                {ActiveDevice->DevInfo.ProductNetInfo.wifiOn=true;}
            else
                {ActiveDevice->DevInfo.ProductNetInfo.wifiOn=false;}
            QStringList rawImeiInfo;
            rawImeiInfo=getGSMModulesServicesInfo(ActiveDevice,"iphonesubinfo",5);
            int lastJ;
            lastJ=-1;
            ActiveDevice->HWInfo.SimCount=0;
            for (int j=0;j<rawImeiInfo.count();j++)
            {
                if (rawImeiInfo.at(j).indexOf("Device ID")!=-1)
                {
                    ActiveDevice->HWInfo.SimCount++;
                    ActiveDevice->HWInfo.IMEI[ActiveDevice->HWInfo.SimCount].IMEI=rawImeiInfo.at(j).split(" ",QString::SkipEmptyParts).last();
                    for (int m=lastJ+1;m<j;m++)
                    {
                        if (rawImeiInfo.at(m).indexOf("Phone Type")!=-1)
                        {
                            ActiveDevice->HWInfo.IMEI[ActiveDevice->HWInfo.SimCount].TypeModule=rawImeiInfo.at(m).split(" ",QString::SkipEmptyParts).last();
                            break;
                        }
                    }
                    lastJ=j;
                }
            }
            QString tmpStr;
            tmpStr=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.inserted]", ": ", "[]");
            if (tmpStr!="")
                {ActiveDevice->Gsm.InsertSim=tmpStr.toInt();}
            else
            {
                int tmpInt;
                bool db;
                tmpInt=0;
                db=false;
                for (int j=0;j<=ActiveDevice->HWInfo.SimCount;j++)
                {
                    QString tmpStrL;
                    if ((j!=0 && j!=1) || (j==1 && !db))
                    {
                        tmpStrL="[gsm.sim.state."+QVariant(j).toString()+"]";
                        if (splitNeedInfoFromRawInfo(rawSysInfo,tmpStrL, ": ", "[]").toUpper()!="Absent")
                            {tmpInt=tmpInt+int(pow(2,j-1));}
                    }
                    else
                    {
                        if (j==0)
                        {
                            tmpStrL="[gsm.sim.state]";
                            if (splitNeedInfoFromRawInfo(rawSysInfo,tmpStrL, ": ", "[]").toUpper()!="Absent")
                            {
                                tmpInt=tmpInt+int(pow(2,0));
                                db=true;
                            }
                        }
                    }
                }
                ActiveDevice->Gsm.InsertSim=tmpInt;
                //ActiveDevice->Gsm.InsertSim=0;
            }
            if (ActiveDevice->Gsm.InsertSim!=0)
            {
                for (int j=0;j<=ActiveDevice->HWInfo.SimCount;j++)
                {
                    if ((j!=0 && j!=1) || (j==1 && ActiveDevice->Gsm.GSMSim[1].SimState==""))
                    {
                        ActiveDevice->Gsm.GSMSim[j].SimType=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.ril.uicctype."+QVariant(j).toString()+"]", ": ", "[]");
                        if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state."+QVariant(j).toString()+"]", ": ", "[]").toUpper()=="READY")
                            {ActiveDevice->Gsm.GSMSim[j].SimState="Ready";}
                        else
                        {
                            if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state."+QVariant(j).toString()+"]", ": ", "[]").toUpper()=="NOT_READY")
                                {ActiveDevice->Gsm.GSMSim[j].SimState="Off";}
                            else
                            {
                                if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state."+QVariant(j).toString()+"]", ": ", "[]").toUpper()=="ABSENT")
                                    {ActiveDevice->Gsm.GSMSim[j].SimState="Absent";}
                            }
                        }
                        ActiveDevice->Gsm.GSMSim[j].GSMOperator=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.alpha."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].SimOperator=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.alpha."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].SimOperatorBN=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.default-name."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].SimCountry=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.iso-country."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].Country=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.iso-country."+QVariant(j).toString()+"]", ": ", "[]");
                        if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.isroaming."+QVariant(j).toString()+"]", ": ", "[]")=="true")
                            {ActiveDevice->Gsm.GSMSim[j].isRoaming=true;}
                        else
                            {ActiveDevice->Gsm.GSMSim[j].isRoaming=false;}
                        if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.roaming.indicator.needed."+QVariant(j).toString()+"]", ": ", "[]")=="true")
                            {ActiveDevice->Gsm.GSMSim[j].RoamingNeeded=true;}
                        else
                            {ActiveDevice->Gsm.GSMSim[j].RoamingNeeded=false;}
                        ActiveDevice->Gsm.GSMSim[j].BaseBand=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.project.baseband."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].rPin1=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.pin1."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].rPin2=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.pin2."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].rPuk1=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.puk1."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].rPuk2=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.puk2."+QVariant(j).toString()+"]", ": ", "[]");
                        ActiveDevice->Gsm.GSMSim[j].CSNetworkType=getGSMNetworkType(splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.cs.network.type."+QVariant(j).toString()+"]", ": ", "[]").toInt());
                        ActiveDevice->Gsm.GSMSim[j].iccid=splitNeedInfoFromRawInfo(rawSysInfo,"[ril.iccid.sim"+QVariant(j).toString()+"]", ": ", "[]");
                    }
                    else
                    {
                        if (j==0)
                        {
                            ActiveDevice->Gsm.GSMSim[1].SimType=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.ril.uicctype]", ": ", "[]");
                            if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state]", ": ", "[]").toUpper()=="READY")
                                {ActiveDevice->Gsm.GSMSim[1].SimState="Ready";}
                            else
                            {
                                if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state]", ": ", "[]").toUpper()=="NOT_READY")
                                    {ActiveDevice->Gsm.GSMSim[1].SimState="Off";}
                                else
                                {
                                    if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state]", ": ", "[]").toUpper()=="ABSENT")
                                        {ActiveDevice->Gsm.GSMSim[1].SimState="Absent";}
                                }
                            }
                            //ActiveDevice->Gsm.GSMSim[1].SimState=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.state]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].GSMOperator=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.alpha]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].SimOperator=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.alpha]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].SimOperatorBN=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.default-name]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].GSMOperator=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.alpha]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].SimCountry=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.operator.iso-country]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].Country=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.iso-country]", ": ", "[]");
                            if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.operator.isroaming]", ": ", "[]")=="true")
                                {ActiveDevice->Gsm.GSMSim[1].isRoaming=true;}
                            else
                                {ActiveDevice->Gsm.GSMSim[1].isRoaming=false;}
                            if (splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.roaming.indicator.needed]", ": ", "[]")=="true")
                                {ActiveDevice->Gsm.GSMSim[1].RoamingNeeded=true;}
                            else
                                {ActiveDevice->Gsm.GSMSim[1].RoamingNeeded=false;}
                            ActiveDevice->Gsm.GSMSim[1].BaseBand=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.project.baseband]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].rPin1=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.pin1]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].rPin2=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.pin2]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].rPuk1=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.puk1]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].rPuk2=splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.sim.retry.puk2]", ": ", "[]");
                            ActiveDevice->Gsm.GSMSim[1].CSNetworkType=getGSMNetworkType(splitNeedInfoFromRawInfo(rawSysInfo,"[gsm.cs.network.type]", ": ", "[]").toInt());
                            ActiveDevice->Gsm.GSMSim[1].iccid=splitNeedInfoFromRawInfo(rawSysInfo,"[ril.iccid.sim]", ": ", "[]");
                            if (ActiveDevice->Gsm.GSMSim[1].iccid=="")
                                {ActiveDevice->Gsm.GSMSim[1].iccid=splitNeedInfoFromRawInfo(rawSysInfo,"[ril.iccid.sim1]", ": ", "[]");}
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (ActiveDevice->DevStatus=="device" || ActiveDevice->DevStatus=="recovery")
        {
            rawSysInfo.clear();
            rawSysInfo=getInfobyCat(ActiveDevice, "/proc/cpuinfo");
            int Kernels;
            Kernels=0;
            for (int j=0;j<rawSysInfo.count();j++)
            {
                if (rawSysInfo.at(j).indexOf("Processor")!=-1)
                    {ActiveDevice->HWInfo.cpu.Processor=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("processor")!=-1)
                    {Kernels++;}
                if (rawSysInfo.at(j).indexOf("Features")!=-1)
                    {ActiveDevice->HWInfo.cpu.Features=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("CPU architecture")!=-1)
                    {ActiveDevice->HWInfo.cpu.cpuArch=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("CPU revision")!=-1)
                    {ActiveDevice->HWInfo.cpu.ProcRev=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("Hardware")!=-1)
                    {ActiveDevice->HWInfo.cpu.HardWare=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("Revision")!=-1)
                    {ActiveDevice->HWInfo.cpu.HardRev=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
                if (rawSysInfo.at(j).indexOf("Serial")!=-1)
                    {ActiveDevice->HWInfo.cpu.CpuSerials=BaseFunction->awk(rawSysInfo.at(j),":", -10).remove(0,1);}
            }
            if (Kernels!=0)
                {ActiveDevice->HWInfo.cpu.KernelsOnL=QVariant(Kernels).toString();}
            else
                {ActiveDevice->HWInfo.cpu.KernelsOnL="";}
            rawSysInfo.clear();
            rawSysInfo=getInfobyCat(ActiveDevice, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
            if (rawSysInfo.count()>0)
            {
                if (rawSysInfo.first().indexOf("No such file or directory")==-1)
                    {ActiveDevice->HWInfo.cpu.cpuFreq=rawSysInfo.first();}
            }
            rawSysInfo.clear();
            rawSysInfo << "/sys/devices/system/cpu";
            rawSysInfo=appProcess->adbCMD(appCmd->adb.listDir, rawSysInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
            Kernels=0;
            for (int i=0;i<32;i++)
            {
                bool newKrln;
                newKrln=false;
                for (int j=0;j<rawSysInfo.count();j++)
                {
                    if (rawSysInfo.at(j).indexOf("cpu"+QVariant(i).toString())!=-1)
                    {
                        Kernels++;
                        newKrln=true;
                        break;
                    }
                }
                if (!newKrln)
                    {break;}
            }
            ActiveDevice->HWInfo.cpu.Kernels=QVariant(Kernels).toString();
            rawSysInfo.clear();
            rawSysInfo=getInfobyCat(ActiveDevice, "/proc/meminfo");
            ActiveDevice->HWInfo.Memory.TotalMem=splitMemInfo(rawSysInfo, "MemTotal");
            ActiveDevice->HWInfo.Memory.FreeMem=splitMemInfo(rawSysInfo, "MemFree");
            ActiveDevice->HWInfo.Memory.CachedMem=splitMemInfo(rawSysInfo, "Cached");
            ActiveDevice->HWInfo.Memory.TotalSwap=splitMemInfo(rawSysInfo, "SwapTotal");
            ActiveDevice->HWInfo.Memory.FreeSwap=splitMemInfo(rawSysInfo, "SwapFree");
            ActiveDevice->HWInfo.Memory.CachedSwap=splitMemInfo(rawSysInfo, "SwapCached");
            GetDisplayInfo(ActiveDevice);
            GetCameraInfo(ActiveDevice);
        }
    }
}

void getInfo::getBatteryInfo(TSummaryInfo *ActiveDevice)
{
    QStringList rawBatteryInfo;
    rawBatteryInfo=getBatteryInfoRaw(ActiveDevice);
    int tmpInt;
    QStringList Param;
    QString tmpStr;
    if (rawBatteryInfo.count()>0 && rawBatteryInfo.first()!="Can't find service: battery")
    {
        if (!splitNeedInfoFromRawInfo(rawBatteryInfo, "AC powered", ":").isEmpty())
            {ActiveDevice->DevBattery.ac=getPowerCoonectionInfo(splitNeedInfoFromRawInfo(rawBatteryInfo, "AC powered", ":").split(" ",QString::SkipEmptyParts).first());}
        else
            {ActiveDevice->DevBattery.ac=0;}
        if (!splitNeedInfoFromRawInfo(rawBatteryInfo, "USB powered", ":").isEmpty())
            {ActiveDevice->DevBattery.usb=getPowerCoonectionInfo(splitNeedInfoFromRawInfo(rawBatteryInfo, "USB powered", ":").split(" ",QString::SkipEmptyParts).first());}
        else
            {ActiveDevice->DevBattery.usb=0;}
        if (!splitNeedInfoFromRawInfo(rawBatteryInfo, "Wireless powered", ":").isEmpty())
            {ActiveDevice->DevBattery.wireless=getPowerCoonectionInfo(splitNeedInfoFromRawInfo(rawBatteryInfo, "Wireless powered", ":").split(" ",QString::SkipEmptyParts).first());}
        else
            {ActiveDevice->DevBattery.wireless=0;}
        tmpInt=splitNeedInfoFromRawInfo(rawBatteryInfo, "status", ":").split(" ",QString::SkipEmptyParts).first().toInt();
        switch (tmpInt)
        {
            case 1:
                {ActiveDevice->DevBattery.bat_Status=tr("Unknown");}
            break;
            case 2:
                {ActiveDevice->DevBattery.bat_Status=tr("Charging");}
            break;
            case 3:
                {ActiveDevice->DevBattery.bat_Status=tr("Discharging");}
            break;
            case 4:
                {ActiveDevice->DevBattery.bat_Status=tr("Not charging");}
            break;
            case 5:
                {ActiveDevice->DevBattery.bat_Status=tr("Full");}
            break;
            default:
                {ActiveDevice->DevBattery.bat_Status=tr("");}
            break;
        }
        tmpInt=splitNeedInfoFromRawInfo(rawBatteryInfo, "health", ":").split(" ",QString::SkipEmptyParts).first().toInt();
        switch (tmpInt)
        {
            case 1:
                {ActiveDevice->DevBattery.bat_Health=tr("Unknown");}
            break;
            case 2:
                {ActiveDevice->DevBattery.bat_Health=tr("Good");}
            break;
            case 3:
                {ActiveDevice->DevBattery.bat_Health=tr("Overhead");}
            break;
            case 4:
                {ActiveDevice->DevBattery.bat_Health=tr("Dead");}
            break;
            case 5:
                {ActiveDevice->DevBattery.bat_Health=tr("Over Voltage");}
            break;
            case 6:
                {ActiveDevice->DevBattery.bat_Health=tr("Unspecified Failure");}
            break;
            case 7:
                {ActiveDevice->DevBattery.bat_Health=tr("Over Cold");}
            break;
            default:
                {ActiveDevice->DevBattery.bat_Health=tr("");}
            break;
        }
        ActiveDevice->DevBattery.bat_Capacity=splitNeedInfoFromRawInfo(rawBatteryInfo, "level", ":").split(" ",QString::SkipEmptyParts).first();
        ActiveDevice->DevBattery.bat_Scale=splitNeedInfoFromRawInfo(rawBatteryInfo, "scale", ":").split(" ",QString::SkipEmptyParts).first();
        ActiveDevice->DevBattery.bat_Voltage=splitNeedInfoFromRawInfo(rawBatteryInfo, "voltage", ":").split(" ",QString::SkipEmptyParts).first();
        ActiveDevice->DevBattery.bat_Temp=splitNeedInfoFromRawInfo(rawBatteryInfo, "temperature", ":").split(" ",QString::SkipEmptyParts).first();
        ActiveDevice->DevBattery.bat_Technology=splitNeedInfoFromRawInfo(rawBatteryInfo, "technology", ":").split(" ",QString::SkipEmptyParts).first();
        Param.clear();
        tmpStr.clear();
        Param << "/sys/class/power_supply/battery/ChargerVoltage";
        tmpStr=appProcess->adbCMD(appCmd->adb.cat, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber).last();
        if (tmpStr.indexOf("No such file or directory")==-1)
            {ActiveDevice->DevBattery.bat_ChargeVoltage=tmpStr;}
        else
            {ActiveDevice->DevBattery.bat_ChargeVoltage="";}
        Param.clear();
        tmpStr.clear();
        Param << "/sys/devices/platform/battery/Charger_TopOff_Value";
        tmpStr=appProcess->adbCMD(appCmd->adb.cat, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber).last();
        if (tmpStr.indexOf("No such file or directory")==-1)
            {ActiveDevice->DevBattery.bat_Voltage_Max=tmpStr;}
        else
            {ActiveDevice->DevBattery.bat_Voltage_Max="";}
        Param.clear();
        tmpStr.clear();
        Param << "/sys/devices/platform/battery/Power_Off_Voltage";
        tmpStr=appProcess->adbCMD(appCmd->adb.cat, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber).last();
        if (tmpStr.indexOf("No such file or directory")==-1)
            {ActiveDevice->DevBattery.bat_Voltage_Min=tmpStr;}
        else
            {ActiveDevice->DevBattery.bat_Voltage_Min="";}
    }
    if (ActiveDevice->DevBattery.bat_Capacity.isEmpty() || ActiveDevice->DevBattery.bat_Health.isEmpty() || ActiveDevice->DevBattery.bat_Status.isEmpty() || ActiveDevice->DevBattery.bat_ChargeVoltage.isEmpty() ||
            ActiveDevice->DevBattery.bat_Technology.isEmpty() || ActiveDevice->DevBattery.bat_Temp.isEmpty() || ActiveDevice->DevBattery.bat_Voltage_Max.isEmpty() || ActiveDevice->DevBattery.bat_Voltage_Min.isEmpty())
    {
        rawBatteryInfo=getBatteryInfoCRaw(ActiveDevice);
        if (ActiveDevice->DevBattery.bat_Capacity.isEmpty())
            {ActiveDevice->DevBattery.bat_Capacity=splitNeedInfoFromRawInfo(rawBatteryInfo, "CAPACITY", "=");}
        if (ActiveDevice->DevBattery.bat_Health.isEmpty())
            {ActiveDevice->DevBattery.bat_Health=splitNeedInfoFromRawInfo(rawBatteryInfo, "HEALTH", "=");}
        ActiveDevice->DevBattery.bat_Name=splitNeedInfoFromRawInfo(rawBatteryInfo, "NAME", "=");
        if (ActiveDevice->DevBattery.bat_Status.isEmpty())
            {ActiveDevice->DevBattery.bat_Status=splitNeedInfoFromRawInfo(rawBatteryInfo, "STATUS", "=");}
        if (ActiveDevice->DevBattery.bat_Technology.isEmpty())
            {ActiveDevice->DevBattery.bat_Technology=splitNeedInfoFromRawInfo(rawBatteryInfo, "TECHNOLOGY", "=");}
        if (ActiveDevice->DevBattery.bat_Temp.isEmpty())
            {ActiveDevice->DevBattery.bat_Temp=splitNeedInfoFromRawInfo(rawBatteryInfo, "TEMP", "=");}
        ActiveDevice->DevBattery.bat_Type=splitNeedInfoFromRawInfo(rawBatteryInfo, "TYPE", "=");
        //ActiveDevice->DevBattery.bat_Voltage.clear();
        //if (ActiveDevice->DevBattery.bat_Voltage.isEmpty())
        //{
        int i;
        QStringList BatVoltage;
        BatVoltage << "VOLTAGE_NOW" << "BATT_VOL" << "VOLTAGE" << "VOL";
        i=0;
        while (ActiveDevice->DevBattery.bat_Voltage=="" && i<BatVoltage.count())
        {
            ActiveDevice->DevBattery.bat_Voltage=splitNeedInfoFromRawInfo(rawBatteryInfo, BatVoltage.at(i), "=");
            i++;
        }
        //}
        if (ActiveDevice->DevBattery.bat_ChargeVoltage.isEmpty())
        ActiveDevice->DevBattery.bat_ChargeVoltage=splitNeedInfoFromRawInfo(rawBatteryInfo, "CHARGERVOLTAGE", "=");
        if (ActiveDevice->DevBattery.bat_Voltage_Max.isEmpty())
            {ActiveDevice->DevBattery.bat_Voltage_Max=splitNeedInfoFromRawInfo(rawBatteryInfo, "VOLTAGE_MAX", "=");}
        if (ActiveDevice->DevBattery.bat_Voltage_Min.isEmpty())
            {ActiveDevice->DevBattery.bat_Voltage_Min=splitNeedInfoFromRawInfo(rawBatteryInfo, "VOLTAGE_MIN", "=");}
    }
    if (!ActiveDevice->DevBattery.bat_Temp.isEmpty())
#if not defined(Q_OS_WIN)
        {ActiveDevice->DevBattery.bat_Temp.insert(ActiveDevice->DevBattery.bat_Temp.count()-1,".");}
#else
        {ActiveDevice->DevBattery.bat_Temp.insert(ActiveDevice->DevBattery.bat_Temp.count()-2,".");}
#endif
}

void getInfo::getDiskSpaceInfo(TSummaryInfo *ActiveDevice)
{
    ActiveDevice->DiskSpace.clear();
    QStringList rawDiskSpace;
    rawDiskSpace=getDiskSpaceInfoRaw(ActiveDevice);
    ActiveDevice->DiskSpace["system"]=splitDFLine(rawDiskSpace, "/system");
    ActiveDevice->DiskSpace["data"]=splitDFLine(rawDiskSpace, "/data");
    ActiveDevice->DiskSpace["cache"]=splitDFLine(rawDiskSpace, "/cache");
    ActiveDevice->DiskSpace["sdcard"]=splitDFLine(rawDiskSpace, "/sdcard", false, ActiveDevice->DevInfo.ProductNames.ExternalSD);
    ActiveDevice->DiskSpace["sdcard external"]=splitDFLine(rawDiskSpace, "/sdcard", true, ActiveDevice->DevInfo.ProductNames.ExternalSD);
    ActiveDevice->DiskSpace["root"]=splitDFLine(rawDiskSpace, "/dev");
    TDiskSpace diskSpace;
    diskSpace=splitDFLine(rawDiskSpace, "/cdrom");
    if (diskSpace.dfSize.isEmpty())
        {diskSpace=splitDFLine(rawDiskSpace, "/cd-rom");}
    if (!diskSpace.dfSize.isEmpty())
        {ActiveDevice->DiskSpace["cdrom"]=diskSpace;}
    diskSpace=splitDFLine(rawDiskSpace, "/protect_f");
    if (!diskSpace.dfSize.isEmpty())
        {ActiveDevice->DiskSpace["protect_f"]=diskSpace;}
    diskSpace=splitDFLine(rawDiskSpace, "/protect_s");
    if (!diskSpace.dfSize.isEmpty())
    {ActiveDevice->DiskSpace["protect_s"]=diskSpace;}
}

void getInfo::getNetCfgInfo(TSummaryInfo *ActiveDevice)
{
    QStringList rawNetCfg;
    ActiveDevice->netIF.clear();
    rawNetCfg=getNetCfgInfoRaw(ActiveDevice);
    if (rawNetCfg.count()>0)
    {
        for (int i=0;i<rawNetCfg.count();i++)
        {
            splitIfInfo(rawNetCfg, "Ethernet", "eth", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "WIFI", "wlan", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Mobile", "ccmni", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Mobile PPP", "ppp", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "IFB", "ifb", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Bridge", "br", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Point2Point", "p2p", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Tunnel", "tunl", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "IPv6 Tunneling", "ip6tnl", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "SIT", "sit", i, ActiveDevice);
            splitIfInfo(rawNetCfg, "Local", "lo", i, ActiveDevice);
        }
        if (!ActiveDevice->DevInfo.ProductNetInfo.wifiOn)
        {
            ActiveDevice->netIF[ActiveDevice->DevInfo.ProductNetInfo.wifiIf].IfType="WIFI";
            ActiveDevice->netIF[ActiveDevice->DevInfo.ProductNetInfo.wifiIf].NetIf=ActiveDevice->DevInfo.ProductNetInfo.wifiIf;
            ActiveDevice->netIF[ActiveDevice->DevInfo.ProductNetInfo.wifiIf].isUP=false;
        }
    }
}

/********************************************* General Info additional functions *********************************************/

void getInfo::GetDisplayInfo(TSummaryInfo *ActiveDevice)
{
    QStringList rawServicesInfo;
    rawServicesInfo << "display";
    rawServicesInfo=appProcess->adbCMD(appCmd->adb.dumpsys, rawServicesInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    //QStringList DispInfo;
    for (int j=0;j<rawServicesInfo.count();j++)
    {
        if (rawServicesInfo.at(j).indexOf("Logical Displays: size")!=-1)
        {
            int i;
            int DisName;
            DisName=0;
            i=j;
            while (i<rawServicesInfo.count() && rawServicesInfo.at(i).indexOf("mOverrideDisplayInfo=")==-1)
            {
                if (rawServicesInfo.at(i).indexOf("mDisplayId")!=-1)
                {
                    DisName=rawServicesInfo.at(i).split("=",QString::SkipEmptyParts).last().toInt();
                }
                if (rawServicesInfo.at(i).indexOf("mPrimaryDisplayDevice")!=-1)
                {
                    ActiveDevice->HWInfo.DispleyHW[DisName].DisplayName=rawServicesInfo.at(i).split("=",QString::SkipEmptyParts).last();
                }
                i++;
            }
            QStringList ODI;
            ODI=rawServicesInfo.at(i).split(",",QString::SkipEmptyParts);
            for (int i=0;i<ODI.count();i++)
            {
                if (ODI.at(i).indexOf("app")!=-1 && ODI.at(i).indexOf("app")<2)
                {
                    QStringList tmpLst;
                    tmpLst=ODI.at(i).split(" ",QString::SkipEmptyParts);
                    if (tmpLst.count()>3)
                    {
                        ActiveDevice->HWInfo.DispleyHW[DisName].appDisplay.ResolurionX=tmpLst.at(tmpLst.count()-3);
                        ActiveDevice->HWInfo.DispleyHW[DisName].appDisplay.ResolurionY=tmpLst.at(tmpLst.count()-1);
                    }
                }
                if (ODI.at(i).indexOf("real")!=-1 && ODI.at(i).indexOf("real")<2)
                {
                    QStringList tmpLst;
                    tmpLst=ODI.at(i).split(" ",QString::SkipEmptyParts);
                    if (tmpLst.count()>3)
                    {
                        ActiveDevice->HWInfo.DispleyHW[DisName].realDisplay.ResolurionX=tmpLst.at(tmpLst.count()-3);
                        ActiveDevice->HWInfo.DispleyHW[DisName].realDisplay.ResolurionY=tmpLst.at(tmpLst.count()-1);
                    }
                }
                if (ODI.at(i).indexOf("largest app")!=-1)
                {
                    QStringList tmpLst;
                    tmpLst=ODI.at(i).split(" ",QString::SkipEmptyParts);
                    if (tmpLst.count()>3)
                    {
                        ActiveDevice->HWInfo.DispleyHW[DisName].LappDisplay.ResolurionX=tmpLst.at(tmpLst.count()-3);
                        ActiveDevice->HWInfo.DispleyHW[DisName].LappDisplay.ResolurionY=tmpLst.at(tmpLst.count()-1);
                    }
                }
                if (ODI.at(i).indexOf("smallest app")!=-1)
                {
                    QStringList tmpLst;
                    tmpLst=ODI.at(i).split(" ",QString::SkipEmptyParts);
                    if (tmpLst.count()>3)
                    {
                        ActiveDevice->HWInfo.DispleyHW[DisName].SappDisplay.ResolurionX=tmpLst.at(tmpLst.count()-3);
                        ActiveDevice->HWInfo.DispleyHW[DisName].SappDisplay.ResolurionY=tmpLst.at(tmpLst.count()-1);
                    }
                }
                if (ODI.at(i).indexOf("fps")!=-1)
                    {ActiveDevice->HWInfo.DispleyHW[DisName].fpsDispley=ODI.at(i).split(" ",QString::SkipEmptyParts).first();}
                if (ODI.at(i).indexOf("rotation")!=-1)
                {
                    if (ODI.at(i).split(" ",QString::SkipEmptyParts).count()>1)
                    {
                        int intRotation;
                        intRotation=ODI.at(i).split(" ",QString::SkipEmptyParts).last().toInt();
                        switch (intRotation) {
                        case 0:
                            {ActiveDevice->HWInfo.DispleyHW[DisName].rotation=tr("Top");}
                            break;
                        case 1:
                            {ActiveDevice->HWInfo.DispleyHW[DisName].rotation=tr("Right");}
                            break;
                        case 2:
                            {ActiveDevice->HWInfo.DispleyHW[DisName].rotation=tr("Bottom");}
                            break;
                        case 3:
                            {ActiveDevice->HWInfo.DispleyHW[DisName].rotation=tr("Left");}
                            break;
                        default:
                            break;
                        }
                    }
                    else
                        {ActiveDevice->HWInfo.DispleyHW[DisName].rotation=QVariant(ODI.at(i).at(ODI.at(i).count()-1)).toString();}
                }
                if (ODI.at(i).indexOf("density")!=-1 && ODI.at(i).indexOf("dpi")!=-1)
                {
                    ActiveDevice->HWInfo.DispleyHW[DisName].density=ODI.at(i).split("(",QString::SkipEmptyParts).first().split(" ",QString::SkipEmptyParts).last();
                    ActiveDevice->HWInfo.DispleyHW[DisName].densityXY.ResolurionX=ODI.at(i).split("(",QString::SkipEmptyParts).last().split(")",QString::SkipEmptyParts).first().split(" ",QString::SkipEmptyParts).first();
                    ActiveDevice->HWInfo.DispleyHW[DisName].densityXY.ResolurionY=ODI.at(i).split("(",QString::SkipEmptyParts).last().split(")",QString::SkipEmptyParts).first().split(" ",QString::SkipEmptyParts).last();
                }
                if (ODI.at(i).indexOf("density")!=-1 && ODI.at(i).indexOf("dpi")==-1)
                    {ActiveDevice->HWInfo.DispleyHW[DisName].density=ODI.at(i).split(" ",QString::SkipEmptyParts).last();}
                if (ODI.at(i).indexOf("density")==-1 && ODI.at(i).indexOf("dpi")!=-1)
                {
                    if (ODI.at(i).split(" ",QString::SkipEmptyParts).count()>3)
                    {
                        ActiveDevice->HWInfo.DispleyHW[DisName].densityXY.ResolurionX=ODI.at(i).split(" ",QString::SkipEmptyParts).first();
                        ActiveDevice->HWInfo.DispleyHW[DisName].densityXY.ResolurionY=ODI.at(i).split(" ",QString::SkipEmptyParts).at(2);
                    }
                }
            }
        }
    }
}

void getInfo::GetCameraInfo(TSummaryInfo *ActiveDevice)
{
    QStringList rawServicesInfo;
    rawServicesInfo << "media.camera";
    ActiveDevice->HWInfo.cameraHW.camDevices=0;
    rawServicesInfo=appProcess->adbCMD(appCmd->adb.dumpsys, rawServicesInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    for (int j=0;j<rawServicesInfo.count();j++)
    {
        if (rawServicesInfo.at(j).indexOf("Camera module name")!=-1)
            {ActiveDevice->HWInfo.cameraHW.CameraModule=rawServicesInfo.at(j).split("Camera module name: ",QString::SkipEmptyParts).last();}
        if (rawServicesInfo.at(j).indexOf("Camera module author")!=-1)
            {ActiveDevice->HWInfo.cameraHW.Vendor=rawServicesInfo.at(j).split("Camera module author: ",QString::SkipEmptyParts).last();}
        if (rawServicesInfo.at(j).indexOf("Number of camera devices")!=-1)
            {ActiveDevice->HWInfo.cameraHW.camDevices=rawServicesInfo.at(j).split("Number of camera devices: ",QString::SkipEmptyParts).last().toInt();}
    }
    if (ActiveDevice->HWInfo.cameraHW.camDevices==0)
    {
        int m;
        m=0;
        for (int j=0;j<rawServicesInfo.count();j++)
        {
            if (rawServicesInfo.at(j).indexOf("Camera ")!=-1 && rawServicesInfo.at(j).indexOf("static information")!=-1)
                {m++;}
        }
        ActiveDevice->HWInfo.cameraHW.camDevices=m;
    }
    int m;
    m=0;
    for (int i=0;i<ActiveDevice->HWInfo.cameraHW.camDevices;i++)
    {
        for (int j=m;j<rawServicesInfo.count();j++)
        {
            if (rawServicesInfo.at(j).indexOf("Camera "+QVariant(i).toString()+" static information:")!=-1)
            {
                if (rawServicesInfo.at(j+1).indexOf("Facing")!=-1)
                    {ActiveDevice->HWInfo.cameraHW.Camera[i].Facing=rawServicesInfo.at(j+1).split("Facing: ").last();}
                if (rawServicesInfo.at(j+2).indexOf("Orientation")!=-1)
                    {ActiveDevice->HWInfo.cameraHW.Camera[i].Orientation=rawServicesInfo.at(j+2).split("Orientation: ").last();}
                if (rawServicesInfo.at(j+4).indexOf("Device is open")!=-1)
                    {ActiveDevice->HWInfo.cameraHW.Camera[i].isOpen=true;}
                else
                    {ActiveDevice->HWInfo.cameraHW.Camera[i].isOpen=false;}
                m=j+4;
                break;
            }
        }
    }
}

void getInfo::switchWifiOnOff(TSummaryInfo *ActiveDevice, QStringList Param)
{
    Param=appProcess->adbCMD(appCmd->adb.svc, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
}

/********************************************* raw info read functions *********************************************/
QStringList getInfo::getDevSysInfoRaw(TSummaryInfo *ActiveDevice)
{
    QStringList rawSysInfo;
    if (ActiveDevice->DevStatus=="device")
        {rawSysInfo=appProcess->adbCMD(appCmd->adb.sysinfo, rawSysInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);}
    else
    {
        if (ActiveDevice->DevStatus=="recovery")
        {
            rawSysInfo << "/tmp/recovery.log";
            rawSysInfo=appProcess->adbCMD(appCmd->adb.cat, rawSysInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
        }
        if (ActiveDevice->DevStatus=="fastboot")
        {
            rawSysInfo.clear();
            rawSysInfo=appProcess->fbCMD(appCmd->fastboot.getvar, rawSysInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
        }
        if (ActiveDevice->DevStatus=="unauthorized")
        {
            QMessageBox::warning(0,QObject::tr("This PC was not authorized for debug from you device!"),tr("Please authorize this computer for debugging on your device."));
            rawSysInfo << "unauthorized";
        }
    }
    return(rawSysInfo);
}

inline QStringList getInfo::getGSMModulesServicesInfo(TSummaryInfo *ActiveDevice, QString SerName, int serCount)
{
    QStringList rawServicesInfo;
    for (int i=0;i<=serCount;i++)
    {
        QStringList inpCMD;
        if (i!=0)
            {inpCMD << SerName+QVariant(i).toString();}
        else
            {inpCMD << SerName;}
        inpCMD=appProcess->adbCMD(appCmd->adb.dumpsys, inpCMD, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
        if (inpCMD.count()>1  && inpCMD.last().indexOf("null")==-1 && inpCMD.first().indexOf("Can't find service")==-1)
        {
            rawServicesInfo<<inpCMD;
            if (i==0)
                {i++;}
        }
        else
        {
            if (i!=0)
                {break;}
        }
    }
    return(rawServicesInfo);
}

inline QStringList getInfo::getInfobyCat(TSummaryInfo *ActiveDevice, QString catFile)
{
    QStringList rawCatInfo;
    rawCatInfo << catFile;
    rawCatInfo=appProcess->adbCMD(appCmd->adb.cat, rawCatInfo, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    return(rawCatInfo);
}

inline QStringList getInfo::getBatteryInfoRaw(TSummaryInfo *ActiveDevice)
{
    QStringList rawBatteryInfo;
    QStringList Param;
    Param << "battery";
    rawBatteryInfo=appProcess->adbCMD(appCmd->adb.dumpsys, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    return(rawBatteryInfo);
}

inline QStringList getInfo::getBatteryInfoCRaw(TSummaryInfo *ActiveDevice)
{
    QStringList BattaryStat;
    QStringList rawBatteryInfo;
    QStringList Param;
    BattaryStat << "/sys/class/power_supply/battery/uevent" << "sys/class/power_supply/MainBattery/uevent";
    int i;
    rawBatteryInfo.clear();
    i=0;
    while (rawBatteryInfo.count()==0 && i<BattaryStat.count())
    {
        Param.clear();
        Param.append(BattaryStat.at(i));
        rawBatteryInfo=appProcess->adbCMD(appCmd->adb.cat, Param, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
        if (rawBatteryInfo.first().indexOf("No such file or directory")!=-1)
            {rawBatteryInfo.clear();}
        i++;
    }
    return(rawBatteryInfo);
}

QStringList getInfo::getDiskSpaceInfoRaw(TSummaryInfo *ActiveDevice)
{
    QStringList rawDiskSpace;
    rawDiskSpace=appProcess->adbCMD(appCmd->adb.diskspace, rawDiskSpace, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    return(rawDiskSpace);
}

QStringList getInfo::getNetCfgInfoRaw(TSummaryInfo *ActiveDevice)
{
    QStringList rawNetCfg;
    rawNetCfg=appProcess->adbCMD(appCmd->adb.netcfg,rawNetCfg, ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber);
    return(rawNetCfg);
}


/********************************************* split functions *********************************************/

QString getInfo::splitNeedInfoFromRawInfo(QStringList StrList, QString SplStr, QString splitStr, QString OtSplitStr)
{
    QString Str;
    for (int i=0;i<StrList.count();i++)
    {
        if (StrList.at(i).indexOf(SplStr)!=-1)
        {
            Str=StrList.at(i).split(splitStr,QString::SkipEmptyParts).last();
            break;
        }
    }
    if (!Str.isEmpty())
    {
        if (Str=="[]")
            {Str="";}
        if (OtSplitStr=="[]" && Str!="")
        //    {Str=Str.split("[",QString::SkipEmptyParts).last().split("]",QString::SkipEmptyParts).first();}
            {Str=BaseFunction->awk(BaseFunction->awk(Str,"[",-10),"]",-1);}
        //else
            //{Str=BaseFuncunction.awk(Str,"=",-10);}
    }
    return(Str);
}

inline int getInfo::getPowerCoonectionInfo(QString inputStatus)
{
    int outputStatus;
    if (!inputStatus.isEmpty())
    {
        if (inputStatus=="true")
            {outputStatus=2;}
        else
            {outputStatus=1;}
    }
    else
        {outputStatus=0;}
    return(outputStatus);
}

inline int getInfo::splitMemInfo(QStringList strList, QString str)
{
    int memSize;
    memSize=0;
    for (int i=0;i<strList.count();i++)
    {
        if (strList.at(i).indexOf(str)==0)
        {
            QStringList memValueList;
            memValueList=strList.at(i).split(" ",QString::SkipEmptyParts);
            if (memValueList.count()>2)
            {
                if (memValueList.at(2)=="kB" || memValueList.at(2)=="KB" || memValueList.at(2)=="kb")
                    {memSize=memValueList.at(1).toDouble()*1000;}
                if (memValueList.at(2)=="mB" || memValueList.at(2)=="MB" || memValueList.at(2)=="mb")
                    {memSize=memValueList.at(1).toDouble()*1000000;}
                if (memValueList.at(2)=="gB" || memValueList.at(2)=="GB" || memValueList.at(2)=="gb")
                    {memSize=memValueList.at(1).toDouble()*1000000000;}
                if (memValueList.at(2)=="B" || memValueList.at(2)=="b")
                    {memSize=memValueList.at(1).toDouble();}
            }
            else
                {memSize=memValueList.at(1).toDouble();}
        }
    }
    return(memSize);
}

TDiskSpace getInfo::splitDFLine(QStringList rawList, QString mountPoint, bool sdEx, QString DevPath)
{
    TDiskSpace dfInfo;
    QStringList infoDF;
    for (int i=0;i<rawList.count();i++)
    {
        if (rawList.at(i).indexOf(mountPoint)!=-1)
        {
            if (mountPoint!="/sdcard")
                {infoDF.append(rawList.at(i).split(" ",QString::SkipEmptyParts));}
            else
            {
                if (sdEx)
                {
                    if (((DevPath=="" && (rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("1")!=-1 ||
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("external")!=-1 ||
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("extSdCard")!=-1)) ||
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0)==DevPath) &&
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(1).indexOf("Permission")==-1)
                    {
                        QString tmpStr;
                        tmpStr=rawList.at(i).split(" ",QString::SkipEmptyParts).at(1);
                        infoDF.append(rawList.at(i).split(" ",QString::SkipEmptyParts));
                    }
                }
                else
                {
                    if (((DevPath=="" && rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("1")==-1 &&
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("external")==-1 &&
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0).indexOf("extSdCard")==-1) ||
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(0)!=DevPath) &&
                        rawList.at(i).split(" ",QString::SkipEmptyParts).at(1).indexOf("Permission")==-1)
                        {infoDF.append(rawList.at(i).split(" ",QString::SkipEmptyParts));}
                }
            }
        }
    }
    if (infoDF.count()>=4)
    {
        int i;
        int j;
        if (infoDF.at(2)!="*")
            {i=1;}
        else
            {i=2;}
        if (infoDF.at(2).indexOf("total")==-1)
            {j=1;}
        else
            {j=2;}
        dfInfo.dfSize=infoDF.at(i);
        dfInfo.dfUsed=infoDF.at(i+j);
        dfInfo.dfFree=infoDF.at(i+2*j);
    }
    return(dfInfo);
}

void getInfo::splitIfInfo(QStringList rawNetCfg, QString IfName, QString shortIfName, int strLine, TSummaryInfo *ActiveDevice)
{
    if (shortIfName!="lo")
    {
        for (int j=0;j<5;j++)
        {
            if (rawNetCfg.at(strLine).indexOf(shortIfName+QVariant(j).toString())!=-1)
            {
                ActiveDevice->netIF[shortIfName+QVariant(j).toString()].IfType=IfName;
                ActiveDevice->netIF[shortIfName+QVariant(j).toString()].NetIf=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(0);
                if (rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(1)=="UP")
                    {ActiveDevice->netIF[shortIfName+QVariant(j).toString()].isUP=true;}
                else
                    {ActiveDevice->netIF[shortIfName+QVariant(j).toString()].isUP=false;}
                if (rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).indexOf("/")!=-1)
                {
                    ActiveDevice->netIF[shortIfName+QVariant(j).toString()].IP=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).split("/").at(0);
                    ActiveDevice->netIF[shortIfName+QVariant(j).toString()].netMask=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).split("/").at(1).toInt();
                }
                else
                {
                    ActiveDevice->netIF[shortIfName+QVariant(j).toString()].IP=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2);
                    ActiveDevice->netIF[shortIfName+QVariant(j).toString()].netMask=-1;
                }
                ActiveDevice->netIF[shortIfName+QVariant(j).toString()].MAC=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(4);
            }
        }
    }
    else
    {
        if (rawNetCfg.at(strLine).indexOf(shortIfName)!=-1)
        {
            ActiveDevice->netIF[shortIfName].IfType=IfName;
            ActiveDevice->netIF[shortIfName].NetIf=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(0);
            if (rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(1)=="UP")
                {ActiveDevice->netIF[shortIfName].isUP=true;}
            else
                {ActiveDevice->netIF[shortIfName].isUP=false;}
            if (rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).indexOf("/")!=-1)
            {
                ActiveDevice->netIF[shortIfName].IP=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).split("/").at(0);
                ActiveDevice->netIF[shortIfName].netMask=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2).split("/").at(1).toInt();
                ActiveDevice->netIF[shortIfName].MAC=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(4);
            }
            else
            {
                ActiveDevice->netIF[shortIfName].IP=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(2);
                //ActiveDevice->netIF[shortIfName].netMask=rawNetCfg.at(strLine).split(" ",QString::SkipEmptyParts).at(3).split("/").at(1).toInt();
            }
        }
    }
}


/********************************************* additional functions *********************************************/
inline QString getInfo::getGSMNetworkType(int typeVal)
{
    QString gsmNetworkType;
    switch (typeVal)
    {
        case 0:
            {gsmNetworkType="Unknown";}
        break;
        case 1:
            {gsmNetworkType="GPRS";}
        break;
        case 2:
            {gsmNetworkType="EDGE";}
        break;
        case 3:
            {gsmNetworkType="UMTS";}
        break;
        case 4:
            {gsmNetworkType="CDMA";}
        break;
        case 5:
            {gsmNetworkType="EVDO rev. 0";}
        break;
        case 6:
            {gsmNetworkType="EVDO rev. A";}
        break;
        case 7:
            {gsmNetworkType="1xRTT";}
        break;
        case 8:
            {gsmNetworkType="HSDPA";}
        break;
        case 9:
            {gsmNetworkType="HSUPA";}
        break;
        case 10:
            {gsmNetworkType="HSPA";}
        break;
        case 11:
            {gsmNetworkType="iDen";}
        break;
        case 12:
            {gsmNetworkType="EVDO rev. B";}
        break;
        case 13:
            {gsmNetworkType="LTE";}
        break;
        case 14:
            {gsmNetworkType="eHRPD";}
        break;
        case 15:
            {gsmNetworkType="HSPA+";}
        break;
        default:
        break;
    }
    return(gsmNetworkType);
}
