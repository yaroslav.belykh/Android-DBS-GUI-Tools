/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/screenshot.h"
#include "ui_screenshot.h"

Screenshot::Screenshot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Screenshot)
{
    ui->setupUi(this);

    ui->pB_saveScrenshot->setEnabled(false);
}

Screenshot::~Screenshot()
{
    delete ui;
}

void Screenshot::setCMDPointer(tCMD *newCmd, tAdbApp *newAdbApp, tDebug *inpDebug)
{
    adbApp=newAdbApp;
    procCmd=newCmd;
    doDebug=inpDebug;
}

void Screenshot::setObjectParam(TSummaryInfo *inpActiveDevice, QString inpTmpPath)
{
    ActiveDevice=inpActiveDevice;
    tmpPath=inpTmpPath;
}

void Screenshot::makescreenshot()
{
    thread = new QThread;
    //ScrThread = new ScreenshotThread(this, procCmd, adbApp, doDebug);
    ScrThread = new ScreenshotThread(0, procCmd, adbApp, doDebug);

    connect(ScrThread, SIGNAL(iamstarted()), this, SLOT(threadStarted()));
    connect(this, SIGNAL(getSreenshot(QString, QString, bool, QString)), ScrThread, SLOT(createScreenshot(QString, QString, bool, QString)));
    connect(ScrThread, SIGNAL(screenshotCreating(QString)), this, SLOT(screenshotCreating(QString)));

    connect(thread, SIGNAL(started()), ScrThread, SLOT(process()));
    connect(ScrThread, SIGNAL(finished()), thread, SLOT(quit()));
    connect(ScrThread, SIGNAL(finished()), ScrThread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    ScrThread->moveToThread(thread);
    thread->start();

    ui->pB_saveScrenshot->setEnabled(false);
    ui->pB_Screenshot->setEnabled(false);
}

QSize Screenshot::getGViewMinSize(QSize imSize, QSize wgxSize)
{
    int imSiX;
    int imSiY;
    int wgxSiX;
    int wgxSiY;
    QSize curentSize;
    imSiX=imSize.width();
    imSiY=imSize.height();
    wgxSiX=wgxSize.width()-234;
    wgxSiY=wgxSize.height()-28;
    if (imSiX<wgxSiX)
    {
        if (imSiY>wgxSiY)
            {curentSize.setWidth(imSiX+20);}
        else
            {curentSize.setWidth(imSiX);}
    }
    else
        {curentSize.setWidth(wgxSiX);}
    if (imSiY<wgxSiY)
    {
        if (imSiX>wgxSiX)
            {curentSize.setHeight(imSiY+20);}
        else
            {curentSize.setHeight(imSiY);}
    }
    else
        {curentSize.setHeight(wgxSiY);}
    return(curentSize);
}

QSize Screenshot::getGViewMaxSize(QSize imSize, QSize wgxSize)
{
    int imSiX;
    int imSiY;
    int wgxSiX;
    int wgxSiY;
    QSize curentSize;
    imSiX=imSize.width();
    imSiY=imSize.height();
    wgxSiX=wgxSize.width()-234;
    wgxSiY=wgxSize.height()-28;
    if (imSiY>wgxSiY)
        {curentSize.setWidth(imSiX+20);}
    else
        {curentSize.setWidth(imSiX);}
    if (imSiX>wgxSiX)
        {curentSize.setHeight(imSiY+20);}
    else
        {curentSize.setHeight(imSiY);}
    return(curentSize);
}

void Screenshot::threadStarted()
{
    emit getSreenshot(ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber, tmpPath+"/"+ActiveDevice->TempPath);
}

void Screenshot::screenshotCreating(QString scrName)
{

    pixMap.load(scrName);
    scrsScene.addPixmap(pixMap);
    ui->gV->setScene(&scrsScene);
    ui->gV->setMinimumSize(getGViewMinSize(pixMap.size(),this->size()));
    ui->gV->setMaximumSize(getGViewMaxSize(pixMap.size(),this->size()));

    /*disconnect(ScrThread, SIGNAL(iamstarted()), this, SLOT(threadStarted()));
    disconnect(this, SIGNAL(getSreenshot(QString, QString, bool, QString*)), ScrThread, SLOT(createScreenshot(QString, QString, bool, QString*)));
    disconnect(ScrThread, SIGNAL(screenshotCreating(QString)), this, SLOT(screenshotCreating(QString)));

    disconnect(thread, SIGNAL(started()), ScrThread, SLOT(process()));
    disconnect(ScrThread, SIGNAL(finished()), thread, SLOT(quit()));
    disconnect(ScrThread, SIGNAL(finished()), ScrThread, SLOT(deleteLater()));
    disconnect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));*/

    ScrThread=0;
    thread=0;
    ui->pB_saveScrenshot->setEnabled(true);
    ui->pB_Screenshot->setEnabled(true);
    ActiveDevice->Sreenshot=scrName;
}

void Screenshot::on_pB_Screenshot_clicked()
{
    makescreenshot();
}

void Screenshot::on_pB_saveScrenshot_clicked()
{
    QFile ScrFile;
    QString saveFileName;
    ScrFile.setFileName(ActiveDevice->Sreenshot);
    if (ScrFile.exists())
    {
        saveFileName=QFileDialog::getSaveFileName(this, tr("Save Screenshot"), QDir::homePath(), tr("PNG Image files ")+"( *.png )");
        if (!saveFileName.isEmpty())
        {
            bool OK;
            OK=ScrFile.copy(saveFileName+".png");
            if (!OK)
                {QMessageBox::critical(this,QObject::tr("Failure save sreenshot!"),tr("Screenshot was not save."));}
        }
        else
            {QMessageBox::critical(NULL,QObject::tr("Failure save sreenshot!"),tr("File name is empty."));}
    }
    else
        {QMessageBox::critical(NULL,QObject::tr("Failure save sreenshot!"),tr("Screenshot file is not exist. May be it was remove."));}
}
