/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/diskinfo.h"
#include "ui_diskinfo.h"

DiskInfoWGX::DiskInfoWGX(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::DiskInfoWGX)
{
    ui->setupUi(this);
    this->setLayout(ui->hl_DiskInfoWGX);
    ui->diCached->setVisible(false);
    ui->lbl_DI_Cahce->setVisible(false);
}

DiskInfoWGX::~DiskInfoWGX()
{
    delete ui;
}

void DiskInfoWGX::setName(QString dValue)
{
    ui->diName->setText(dValue);
}

void DiskInfoWGX::setSUFValue(QString dSize, QString dUsed, QString dFree)
{
    ui->diSize->setText(dSize);
    ui->diUsed->setText(dUsed);
    ui->diFree->setText(dFree);
}

void DiskInfoWGX::setCachedValue(QString dValue)
{
    ui->diCached->setVisible(true);
    ui->lbl_DI_Cahce->setVisible(true);
    ui->diCached->setText(dValue);
}

void DiskInfoWGX::setValue(int DMax, int DValue)
{
    ui->diPBar->setMinimum(0);
    ui->diPBar->setMaximum(DMax);
    ui->diPBar->setValue(DValue);
}

void DiskInfoWGX::setClear()
{
    ui->diPBar->setMinimum(0);
    ui->diPBar->setMaximum(100);
    ui->diPBar->setValue(0);
    ui->diSize->clear();
    ui->diUsed->clear();
    ui->diFree->clear();
}

