/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/simcount.h"
#include "ui_simcount.h"

simCount::simCount(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::simCount)
{
    ui->setupUi(this);
}

simCount::~simCount()
{
    delete ui;
}

void simCount::addItem(int sim, bool isActive)
{
    countSimWGX[sim]=new doubleLabel(this);
    countSimWGX[sim]->setObjectName(QStringLiteral("Sim")+QVariant(sim).toString());
    if (isActive)
        {countSimWGX[sim]->setValue(QVariant(sim).toString()+" - ",QString::fromUtf8("Green_dot_on"));}
    else
        {countSimWGX[sim]->setValue(QVariant(sim).toString()+" - ",QString::fromUtf8("Green_dot_off"));}
    countSimWGX[sim]->setLabelSizeMaximum(24,24);
    countSimWGX[sim]->setSizeMaximum(70,70);
    ui->hl_SIM->insertWidget(sim, countSimWGX[sim]);
}
