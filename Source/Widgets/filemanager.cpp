/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/filemanager.h"
#include "ui_filemanager.h"

FileManager::FileManager(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileManager)
{
    ui->setupUi(this);

    FM1 = new FileManagerTab(this);
    FM1->setObjectName(QStringLiteral("FM_Tab1"));
    ui->HL_FMTabs->addWidget(FM1);
    FM2 = new FileManagerTab(this);
    FM2->setObjectName(QStringLiteral("FM_Tab2"));
    ui->HL_FMTabs->addWidget(FM2);
}

FileManager::~FileManager()
{
    delete ui;
}

void FileManager::setCMDPointer(tCMD *newCmd, tAdbApp *newAdbApp, tDebug *inpDebug)
{
    adbApp=newAdbApp;
    procCmd=newCmd;
    doDebug=inpDebug;
}

void FileManager::setObjectParam(TSummaryInfo *inpActiveDevice)
{
    ActiveDevice=inpActiveDevice;
}
