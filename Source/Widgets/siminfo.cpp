/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/siminfo.h"
#include "ui_siminfo.h"

simInfo::simInfo(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::simInfo)
{
    ui->setupUi(this);
}

simInfo::~simInfo()
{
    delete ui;
}

void simInfo::setSimName(int simNumber, bool isActive, bool isRoaming, QString iccid)
{
    QString simNumberStr;
    simNumberStr.clear();
    if (simNumber>0)
        {simNumberStr=QVariant(simNumber).toString();}
    ui->SIM->setText("SIM"+simNumberStr+":");
    if (isActive)
        {ui->SIM_ReadyValue->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_on.png")));}
    else
        {ui->SIM_ReadyValue->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));}
    if (isRoaming)
    {ui->isRoamingValue->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_on.png")));}
else
    {ui->isRoamingValue->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));}
    ui->ICCID1Value->setText(iccid);
}

void simInfo::setSimInfo(QString sType, QString sOperator, QString sCountry, QString iType, QString iOperator, QString iCountry)
{
    ui->SimType_Value->setText(sType);
    ui->SimOperator_Value->setText(sOperator);
    ui->SimCountry_Value->setText(sCountry);
    ui->NetWorkType_Value->setText(iType);
    ui->Operator_Value->setText(iOperator);
    ui->Country_Value->setText(iCountry);
}

void simInfo::setPinPukInfo(QString pin1, QString pin2, QString puk1, QString puk2)
{
    ui->PIN1Retry_Value->setText(pin1);
    ui->PIN2Retry_Value->setText(pin2);
    ui->PUK1Retry_Value->setText(puk1);
    ui->PUK2Retry_Value->setText(puk2);
}
