/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/netinfo.h"
#include "ui_netinfo.h"

NetInformation::NetInformation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NetInformation)
{
    ui->setupUi(this);

    Instruments = new InstrumentsCL(this);

    ui->NetWorkCommunication->setColumnWidth(2,70);
    ui->NetWorkCommunication->setColumnWidth(3,130);
    ui->NetWorkCommunication->setColumnWidth(4,130);
    ui->NetWorkCommunication->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setLayout(ui->verticalLayout);

}

NetInformation::~NetInformation()
{
    delete ui;
}

void NetInformation::setObjectParam(TSummaryInfo *ActiveDeiceInput, getInfo *getInfoCl, basefunction *bf)
{
    ActiveDevice = ActiveDeiceInput;
    getDevInfo=getInfoCl;
    BaseFunction=bf;
}

void NetInformation::getNetWorkInfo()
{
    getDevInfo->getNetCfgInfo(ActiveDevice);
}

void NetInformation::showNetWorkInfo(bool isNew)
{
    clear();
    addNetIfInfo(ActiveDevice->netIF["local"]);
    for (int i=0;i<5;i++)
    {
        addNetIfInfo(ActiveDevice->netIF["eth"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["wlan"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["ccmni"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["ppp"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["br"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["ifb"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["p2p"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["tunl"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["sit"+QVariant(i).toString()]);
        addNetIfInfo(ActiveDevice->netIF["ip6tnl"+QVariant(i).toString()]);
    }
    setLedStatus(ActiveDevice->netIF[ActiveDevice->DevInfo.ProductNetInfo.wifiIf].isUP);
    if (isNew)
        {setHostNames(ActiveDevice->DevInfo.ProductNetInfo.HostName, ActiveDevice->DevInfo.ProductNetInfo.BtName, ActiveDevice->DevInfo.ProductNetInfo.WifiAPName);}
}

void NetInformation::addNetIfInfo(TNetIf ActiveDevice)
{
    DeltaRow=IFInfoTableRowBuild(ActiveDevice,DeltaRow);
    if (ActiveDevice.IfType=="WIFI")
    {
        ui->LED_Wifi->setEnabled(true);
    }
    ui->pb_wifiOnOff->setEnabled(true);
}

void NetInformation::setLedStatus(bool statusLed, int Led)
{
    switch (Led)
    {
        case 0:
            {ledOnOff(ui->LED_Wifi, statusLed);}
        break;
        default:
        break;
    }
}

void NetInformation::setHostNames(QString hostName, QString btName, QString apName)
{
    NetInfoWGX = new BaseScrollWigetVL(this);
    NetInfoWGX->setObjectName(QStringLiteral("NetWorkInformation"));
    NetInfoWGX->setMinimumHeight(175);
    ui->verticalLayout->insertWidget(2, NetInfoWGX);
    NetInfoWGX->addItem(tr("Host Name"), hostName);
    NetInfoWGX->addItem(tr("Bluetooth Name"), btName);
    NetInfoWGX->addItem(tr("WIFI AP Name"), apName);
    NetInfoWGX->addCloseItem();
}

inline int NetInformation::IFInfoTableRowBuild(TNetIf LocNetIF, int rowNum)
{
    if (LocNetIF.NetIf!="")
    {
        ui->NetWorkCommunication->insertRow(rowNum);
        ui->NetWorkCommunication->setItem(rowNum,0,new QTableWidgetItem(LocNetIF.IfType));
        ui->NetWorkCommunication->setItem(rowNum,1,new QTableWidgetItem(LocNetIF.NetIf));
        if (LocNetIF.isUP)
        {
            ui->NetWorkCommunication->setItem(rowNum,2,new QTableWidgetItem(tr("OnLine")));
            ui->NetWorkCommunication->setItem(rowNum,3,new QTableWidgetItem(LocNetIF.IP));
            if (LocNetIF.netMask!=-1)
                {ui->NetWorkCommunication->setItem(rowNum,4,new QTableWidgetItem(BaseFunction->netMaskValue(LocNetIF.netMask)));}
            else
                {ui->NetWorkCommunication->setItem(rowNum,4,new QTableWidgetItem(""));}
        }
        else
        {
            ui->NetWorkCommunication->setItem(rowNum,2,new QTableWidgetItem(tr("OffLine")));
            ui->NetWorkCommunication->setItem(rowNum,3,new QTableWidgetItem(""));
            ui->NetWorkCommunication->setItem(rowNum,4,new QTableWidgetItem(""));
        }
        if (LocNetIF.MAC!="00:00:00:00:00:00")
            {ui->NetWorkCommunication->setItem(rowNum,5,new QTableWidgetItem(LocNetIF.MAC));}
        else
            {ui->NetWorkCommunication->setItem(rowNum,5,new QTableWidgetItem(""));}
        rowNum++;
    }
    return(rowNum);
}

inline void NetInformation::ledOnOff(QLabel *led, bool statusLed)
{
    QString statCharge;
    if (statusLed)
    {
        statCharge=tr("Wifi On");
        led->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_on.png")));
    }
    else
    {
        statCharge=tr("Wifi Off");
        led->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));
    }
    led->setToolTip("<html><head/><body><p>"+statCharge+"</p></body></html>");
}

void NetInformation::clear()
{
    Instruments->tableWigetClear(ui->NetWorkCommunication);
    DeltaRow=0;
}

void NetInformation::CallDevInfo()
{
    disconnect(Timer, SIGNAL(timeout()),this, SLOT(CallDevInfo()));
    getDevInfo->getNameDevSysInfo(ActiveDevice);
    Timer->setInterval(2000);
    connect(Timer, SIGNAL(timeout()),this, SLOT(CallNetCfgInfo()));
    Timer->setSingleShot(true);
    Timer->start();
}

void NetInformation::CallNetCfgInfo()
{
    getDevInfo->getNetCfgInfo(ActiveDevice);
    showNetWorkInfo(false);
    disconnect(Timer, SIGNAL(timeout()),this, SLOT(CallNetCfgInfo()));
    Timer->deleteLater();
}

void NetInformation::on_pb_wifiOnOff_clicked()
{
    Timer = new QTimer(this);
    QStringList Param;
    ui->pb_wifiOnOff->setEnabled(false);
    if (ActiveDevice->netIF[ActiveDevice->DevInfo.ProductNetInfo.wifiIf].isUP)
        {Param << "wifi" << "disable";}
    else
        {Param << "wifi" << "enable";}
    getDevInfo->switchWifiOnOff(ActiveDevice, Param);
    Timer->setInterval(2000);
    connect(Timer, SIGNAL(timeout()),this, SLOT(CallDevInfo()));
    Timer->setSingleShot(true);
    Timer->start();
}
