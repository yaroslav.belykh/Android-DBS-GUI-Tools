/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Widgets/Additional/doublelabel.h"
#include "ui_doublelabel.h"

doubleLabel::doubleLabel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::doubleLabel)
{
    ui->setupUi(this);

    //this->setLayout(ui->PropLayout);
}

doubleLabel::~doubleLabel()
{
    delete ui;
}

void doubleLabel::setSize(int widthWGX, int heightWGX)
{
    this->setSize(widthWGX, heightWGX);
}

void doubleLabel::setSizeMaximum(int widthWGX, int heightWGX)
{
    this->setMaximumWidth(widthWGX);
    this->setMaximumHeight(heightWGX);
    //this->setSizeMaximum(widthWGX, heightWGX);
}

void doubleLabel::setLabelSizeMinimum(int minW)
{
    ui->PropName->setMinimumWidth(ui->PropName->text().count()*minW);
    ui->PropValue->setMinimumWidth(ui->PropValue->text().count()*minW);
}

void doubleLabel::setLabelSizeMaximum(int maxW, int maxH)
{
    ui->PropValue->setMaximumWidth(maxW);
    ui->PropValue->setMaximumHeight(maxH);
    ui->PropValue->setScaledContents(true);
}

void doubleLabel::setFontSize(int fSize)
{
    font.setPointSize(fSize);
    ui->PropName->setFont(font);
    ui->PropValue->setFont(font);
    ui->PropLayout->setSpacing(2);
}

/*void doubleLabel::setWordWrap(bool isWW)
{
    ui->PropValue->setWordWrap(isWW);
}*/

void doubleLabel::setValue(QString strName, QString strValue)
{
    ui->PropName->setText(strName);
    changeValue(strValue);
}

void doubleLabel::changeValue(QString strValue)
{
    if (strValue.indexOf(QString::fromUtf8("Green_dot_o"))==-1)
        {ui->PropValue->setText(strValue);}
    else
        {ui->PropValue->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/")+strValue+QString::fromUtf8(".png")));}
}
