/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Widgets/Additional/basegridwidget.h"
#include "ui_basegridwidget.h"

BaseScrollWidgetGV::BaseScrollWidgetGV(QWidget *parent) :
    QScrollArea(parent),
    ui(new Ui::BaseScrollWidgetGV)
{
    ui->setupUi(this);
    posWGX=6;
}

BaseScrollWidgetGV::~BaseScrollWidgetGV()
{
    delete ui;
}

void BaseScrollWidgetGV::addItem(QString dName, QString dSize, QString dUsed, QString dFree, int diSize, int diValue, int row, int col)
{
    diskSizeInfo[dName]=new DiskInfoWGX(this);
    diskSizeInfo[dName]->setObjectName(QStringLiteral("DiskInfo_")+dName);
    diskSizeInfo[dName]->setName(dName);
    if (!dSize.isEmpty())
    {
        diskSizeInfo[dName]->setSUFValue(dSize, dUsed, dFree);
        diskSizeInfo[dName]->setValue(diSize, diValue);
    }
    else
        {diskSizeInfo[dName]->setEnabled(false);}
    if (row==-1 && col==-1)
    {
        if (posWGX%3==0)
            {col=0;}
        else
        {
            if (posWGX%3==1)
                {col=1;}
            else
                {col=2;}
        }
        row=posWGX/3;
        //ui->gridLayout->addWidget(diskInfo[mapList.at(i)],rowWGX,posWGX/2,1,1);
        posWGX++;
    }
    ui->GL->addWidget(diskSizeInfo[dName],row, col, 1,1);
}

void BaseScrollWidgetGV::deleteAllItems()
{
    QStringList MapKeys;
    MapKeys=diskSizeInfo.keys();
    for (int i=0;i<MapKeys.count();i++)
    {
        diskSizeInfo[MapKeys.at(i)]->close();
        delete diskSizeInfo[MapKeys.at(i)];
        diskSizeInfo[MapKeys.at(i)]=0;
        diskSizeInfo.remove(MapKeys.at(i));
    }
    diskSizeInfo.clear();
}

void BaseScrollWidgetGV::addCloseItems()
{
    ui->GL->addItem(new QSpacerItem(2,28, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 10, 0);
    ui->GL->addItem(new QSpacerItem(28,2, QSizePolicy::Minimum, QSizePolicy::Expanding), 10, 0, 0);
}

