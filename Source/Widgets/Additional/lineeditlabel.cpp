/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Widgets/Additional/lineeditlabel.h"
#include "ui_lineeditlabel.h"

lineEditLabel::lineEditLabel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::lineEditLabel)
{
    ui->setupUi(this);
}

lineEditLabel::~lineEditLabel()
{
    delete ui;
}

void lineEditLabel::setValue(QString propName, QString propValue, QString propValue2)
{
    ui->PropName->setText(propName);
    ui->PropValue->setText(propValue);
    if (!propValue2.isEmpty())
    {
        ui->PropValue2->setVisible(true);
        ui->PropValue2->setText(propValue2);
    }
    else
        {ui->PropValue2->setVisible(false);}
}
