/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/Additional/basescrollwiget.h"
#include "ui_basescrollwiget.h"

BaseScrollWigetVL::BaseScrollWigetVL(QWidget *parent) :
    QScrollArea(parent),
    ui(new Ui::BaseScrollWigetVL)
{
    ui->setupUi(this);
}

BaseScrollWigetVL::~BaseScrollWigetVL()
{
    delete ui;
}

void BaseScrollWigetVL::addItem(QString itemName, QString itemValue)
{
    if (!itemValue.isEmpty())
    {
        mapItem[itemName] = new doubleLabel(this);
        mapItem[itemName]->setObjectName(QStringLiteral("lbl_GI_")+itemName);
        mapItem[itemName]->setValue(itemName+":", itemValue);
        ui->VL->addWidget(mapItem[itemName]);
    }
}

void BaseScrollWigetVL::addLEItem(QString itemName, QString itemValue, QString pValue2)
{
    if (!itemValue.isEmpty())
    {
        mapLEItem[itemName] = new lineEditLabel(this);
        mapLEItem[itemName]->setObjectName(QStringLiteral("le_HI_")+itemName);
        mapLEItem[itemName]->setValue(itemName+":", itemValue, pValue2);
        ui->VL->addWidget(mapLEItem[itemName]);
    }
}

void BaseScrollWigetVL::addGSMItem(int simCount, bool isReady, QString simType, QString simOperator, QString simCountry, QString networkType,
                                   QString Operator, QString Country, bool isRoaming, QString pin1, QString pin2, QString puk1, QString puk2, QString iccid)
{
    SimInfoWGX[simCount]=new simInfo(this);
    SimInfoWGX[simCount]->setObjectName(QStringLiteral("SimInfo")+QVariant(simCount).toString());
    SimInfoWGX[simCount]->setSimName(simCount, isReady, isRoaming, iccid);
    SimInfoWGX[simCount]->setSimInfo(simType, simOperator, simCountry, networkType, Operator, Country);
    SimInfoWGX[simCount]->setPinPukInfo(pin1, pin2, puk1, puk2);
    ui->VL->addWidget(SimInfoWGX[simCount]);
}

void BaseScrollWigetVL::addMemoryInfo(QString memName, QString dSize, QString dUsed, QString dFree, QString dCached, int maxValue, int dValue, int *pos)
{
    memSizeInfo[memName] = new DiskInfoWGX(memHLayoutWgx);
    memSizeInfo[memName]->setObjectName(QStringLiteral("MemInfo_")+memName);
    memSizeInfo[memName]->setName(memName);
    memSizeInfo[memName]->setSUFValue(dSize,dUsed,dFree);
    memSizeInfo[memName]->setCachedValue(dCached);
    memSizeInfo[memName]->setValue(maxValue, dValue);
    memHLayout->insertWidget(*pos, memSizeInfo[memName]);
    *pos=*pos+1;
}

void BaseScrollWigetVL::addHorizontalLayout(int defLayout)
{
    switch (defLayout)
    {
        case 0:
        {
            memHLayoutWgx = new QWidget(this);
            memHLayoutWgx->setObjectName(QStringLiteral("MemInfoWgx"));
            memHLayout = new QHBoxLayout(memHLayoutWgx);
            memHLayout->setObjectName(QStringLiteral("MemInfoLayout"));
            memHLayout->setMargin(0);
            memHLayout->setSpacing(24);
            memHLayout->addItem(new QSpacerItem(2,8, QSizePolicy::Expanding, QSizePolicy::Minimum));
            ui->VL->addWidget(memHLayoutWgx);
        }
        break;
        default:
        break;
    }
}

void BaseScrollWigetVL::addCloseItem()
{
    ui->VL->addItem(new QSpacerItem(28,2, QSizePolicy::Minimum, QSizePolicy::Expanding));
}

void BaseScrollWigetVL::clearVariablesWigets(int defWgx)
{
    switch (defWgx)
    {
        case 0:
        {
            QStringList memKeys;
            memKeys=memSizeInfo.keys();
            for (int i=0;i<memKeys.count();i++)
            {
                memSizeInfo[memKeys.at(i)]->close();
                delete memSizeInfo[memKeys.at(i)];
                memSizeInfo[memKeys.at(i)]=0;
                memSizeInfo.remove(memKeys.at(i));
            }
            memSizeInfo.clear();
        }
        break;
        default:
        break;
    }
}
