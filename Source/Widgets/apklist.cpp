/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Widgets/apklist.h"
#include "ui_apklist.h"

apkList::apkList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::apkList)
{
    ui->setupUi(this);

    this->setLayout(ui->verticalLayout);

    apkSort.aDevice=0;
    apkSort.aType=0;
    apkSort.aStatus=0;

    ui->progressBar_APK->setVisible(false);
}

apkList::~apkList()
{
    delete ui;
}

void apkList::setCMDPointer(tCMD *newCmd, tAdbApp *newAdbApp, tDebug *inpDebug)
{
    adbApp=newAdbApp;
    procCmd=newCmd;
    doDebug=inpDebug;
}

void apkList::setObjectParam(TSummaryInfo *inpActiveDevice, int inpIndex, QString inpTmpPath, QString *inpCachePath, sqldb *SqlDb, basefunction *newBaseFunction)
{
    ActiveDevice=inpActiveDevice;
    devIndex = inpIndex;
    tmpPath=inpTmpPath;
    cachePath=inpCachePath;
    APKListModel = new apkListModel(inpCachePath, SqlDb, newBaseFunction, this);
    SortModel = new QSortFilterProxyModel(this);
    SortModel->setSourceModel(APKListModel);
    ui->tableView->setModel(SortModel);
    //ItemSelectionModel = new QItemSelectionModel(SortModel, this);
    ItemSelectionModel = ui->tableView->selectionModel();
    BaseFunction=newBaseFunction;
    ui->tableView->setColumnHidden(0, true);
    ui->tableView->setColumnWidth(1, 40);
    ui->tableView->setColumnWidth(2, 360);
    ui->tableView->setColumnWidth(3, 270);
    ui->tableView->setColumnWidth(4, 100);
    ui->tableView->setColumnWidth(5, 100);
    ui->tableView->setColumnWidth(6, 100);
    ui->tableView->setColumnWidth(7, 270);
    ui->tableView->setColumnWidth(8, 150);
    ui->tableView->setColumnWidth(9, 150);
}

void apkList::getAPKList()
{
    workParam.clear();
    workList.clear();

    APKList.clear();
    APKListModel->clearData();
    makeConnect();
}

void apkList::threadStarted()
{
    emit makeWork(ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber, currentWork, workParam, workList);
}

void apkList::setAPKcount(int APKCount)
{
    ActiveDevice->APKListInfo.APKCount=APKCount;
    ui->progressBar_APK->setMaximum(APKCount);
    ui->lbl_APK_TP_Val->setText(QVariant(APKCount).toString());
}

void apkList::setAPKInstallStatus(int currentPacage, QString packageName, int errorCode)
{
    ui->progressBar_APK->setValue(currentPacage+1);
    switch (errorCode)
    {
        case 1:
            {workPackagesStatus[currentPacage]=tr("Package ")+BaseFunction->awk(packageName,"/",-10)+tr(" is already installed.");}
        break;
        case 2:
            {workPackagesStatus[currentPacage]=tr("Install package ")+BaseFunction->awk(packageName,"/",-10)+tr(" was failured.");}
        break;
        case 3:
            {workPackagesStatus[currentPacage]=tr("Backup was not create.");}
        break;
        case 4:
            {workPackagesStatus[currentPacage]=tr("Restore package ")+BaseFunction->awk(packageName,"/",-10)+tr(" was failured.");}
        break;
        default:
        break;
    }
    if (currentPacage+1==workList.count())
    {
        emit updateDeviceInfo(devIndex);
        if (workPackagesStatus.keys().count()>0)
        {
            QString msgStr;
            msgStr=tr("Some packages was not be ");
            switch (currentWork)
            {
                case 1:
                    {msgStr.append(tr("installed")+":\n");}
                break;
                case 2:
                    {msgStr.append(tr("uninstalled")+":\n");}
                break;
                case 3:
                    {msgStr.append(tr("backaped")+":\n");}
                break;
                case 4:
                    {msgStr.append(tr("restored")+":\n");}
                break;
                default:
                break;
            }
            for (int i=0;i<workPackagesStatus.keys().count();i++)
            {
                msgStr.append(workPackagesStatus[workPackagesStatus.keys().at(i)]);
                if (i<workPackagesStatus.keys().count()-1)
                    {msgStr.append("\n");}
            }
            QMessageBox::critical(NULL,QObject::tr("Failure!"),msgStr);
        }
        if (workPackagesStatus.keys().count()<workList.count() && currentWork!=3)
        {
            APKList.clear();
            APKListModel->clearData();
            workParam.clear();
            workList.clear();
            currentWork=0;
            ui->progressBar_APK->setMaximum(100);
            ui->progressBar_APK->setValue(0);
            connect(APKListThread, SIGNAL(setAPKcount(int)),this, SLOT(setAPKcount(int)));
            connect(APKListThread, SIGNAL(giveMapAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int)), this, SLOT(setAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int)));
            emit makeWork(ActiveDevice->DevNumber, ActiveDevice->DevPath, ActiveDevice->unicDevNumber, currentWork, workParam, workList);
        }
        else
        {
            disconnect(APKListThread, SIGNAL(iamstarted()), this, SLOT(threadStarted()));
            disconnect(this, SIGNAL(makeWork(QString, QString, bool, int, QStringList, QStringList)), APKListThread, SLOT(doWork(QString, QString, bool, int, QStringList, QStringList)));
            disconnect(APKListThread, SIGNAL(giveAPKInstallStatus(int,QString,int)), this, SLOT(setAPKInstallStatus(int,QString,int)));

            disconnect(thread, SIGNAL(started()), APKListThread, SLOT(process()));
            disconnect(APKListThread, SIGNAL(finished()), thread, SLOT(quit()));
            disconnect(APKListThread, SIGNAL(finished()), APKListThread, SLOT(deleteLater()));
            disconnect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

            thread->quit();
            APKListThread->deleteLater();
            thread->deleteLater();
            ui->progressBar_APK->setEnabled(false);
            ui->progressBar_APK->setVisible(false);
            ui->frm_APK->setEnabled(true);
        }
    }
}

void apkList::setAPKList(QString APKName, bool isSystem, QString Devices, QString UID, QString CodePath, QString DataPath, QString Version, int usedSize, int FullSize, bool isEnabledPackage, bool isUninstall, int currIndex)
{
    APKList[APKName].isSystem=isSystem;
    APKList[APKName].Devices=Devices;
    APKList[APKName].UID=UID;
    APKList[APKName].codePath=CodePath;
    APKList[APKName].dataPath=DataPath;
    APKList[APKName].APKversion=Version;
    APKList[APKName].UsedSize=usedSize;
    APKList[APKName].HiddenSize=FullSize;
    APKList[APKName].isEnable=isEnabledPackage;
    APKList[APKName].isUninstall=isUninstall;
    ActiveDevice->APKListInfo.TotallyPackagesSpace=ActiveDevice->APKListInfo.TotallyPackagesSpace+usedSize;
    ActiveDevice->APKListInfo.usedHiddenPacckahesSpace=ActiveDevice->APKListInfo.usedHiddenPacckahesSpace+FullSize;
    if (isSystem && Devices!="System")
        {ActiveDevice->APKListInfo.UpdSystem++;}
    if (checkAPStatusK(&APKList[APKList.keys().at(currIndex-1)], ui->cB_APK_Section_Device->currentIndex(), ui->cB_APK_Section_Status->currentIndex(), ui->cB_APK_Section_Type->currentIndex()))
    {
        int row;
        row=APKListModel->rowCount();
        APKListModel->updateModel(APKList.keys().at(currIndex-1), APKList[APKList.keys().at(currIndex-1)], row);
    }
    ui->progressBar_APK->setValue(currIndex);
    ui->lbl_APK_TS_Val->setText(BaseFunction->roundString(QVariant(BaseFunction->roundTo(double(ActiveDevice->APKListInfo.TotallyPackagesSpace)/1024,2)).toString(),".",3)+" MB");
    ui->lbl_APK_HS_Val->setText(BaseFunction->roundString(QVariant(BaseFunction->roundTo(double(ActiveDevice->APKListInfo.usedHiddenPacckahesSpace)/1024,2)).toString(),".",3)+" MB");
    ui->lbl_APK_SP_Val->setText(QVariant(ActiveDevice->APKListInfo.UpdSystem).toString());
    if(currIndex==ActiveDevice->APKListInfo.APKCount)
    {
        ui->frm_APK->setEnabled(true);
        //ui->wgx_APK->setEnabled(true);
        ui->progressBar_APK->setEnabled(false);
        ui->progressBar_APK->setVisible(false);
    }
}

void apkList::on_pBAPKInstaller_clicked()
{
    workParam.clear();
    workList.clear();
    //QStringList apkList;

    workList.append(QFileDialog::getOpenFileNames(0, tr("Chose APK files for install"), QDir::homePath(), tr("APK Files ( *.apk ) ")));
    if (workList.count()!=0)
    {
        //ui->frm_APK->setEnabled(false);

        //currentWork=1;

        if (ui->chB_Install_Replace->isChecked())
            {workParam << "-r";}
        if (ui->chB_Install_Doungrade->isChecked())
            {workParam << "-d";}
        if (ui->chB_Install_SDC->isChecked())
            {workParam << "-s";}

        /*APKListThread = new ApkListThread(0, procCmd, adbApp, doDebug);
        thread = new QThread;

        connect(APKListThread, SIGNAL(iamstarted()), this, SLOT(threadStarted()));
        connect(this, SIGNAL(makeWork(QString, QString, bool, int, QStringList, QStringList)), APKListThread, SLOT(doWork(QString, QString, bool, int, QStringList, QStringList)));
        connect(APKListThread, SIGNAL(giveAPKInstallStatus(int,QString,int)), this, SLOT(setAPKInstallStatus(int,QString,int)));

        connect(thread, SIGNAL(started()), APKListThread, SLOT(process()));
        connect(APKListThread, SIGNAL(finished()), thread, SLOT(quit()));
        connect(APKListThread, SIGNAL(finished()), APKListThread, SLOT(deleteLater()));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

        APKListThread->moveToThread(thread);
        thread->start();

        ui->progressBar_APK->setMaximum(apkList.count());
        ui->progressBar_APK->setValue(0);
        ui->progressBar_APK->setVisible(true);
        ui->progressBar_APK->setEnabled(true);*/
        makeConnect(workList.count(), 1, false);
    }
}

void apkList::on_pBAPKUninstaller_clicked()
{
    workParam.clear();
    workList.clear();
    workList=getSelectedAPK();
    if (workList.count()!=0)
    {
        if (ui->chB_Uninstall_kd->isChecked())
            {workParam << "-k";}

        makeConnect(workList.count(), 2, false);
    }
}

void apkList::on_pBAPKBackuper_clicked()
{
    QString saveBackupFileName;
    workParam.clear();
    workList.clear();
    saveBackupFileName=QFileDialog::getSaveFileName(this, tr("Save adb backup"), QDir::homePath(), tr("adb backup files ")+"( *.ab )");
    workList=getSelectedAPK();
    if (!saveBackupFileName.isEmpty())
    {
        workList=getSelectedAPK();
        workParam << saveBackupFileName;
        if (ui->chB_Backup_APK->isChecked())
            {workParam << "-apk";}
        else
            {workParam << "-noapk";}
        if (ui->chB_BackUp_Obb->isChecked())
            {workParam << "-obb";}
        else
            {workParam << "-noobb";}
        if (ui->chB_Backup_Shared->isChecked())
            {workParam << "-shared";}
        else
            {workParam << "-noshared";}
        if (ui->chB_BackUp_System->isChecked())
            {workParam << "-system";}
        else
            {workParam << "-nosystem";}
        if (ui->chB_Backup_All->isChecked())
            {workParam << "-all";}
        makeConnect(1, 3, false);
    }
}

void apkList::on_pBAPKRestore_clicked()
{
    workParam.clear();
    workList.clear();

    workList.append(QFileDialog::getOpenFileNames(0, tr("Chose ADB Backup files for restore"), QDir::homePath(), tr("adb backup ( *.ab );; All Files ( *.* )")));
    if (workList.count()!=0)
        {makeConnect(workList.count(), 4, false);}
}

void apkList::on_cB_APK_Section_Device_currentIndexChanged(int index)
{
    APKListModel->clearData();
    for (int i=0; i<APKList.keys().count();i++)
    {
        if (checkAPStatusK(&APKList[APKList.keys().at(i)],index, ui->cB_APK_Section_Type->currentIndex(), ui->cB_APK_Section_Status->currentIndex()))
        {
            int row;
            row=APKListModel->rowCount();
            APKListModel->updateModel(APKList.keys().at(i), APKList[APKList.keys().at(i)], row);
        }
    }
}

void apkList::on_cB_APK_Section_Type_currentIndexChanged(int index)
{
    APKListModel->clearData();
    for (int i=0; i<APKList.keys().count();i++)
    {
        if (checkAPStatusK(&APKList[APKList.keys().at(i)], ui->cB_APK_Section_Device->currentIndex(), index, ui->cB_APK_Section_Status->currentIndex()))
        {
            int row;
            row=APKListModel->rowCount();
            APKListModel->updateModel(APKList.keys().at(i), APKList[APKList.keys().at(i)], row);
        }
    }
}

void apkList::on_cB_APK_Section_Status_currentIndexChanged(int index)
{
    APKListModel->clearData();
    for (int i=0; i<APKList.keys().count();i++)
    {
        if (checkAPStatusK(&APKList[APKList.keys().at(i)], ui->cB_APK_Section_Device->currentIndex(), ui->cB_APK_Section_Type->currentIndex(), index))
        {
            int row;
            row=APKListModel->rowCount();
            APKListModel->updateModel(APKList.keys().at(i), APKList[APKList.keys().at(i)], row);
        }
    }
}

void apkList::on_chB_Install_Replace_toggled(bool checked)
{
    if (!checked)
        {ui->chB_Install_Doungrade->setChecked(checked);}
}

void apkList::on_chB_Install_Doungrade_toggled(bool checked)
{
    if (checked)
        {ui->chB_Install_Replace->setChecked(checked);}
}

void apkList::on_chB_BackUp_System_toggled(bool checked)
{
    if (checked)
        {ui->chB_Backup_All->setChecked(checked);}
}

void apkList::on_chB_Backup_All_toggled(bool checked)
{
    if (!checked)
        {ui->chB_BackUp_System->setChecked(checked);}
}

// ===================================== inline sort functions ===================================== //

inline bool apkList::checkAPStatusK(tAPK *apk, int diskDevice, int appType, int appStatus)
{
    if (isDeviceAPK(apk, diskDevice))
    {
        if (isSystemAPK(apk, appType))
        {
            if (isEnableAPK(apk, appStatus))
                {return(true);}
            else
                {return(false);}
        }
        else
            {return(false);}
    }
    else
        {return(false);}
}

inline bool apkList::isDeviceAPK(tAPK *apk, int index)
{
    switch (index) {
        case 0:
            {return(true);}
        break;
        case 1:
        {
            if (apk->Devices=="System")
                {return(true);}
            else
                {return(false);}
        }
        break;
        case 2:
        {
            if (apk->Devices=="Data")
                {return(true);}
            else
                {return(false);}
        }
        break;
        case 3:
        {
            if (apk->Devices=="SD Card")
                {return(true);}
            else
                {return(false);}
        }
        break;
        default:
            {return(false);}
        break;
    }
}

inline bool apkList::isSystemAPK(tAPK *apk, int index)
{
    switch (index) {
        case 0:
            {return(true);}
        break;
        case 1:
        {
            if (apk->isSystem)
                {return(true);}
            else
                {return(false);}
        }
        break;
        case 2:
        {
            if (!apk->isSystem)
                {return(true);}
            else
                {return(false);}
        }
        break;
        default:
            {return(false);}
        break;
    }
}

inline bool apkList::isEnableAPK(tAPK *apk, int index)
{
    switch (index) {
        case 0:
            {return(true);}
        break;
        case 1:
        {
            if (apk->isEnable)
                {return(true);}
            else
                {return(false);}
        }
        break;
        case 2:
        {
            if (!apk->isEnable)
                {return(true);}
            else
                {return(false);}
        }
        break;
        default:
            {return(false);}
        break;
    }
}

inline bool apkList::isUninstAPK(tAPK *apk, int index)
{
    switch (index) {
        case 0:
            {return(true);}
        break;
        case 1:
        {
            if (apk->isUninstall)
                {return(true);}
            else
                {return(false);}
        }
        break;
        case 2:
        {
            if (!apk->isUninstall)
                {return(true);}
            else
                {return(false);}
        }
        break;
        default:
            {return(false);}
        break;
    }
}

QStringList apkList::getSelectedAPK()
{
    QStringList selectedAPK;
    QModelIndexList selectedIndexList;
//    selectedIndexList = ItemSelectionModel->selectedIndexes();
    selectedIndexList = ItemSelectionModel->selectedRows();

    for (int i=0;i<selectedIndexList.count();i++)
    {
        QModelIndex ModelIndex;
        QString currentAPK;
        ModelIndex = SortModel->mapToSource(selectedIndexList.at(i));
        currentAPK=APKListModel->APKList[ModelIndex.data(Qt::DisplayRole).toInt()].apkName;
        if (APKList[currentAPK].isUninstall)
            {selectedAPK.append(currentAPK);}
    }
    return(selectedAPK);
}

inline void apkList::makeConnect(int currValue, int curreWork, bool addStatus)
{
    ui->frm_APK->setEnabled(false);
    currentWork=curreWork;
    APKListThread = new ApkListThread(0, procCmd, adbApp, doDebug);
    thread = new QThread;

    connect(APKListThread, SIGNAL(iamstarted()), this, SLOT(threadStarted()));
    connect(this, SIGNAL(makeWork(QString, QString, bool, int, QStringList, QStringList)), APKListThread, SLOT(doWork(QString, QString, bool, int, QStringList, QStringList)));
    if (addStatus)
    {
        connect(APKListThread, SIGNAL(giveMapAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int)), this, SLOT(setAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int)));
        connect(APKListThread, SIGNAL(setAPKcount(int)),this, SLOT(setAPKcount(int)));
    }
    else
        {connect(APKListThread, SIGNAL(giveAPKInstallStatus(int,QString,int)), this, SLOT(setAPKInstallStatus(int,QString,int)));}

    connect(thread, SIGNAL(started()), APKListThread, SLOT(process()));
    connect(APKListThread, SIGNAL(finished()), thread, SLOT(quit()));
    connect(APKListThread, SIGNAL(finished()), APKListThread, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    APKListThread->moveToThread(thread);
    thread->start();

    ui->progressBar_APK->setMaximum(currValue);
    ui->progressBar_APK->setValue(0);
    ui->progressBar_APK->setVisible(true);
    ui->progressBar_APK->setEnabled(true);
}
