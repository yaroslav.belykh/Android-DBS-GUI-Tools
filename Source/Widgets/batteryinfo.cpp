/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/batteryinfo.h"
#include "ui_batteryinfo.h"

BatteryInformation::BatteryInformation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BatteryInformation)
{
    ui->setupUi(this);
    this->setLayout(ui->BatteryHL);
    Capacity=100;
}

BatteryInformation::~BatteryInformation()
{
    delete ui;
}

void BatteryInformation::setObjectParam(TSummaryInfo *ActiveDeiceInput, getInfo *getInfoCl)
{
    ActiveDevice = ActiveDeiceInput;
    getDevInfo=getInfoCl;
}

void BatteryInformation::getBatteryInfo()
{
    getDevInfo->getBatteryInfo(ActiveDevice);
}

void BatteryInformation::showBatteryInfo()
{
    ui->BattaryLevel->display(ActiveDevice->DevBattery.bat_Capacity);
    ui->BattaryLevelBar->setValue(ActiveDevice->DevBattery.bat_Capacity.toInt());
    ui->StatuSBattary->setText(ActiveDevice->DevBattery.bat_Status);
    ui->HealtHBattary->setText(ActiveDevice->DevBattery.bat_Health);
    QString statCharge;
    if (ActiveDevice->DevBattery.bat_Status=="Charging")
    {
        statCharge=tr("Charging");
        ui->lbl_LED_Charging->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_on.png")));
    }
    else
    {
        statCharge=tr("Not charging");
        ui->lbl_LED_Charging->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));
    }
    ui->lbl_LED_Charging->setToolTip("<html><head/><body><p>"+statCharge+"</p></body></html>");
    ui->pb_BatteryRefresh->setEnabled(true);
    Capacity=ActiveDevice->DevBattery.bat_Capacity.toInt();

    corFunction = new correctionFunctions(this);
    if (!ActiveDevice->DevBattery.bat_Voltage.isEmpty())
        {ui->BatteryVoltage->setText(corFunction->StringToDoubleToString(ActiveDevice->DevBattery.bat_Voltage)+tr(" V"));}
    if (!ActiveDevice->DevBattery.bat_Voltage_Min.isEmpty())
        {ui->BatteryVoltageMin->setText(corFunction->StringToDoubleToString(ActiveDevice->DevBattery.bat_Voltage_Min)+tr(" V"));}
    if (!ActiveDevice->DevBattery.bat_Voltage_Max.isEmpty())
        {ui->BatteryVoltageMax->setText(corFunction->StringToDoubleToString(ActiveDevice->DevBattery.bat_Voltage_Max)+tr(" V"));}
    if (!ActiveDevice->DevBattery.bat_Voltage.isEmpty() && !ActiveDevice->DevBattery.bat_Voltage_Min.isEmpty())
    {
        double curVoltd;
        curVoltd=corFunction->BaseFunction.roundTo(double(ActiveDevice->DevBattery.bat_Voltage.toInt()-ActiveDevice->DevBattery.bat_Voltage_Min.toInt())/Capacity*100+ActiveDevice->DevBattery.bat_Voltage_Min.toInt(),0);
        if (curVoltd/1000>1)
        {
            if (curVoltd/1000>1000)
                {ui->BatteryVoltageMaxCur->setText(corFunction->roundString((QVariant(corFunction->BaseFunction.roundTo((curVoltd/1000/1000),3)).toString()),".",3)+tr(" V"));}
            else
                {ui->BatteryVoltageMaxCur->setText(corFunction->roundString((QVariant(corFunction->BaseFunction.roundTo(curVoltd/1000,3)).toString()),".",3)+tr(" V"));}
        }
        else
           {ui->BatteryVoltageMaxCur->setText(QVariant(curVoltd).toString()+tr(" V"));}
    }
    //delete corFunction;
    //corFunction=0;

    if (!ActiveDevice->DevBattery.bat_Temp.isEmpty())
        {ui->BatteryTemperature->setText(ActiveDevice->DevBattery.bat_Temp+" ºC");}
    else
        {ui->BatteryTemperature->clear();}
    ui->BatteryTechnology->setText(ActiveDevice->DevBattery.bat_Technology);

    //corFunction = new correctionFunctions(this);
    if (!ActiveDevice->DevBattery.bat_ChargeVoltage.isEmpty())
        {ui->BatteryChargeVoltage->setText(corFunction->StringToDoubleToString(ActiveDevice->DevBattery.bat_ChargeVoltage)+tr(" V"));}
    showPowerCoonectionInfo(ActiveDevice->DevBattery.ac, ui->lbl_LED_ChargingAC);
    showPowerCoonectionInfo(ActiveDevice->DevBattery.usb, ui->lbl_LED_ChargingUSB);
    showPowerCoonectionInfo(ActiveDevice->DevBattery.wireless, ui->lbl_LED_ChargingWL);
    delete corFunction;
    corFunction=0;

}

void BatteryInformation::showPowerCoonectionInfo(int stS, QLabel *stLabel)
{
    switch (stS)
    {
        case 0:
        {
            stLabel->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));
            stLabel->setDisabled(true);
        }
        break;
        case 1:
        {
            stLabel->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_off.png")));
            stLabel->setEnabled(true);
        }
        break;
        case 2:
        {
            stLabel->setPixmap(QPixmap(QString::fromUtf8(":/Dots/Icons/Green_dot_on.png")));
            stLabel->setEnabled(true);
        }
        break;
        default:
        break;
    }
}

void BatteryInformation::on_pb_BatteryRefresh_clicked()
{
    getBatteryInfo();
    showBatteryInfo();
}
