/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/apklistthread.h"

ApkListThread::ApkListThread(QObject *parent, tCMD *adbCmdInp, tAdbApp *adbAppInp, tDebug *doDebug) : QObject(parent)
{
    appProcess = new adbprocess(this);
    adbCmd=adbCmdInp;
    adbApp=adbAppInp;
    appProcess->setCMDPointer(adbCmdInp, adbAppInp, doDebug->classDebug, "APK List");
}

ApkListThread::~ApkListThread()
{
    delete appProcess;
    appProcess=0;
}

void ApkListThread::getInstalledAPKInfo(QString devNumber, QString devPath, bool unicNumber)
{
    QStringList rawAPKList;
        QStringList rawAPKListSystem;
        QStringList rawAPKListThirdParty;
        QStringList rawAPKListEnabled;
        QStringList rawAPKListDisabled;
        QStringList rawAPKListUninstall;
        QStringList Param;
        int isSystemint;
        int isThirdPartyint;
        int isEnabledint;
        int isDisabledint;
        int isUninstallint;
        //Param << "-f";
        rawAPKList=appProcess->adbCMD(adbCmd->adb.lsAPK, rawAPKList, devNumber, devPath, unicNumber, true);
        emit setAPKcount(rawAPKList.count());
        Param.clear();
        Param << "-s";
        rawAPKListSystem=appProcess->adbCMD(adbCmd->adb.lsAPK, Param, devNumber, devPath, unicNumber, false);
        Param.clear();
        Param << "-3";
        rawAPKListThirdParty=appProcess->adbCMD(adbCmd->adb.lsAPK, Param, devNumber, devPath, unicNumber, false);
        Param.clear();
        Param << "-e";
        rawAPKListEnabled=appProcess->adbCMD(adbCmd->adb.lsAPK, Param, devNumber, devPath, unicNumber, false);
        Param.clear();
        Param << "-d";
        rawAPKListDisabled=appProcess->adbCMD(adbCmd->adb.lsAPK, Param, devNumber, devPath, unicNumber, false);
        Param.clear();
        Param << "-i";
        rawAPKListUninstall=appProcess->adbCMD(adbCmd->adb.lsAPK, Param, devNumber, devPath, unicNumber, false);
        isSystemint=0;
        isThirdPartyint=0;
        isEnabledint=0;
        isDisabledint=0;
        for (int i=0;i<rawAPKList.count();i++)
        {
            QString APKName;
            bool isSystem;
            QString Devices;
            QString UID;
            TInfoAPK WorkingAPK;
            TInfoAPK HideingAPK;
            bool isEnabledPackages;
            bool isUninstallPackages;
            QStringList rawAPKInfo;
            bool ActivePackages;
            int APkSize;
            int HiddenAPKSize;
            APKName=BaseFunction.awk(rawAPKList.at(i),"package:",-10);
            ActivePackages=true;
            rawAPKInfo << "package" << APKName;
            rawAPKInfo=appProcess->adbCMD(adbCmd->adb.dumpsys, rawAPKInfo, devNumber, devPath, unicNumber, true);
            HideingAPK.SizeAPK=0;
            HideingAPK.SizeAPKlib=0;
            HideingAPK.sizeData=0;
            for (int j=0;j<rawAPKInfo.count();j++)
            {
                if (rawAPKInfo.at(j).indexOf("Packages:")!=-1)
                    {ActivePackages=true;}
                if (rawAPKInfo.at(j).indexOf("Hidden system packages:")!=-1)
                    {ActivePackages=false;}
                if (ActivePackages)
                {
                    if (rawAPKInfo.at(j).indexOf("codePath")!=-1)
                    {
                        WorkingAPK.codePath=BaseFunction.awk(rawAPKInfo.at(j),"codePath=", -10);
                        WorkingAPK.SizeAPK=getDUValiue(WorkingAPK.codePath, devNumber, devPath, unicNumber);
                        Param.clear();
                        QString OdexAPK;
                        OdexAPK=BaseFunction.awk(WorkingAPK.codePath, ".apk", 0)+".odex";
                        Param << OdexAPK;
                        if (appProcess->adbCMD(adbCmd->adb.listDir, Param, devNumber, devPath, unicNumber, false).indexOf("No such file or directory")!=-1)
                        {
                            WorkingAPK.odex=true;
                            WorkingAPK.SizeAPK=WorkingAPK.SizeAPK+getDUValiue(OdexAPK, devNumber, devPath, unicNumber);
                        }
                        else
                            {WorkingAPK.odex=false;}
                    }
                    if (rawAPKInfo.at(j).indexOf("resourcePath")!=-1)
                        {WorkingAPK.resourcePath=BaseFunction.awk(rawAPKInfo.at(j),"resourcePath=", -10);}
                    if (rawAPKInfo.at(j).indexOf("nativeLibraryPath")!=-1)
                        {WorkingAPK.nativeLibraryPath=BaseFunction.awk(rawAPKInfo.at(j),"nativeLibraryPath=", -10);}
                    if (rawAPKInfo.at(j).indexOf("versionName")!=-1)
                        {WorkingAPK.APKversion=BaseFunction.awk(rawAPKInfo.at(j),"versionName=", -10);}
                    if (rawAPKInfo.at(j).indexOf("dataDir")!=-1)
                        {WorkingAPK.dataPath=BaseFunction.awk(rawAPKInfo.at(j),"dataDir=", -10);}
                }
                else
                {
                    if (rawAPKInfo.at(j).indexOf("codePath")!=-1)
                    {
                        HideingAPK.codePath=BaseFunction.awk(rawAPKInfo.at(j),"codePath=", -10);
                        HideingAPK.SizeAPK=getDUValiue(HideingAPK.codePath, devNumber, devPath, unicNumber);
                        Param.clear();
                        QString OdexAPK;
                        OdexAPK=BaseFunction.awk(WorkingAPK.codePath, ".apk", 0)+".odex";
                        Param << OdexAPK;
                        if (appProcess->adbCMD(adbCmd->adb.listDir, Param, devNumber, devPath, unicNumber, false).indexOf("No such file or directory")!=-1)
                        {
                            HideingAPK.odex=true;
                            HideingAPK.SizeAPK=HideingAPK.SizeAPK+getDUValiue(OdexAPK, devNumber, devPath, unicNumber);
                        }
                        else
                            {HideingAPK.odex=false;}
                    }
                    if (rawAPKInfo.at(j).indexOf("resourcePath")!=-1)
                        {HideingAPK.resourcePath=BaseFunction.awk(rawAPKInfo.at(j),"resourcePath=", -10);}
                    if (rawAPKInfo.at(j).indexOf("nativeLibraryPath")!=-1)
                        {HideingAPK.nativeLibraryPath=BaseFunction.awk(rawAPKInfo.at(j),"nativeLibraryPath=", -10);}
                    if (rawAPKInfo.at(j).indexOf("versionName")!=-1)
                        {HideingAPK.APKversion=BaseFunction.awk(rawAPKInfo.at(j),"versionName=", -10);}
                    if (rawAPKInfo.at(j).indexOf("dataDir")!=-1)
                        {HideingAPK.dataPath=BaseFunction.awk(rawAPKInfo.at(j),"dataDir=", -10);}
                }
                if (rawAPKInfo.at(j).indexOf("userId")!=-1)
                {
                    if (UID=="")
                        {UID=BaseFunction.awk(BaseFunction.awk(rawAPKInfo.at(j),"userId=", -10)," ", -1);}
                }
            }
            if (WorkingAPK.nativeLibraryPath!=WorkingAPK.codePath && WorkingAPK.nativeLibraryPath.indexOf(WorkingAPK.dataPath)==-1 && WorkingAPK.nativeLibraryPath!="")
                {WorkingAPK.SizeAPKlib=getDUValiue(WorkingAPK.nativeLibraryPath, devNumber, devPath, unicNumber);}
            else
                {WorkingAPK.SizeAPKlib=0;}
            if (WorkingAPK.dataPath!="")
                {WorkingAPK.sizeData=getDUValiue(WorkingAPK.dataPath, devNumber, devPath, unicNumber);}
            else
                {WorkingAPK.sizeData=0;}
            if (HideingAPK.codePath!="")
            {
                if (HideingAPK.nativeLibraryPath!=HideingAPK.codePath && HideingAPK.nativeLibraryPath.indexOf(HideingAPK.dataPath)==-1 && HideingAPK.nativeLibraryPath!="")
                    {HideingAPK.SizeAPKlib=getDUValiue(HideingAPK.nativeLibraryPath, devNumber, devPath, unicNumber);}
                else
                    {HideingAPK.SizeAPKlib=0;}
                if (HideingAPK.dataPath!="" && HideingAPK.dataPath!=WorkingAPK.dataPath)
                    {HideingAPK.sizeData=getDUValiue(HideingAPK.dataPath, devNumber, devPath, unicNumber);}
                else
                    {HideingAPK.sizeData=0;}
            }
            if (BaseFunction.awk(WorkingAPK.codePath,"/",0)=="system" || BaseFunction.awk(WorkingAPK.codePath,"/",0)=="data")
                {Devices=BaseFunction.firstSymbolToUpper(BaseFunction.awk(WorkingAPK.codePath,"/",0));}
            else
                {Devices="SD Card";}
            for (int s=isSystemint;s<rawAPKListSystem.count();s++)
            {
                if (rawAPKListSystem.at(s).indexOf(APKName)==8)
                {
                    isSystem=true;
                    isSystemint++;
                    break;
                }
            }
            for (int s=isThirdPartyint;s<rawAPKListThirdParty.count();s++)
            {
                if (rawAPKListThirdParty.at(s).indexOf(APKName)==8)
                {
                    isSystem=false;
                    isThirdPartyint++;
                    break;
                }
            }
            for (int s=isEnabledint;s<rawAPKListEnabled.count();s++)
            {
                if (rawAPKListEnabled.at(s).indexOf(APKName)==8)
                {
                    isEnabledPackages=true;
                    isEnabledint++;
                    break;
                }
            }
            for (int s=isDisabledint;s<rawAPKListDisabled.count();s++)
            {
                if (rawAPKListDisabled.at(s).indexOf(APKName)==8)
                {
                    isEnabledPackages=false;
                    isDisabledint++;
                    break;
                }
            }
            for (int s=isUninstallint;s<rawAPKListUninstall.count();s++)
            {
                if (rawAPKListUninstall.at(s).indexOf(APKName)==8)
                {
                    if (rawAPKListUninstall.at(s).indexOf("installer=null")==-1)
                        {isUninstallPackages=true;}
                    else
                        {isUninstallPackages=false;}
                    if (rawAPKListUninstall.at(s).indexOf("installer=")==-1)
                        {isUninstallPackages=true;}
                    isUninstallint++;
                    break;
                }
            }
            APkSize=WorkingAPK.SizeAPK+WorkingAPK.SizeAPKlib+WorkingAPK.sizeData;
            HiddenAPKSize=HideingAPK.SizeAPK+HideingAPK.SizeAPKlib+HideingAPK.sizeData;
            emit giveMapAPKList(APKName, isSystem, Devices, UID, WorkingAPK.codePath, WorkingAPK.dataPath, WorkingAPK.APKversion, APkSize, HiddenAPKSize, isEnabledPackages, isUninstallPackages, i+1);
        }
        emit finished();
}

void ApkListThread::installAPK(const QStringList Param, QStringList Packages, bool isInstall, QString devNumber, QString devPath, bool unicNumber)
{
    for (int i=0; i< Packages.count();i++)
    {
        QStringList workParam;
        bool Ok;
        workParam << Param << Packages.at(i);
        QStringList rawAPKInstallStatus;
        if (isInstall)
            {rawAPKInstallStatus=appProcess->adbCMD(adbCmd->adb.instAPK, workParam, devNumber, devPath, unicNumber, true);}
        else
            {rawAPKInstallStatus=appProcess->adbCMD(adbCmd->adb.uninstAPK, workParam, devNumber, devPath, unicNumber, true);}
        Ok=false;
        for (int j=0; j<rawAPKInstallStatus.count();j++)
        {
            if (rawAPKInstallStatus.at(i)=="Success")
                {Ok=true;}
        }
        if (Ok)
            {emit giveAPKInstallStatus(i, "", 0);}
        else
        {
            if (isInstall)
            {
                if (rawAPKInstallStatus.last()=="Failure [INSTALL_FAILED_ALREADY_EXISTS]")
                    {emit giveAPKInstallStatus(i, Packages.at(i), 1);}
                else
                    {emit giveAPKInstallStatus(i, Packages.at(i), 2);}
            }
            else
                {emit giveAPKInstallStatus(i, Packages.at(i), 3);}
        }
    }
}

void ApkListThread::backupAPK(const QStringList Param, QStringList Packages, bool isInstall, QString devNumber, QString devPath, bool unicNumber)
{
    QStringList workParam;
    QStringList rawAPKInstallStatus;
    if (isInstall)
    {
        workParam << Param << Packages;
        QFile backupAB;
        backupAB.setFileName(Param.first());
        backupAB.remove();
        rawAPKInstallStatus=appProcess->adbCMD(adbCmd->adb.backup, workParam, devNumber, devPath, unicNumber, true);
        if (backupAB.exists() && backupAB.size()>0)
            {emit giveAPKInstallStatus(0, "", 0);}
        else
            {emit giveAPKInstallStatus(0, "", 3);}
    }
    else
    {
        for (int i=0; i< Packages.count();i++)
        {
            workParam << Packages.at(i);
            rawAPKInstallStatus=appProcess->adbCMD(adbCmd->adb.restore, workParam, devNumber, devPath, unicNumber, true);
            BaseFunction.deley(10000);
            emit giveAPKInstallStatus(i, "", 0);
        }
    }
}

void ApkListThread::process()
{
    emit iamstarted();
}

void ApkListThread::doWork(QString devNumber, QString devPath, bool unicNumber, int currentWork, QStringList WorkParam, QStringList curList)
{
    switch (currentWork)
    {
        case 0:
            {getInstalledAPKInfo(devNumber, devPath, unicNumber);}
        break;
        case 1:
            {installAPK(WorkParam, curList, true, devNumber, devPath, unicNumber);}
        break;
        case 2:
            {installAPK(WorkParam, curList, false, devNumber, devPath, unicNumber);}
        break;
        case 3:
            {backupAPK(WorkParam, curList, true, devNumber, devPath, unicNumber);}
        break;
        case 4:
            {backupAPK(WorkParam, curList, false, devNumber, devPath, unicNumber);}
        break;
        default:
        break;
    }
}

inline int ApkListThread::getDUValiue(QString duPath, QString devNumber, QString devPath, bool unicNumber)
{
    QString rawAPKDU;
        QStringList Param;
        int dusize;
        Param << duPath;
        Param=appProcess->adbCMD(adbCmd->adb.diskUsage, Param, devNumber, devPath, unicNumber, false);
        if (Param.count()>0)
        {
            rawAPKDU=Param.last();
            if (rawAPKDU.split("\t",QString::SkipEmptyParts).count()>1)
                {dusize=BaseFunction.awk(rawAPKDU, "\t", 0).toInt();}
            else
                {dusize=BaseFunction.awk(rawAPKDU, " ", 0).toInt();}
        }
        else
            {dusize=0;}
        return(dusize);
}
