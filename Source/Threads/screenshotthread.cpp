/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/screenshotthread.h"

ScreenshotThread::ScreenshotThread(QObject *parent, tCMD *adbCmdInp, tAdbApp *adbAppInp, tDebug *doDebug) : QObject(parent)
{
    appProcess = new adbprocess(this);
    adbCmd=adbCmdInp;
    adbApp=adbAppInp;
    appProcess->setCMDPointer(adbCmdInp, adbAppInp, doDebug->classDebug, "Scrennshot");
}

ScreenshotThread::~ScreenshotThread()
{
    delete appProcess;
    appProcess=0;
}

void ScreenshotThread::process()
{
    emit iamstarted();
}

void ScreenshotThread::createScreenshot(QString devNumber, QString devPath, bool unicNumber, QString localPath)
{
    QStringList param;
    bool OK;
    bool tp;
    QStringList workPath;
    int i;
    OK=false;
    tp=true;
    workPath << "/cache" << "/sdcard";
    i=0;
    while (!OK && i<workPath.count())
    {
        param.clear();
        param << workPath.at(i)+"/adbguitools";
        if (tp)
            {param=appProcess->adbCMD(adbCmd->adb.mkdir, param, devNumber, devPath, unicNumber, false);}
        else
        {
            QStringList param2;
            param2 << "mkdir" << workPath.at(i)+"/adbguitools";
            param=appProcess->adbCMD(adbCmd->adb.shellCMD, param2, devNumber, devPath, unicNumber, false);
        }
        if (param.count()==0)
            {OK=true;}
        else
        {
            if (param.first().indexOf("mkdir failed for -p")==-1)
            {
                if (param.first().indexOf("File exists")!=-1)
                    {OK=true;}
                else
                    {i++;}
            }
            else
                {tp=false;}
        }
    }
    if (OK)
    {
        param << workPath.at(i)+"/adbguitools/screenshot.pmg";
        param=appProcess->adbCMD(adbCmd->adb.screenshot, param, devNumber, devPath, unicNumber, true);
        if (param.isEmpty())
        {
            param << workPath.at(i)+"/adbguitools/screenshot.pmg" << localPath+"/screenshot.png";
            param=appProcess->adbCMD(adbCmd->adb.pull, param, devNumber, devPath, unicNumber, true);
            param.clear();
            param << workPath.at(i)+"/adbguitools/screenshot.pmg";
            param=appProcess->adbCMD(adbCmd->adb.rm, param, devNumber, devPath, unicNumber, false);
            emit screenshotCreating(localPath+"/screenshot.png");
        }
        else
            {QMessageBox::critical(NULL,QObject::tr("Failure creating sreenshot!"),tr("Screenshot was not creating."));}
    }
    else
        {QMessageBox::critical(NULL,QObject::tr("Failure creating sreenshot!"),tr("Screenshot was not creating."));}
    emit finished();
}
