/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Dialogs/adbtcpipconnect.h"
#include "ui_adbtcpipconnect.h"

AdbTcpConnect::AdbTcpConnect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdbTcpConnect)
{
    ui->setupUi(this);

    this->setLayout(ui->vLayot);
    //ui->port_LE->setText("5555");

}

AdbTcpConnect::~AdbTcpConnect()
{
    delete ui;
}

void AdbTcpConnect::getConnectedDevices(QStringList *conDev)
{
    ConnectedDevices=conDev;
}

void AdbTcpConnect::on_port_LE_textEdited(const QString &arg1)
{
    int Port;
    bool OK;
    Port=arg1.toInt(&OK, 10);
    if (OK)
    {
        if (Port<1 || Port>65535)
            {QMessageBox::warning(this,QObject::tr("Port is out limits!"),tr("Please enter the port in the range of 1 to 65535"));}
    }
    else
    {
        QMessageBox::warning(this,QObject::tr("Port is out limits!"),tr("You can only use the digits 0 through 9."));
        QString tmpStr;
        tmpStr=arg1;
        int i;
        i=0;
        while (i<tmpStr.count())
        {
            if (tmpStr.at(i)!='0' && tmpStr.at(i)!='1' && tmpStr.at(i)!='2' && tmpStr.at(i)!='3' && tmpStr.at(i)!='4' &&
                    tmpStr.at(i)!='5' && tmpStr.at(i)!='6' && tmpStr.at(i)!='7' && tmpStr.at(i)!='8' && tmpStr.at(i)!='9')
                {tmpStr.remove(i,1);}
            else
                {i++;}
        }
        ui->port_LE->setText(tmpStr);
    }
}

void AdbTcpConnect::on_buttonBox_clicked(QAbstractButton *button)
{
    if (ui->buttonBox->standardButton(button)==QDialogButtonBox::Ok)
    {
        bool OK;
        OK=true;
        for (int i=0;i<ConnectedDevices->count();i++)
        {
            if (ui->address_LE->text()==ConnectedDevices->at(i))
                {OK=false;}
        }
        if (OK)
            {emit setConnectionAddr(ui->address_LE->text()+":"+ui->port_LE->text());}
        else
            {QMessageBox::warning(this,QObject::tr("Device is connected"),tr("You are already connected to this device."));}
    }
    emit closeThisWindow();
}
