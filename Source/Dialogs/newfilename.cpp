/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Dialogs/newfilename.h"
#include "ui_newfilename.h"

newFileName::newFileName(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newFileName)
{

    ui->setupUi(this);

    this->setLayout(ui->verticalLayout);

    deniedSymbolsList << "*" << "?" << "<" << ":" << ">" << "/" << "\\" << "|" << "\"" << "+" << ";" << "=" << "[" << "]";
}

newFileName::~newFileName()
{
    delete ui;
}

void newFileName::on_buttonBox_clicked(QAbstractButton *button)
{
    if (ui->buttonBox->standardButton(button)==QDialogButtonBox::Ok)
        {setFileNameString();}
}

void newFileName::getInputParams(int CurrAcction, QString n1FileName, QString o1FileName, int inpIndex, bool inpLTab)
{
    index=inpIndex;
    nFileName=n1FileName;
    oFileName=o1FileName;
    returnAcction=CurrAcction;
    lTab=inpLTab;
    ui->lineEdit->clear();
    switch (returnAcction)
    {
        case 0:
        {
            ui->label->setText(tr("Please input name for new folder"));
            this->setWindowTitle(tr("New folder Name"));
        }
        break;
        case 1:
        {
            ui->label->setText(tr("Please input new name for")+" \""+o1FileName.split("/",QString::SkipEmptyParts).last()+"\"");
            this->setWindowTitle(tr("New name for")+" \""+o1FileName.split("/",QString::SkipEmptyParts).last()+"\"");
        }
        break;
        default:
        break;
    }
}

void newFileName::on_lineEdit_textEdited(const QString &arg1)
{
    if (arg1.count()>0)
    {
        QString tmpStr;
        bool denSymb;
        denSymb=false;
        tmpStr=arg1;
        for (int i=0;i<deniedSymbolsList.count();i++)
        {
            int denSymbInd;
            denSymbInd=tmpStr.indexOf(deniedSymbolsList.at(i));
            while (denSymbInd!=-1)
            {
                tmpStr.remove(denSymbInd,1);
                denSymb=true;
                denSymbInd=tmpStr.indexOf(deniedSymbolsList.at(i));
            }
        }
        if (denSymb)
        {
            QMessageBox::critical(NULL,QObject::tr("Symbol is forbidden"),tr("Symbols  *?<:>/\\|\"+;=[] are forbidden"));
            ui->lineEdit->setText(tmpStr);
        }
    }
}

void newFileName::setFileNameString()
{
    QString outName;
    if (nFileName.at(nFileName.count()-1)!='/')
        {outName=nFileName+"/"+ui->lineEdit->text();}
    else
        {outName=nFileName+ui->lineEdit->text();}
    emit sendFileNameString(returnAcction, index, lTab, outName, oFileName);
}
