/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Models/apklistmodel.h"

apkListModel::apkListModel(QString* inpCahce, sqldb* sql, basefunction *inBaseFunction, QWidget *parent)
{
    iconPath=inpCahce;
    Sql=sql;
    BaseFunction=inBaseFunction;
}

apkListModel::~apkListModel()
{

}


int apkListModel::rowCount(const QModelIndex &parent) const
{
    return(APKList.keys().count());
}

void apkListModel::updateModel(QString apkName, tAPK apkInfo, int indexRow)
{
    this->beginResetModel();
    APKList[indexRow].apkName=apkName;
    APKList[indexRow].apkInfo=apkInfo;
    if (apkName=="android")
        {APKList[indexRow].apkIcon=QStringLiteral(":/Apps/android");}
    if (apkName.indexOf("com.android.")==0 && apkName.indexOf("providers")==-1)
    {
        QFile isFile;
        isFile.setFileName(QStringLiteral(":/Apps/")+apkName.split("com.android.",QString::SkipEmptyParts).last());
        if (isFile.exists())
            {APKList[indexRow].apkIcon=QStringLiteral(":/Apps/")+apkName.split("com.android.",QString::SkipEmptyParts).last();}
        else
            {APKList[indexRow].apkIcon=QStringLiteral(":/Apps/apk");}
    }
    else
    {
        if (apkName!="android" || (apkName.indexOf("com.android.")==0 && apkName.indexOf("providers")!=-1))
            {APKList[indexRow].apkIcon=QStringLiteral(":/Apps/apk");}
    }
    insertRow(indexRow);
    this->endResetModel();
}

void apkListModel::clearData()
{
    this->beginResetModel();
    int rowC;
    rowC=rowCount();
    APKList.clear();
    removeRows(0,rowC);
    this->endResetModel();
}

int apkListModel::columnCount(const QModelIndex &parent) const
{
    return(9);
}

QVariant apkListModel::headerData(int section, Qt::Orientation orientation, int role)  const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch (section)
        {
            case 1:
                {return(QVariant(tr("Icon")));}
            break;
            case 2:
                {return(QVariant(tr("Application")));}
            break;
            case 3:
                {return(QVariant(tr("Version")));}
            break;
            case 4:
                {return(QVariant(tr("Type")));}
            break;
            case 5:
                {return(QVariant(tr("Drive")));}
            break;
            case 6:
                {return(QVariant(tr("Status")));}
            break;
            case 7:
                 {return(QVariant(tr("Data Path")));}
            break;
            case 8:
                {return(QVariant(tr("Size")));}
            break;
            default:
                {return(QVariant());}
             break;
        }
    }
    else
        {return(QVariant());}
}

QVariant apkListModel::data(const QModelIndex &index, int role) const
{
    //if (role == Qt::DisplayRole)
    switch (role)
    {
        case Qt::DisplayRole:
        {
            QVariant Unswer;
            switch (index.column())
            {
                case 0:
                    {Unswer=index.row();}
                break;
                case 2:
                    {Unswer=QVariant(APKList[index.row()].apkName);}
                break;
                case 3:
                     {Unswer=QVariant(APKList[index.row()].apkInfo.APKversion);}
                break;
                case 4:
                {
                    if (APKList[index.row()].apkInfo.isSystem)
                        {Unswer=QVariant("System");}
                    else
                        {Unswer=QVariant("Third Party");}
                }
                break;
                case 5:
                    {Unswer=QVariant(APKList[index.row()].apkInfo.Devices);}
                break;
                case 6:
                {
                    if (APKList[index.row()].apkInfo.isEnable)
                        {Unswer=QVariant("Enable");}
                    else
                         {Unswer=QVariant("Disable");}
                }
                break;
                case 7:
                    {Unswer=QVariant(APKList[index.row()].apkInfo.dataPath);}
                break;
                case 8:
                {
                    QString strSize;
                    strSize=BaseFunction->roundString(QVariant(BaseFunction->roundTo(double(APKList[index.row()].apkInfo.UsedSize)/1024, 3)).toString(),".");
                    if (APKList[index.row()].apkInfo.HiddenSize!=0)
                        {strSize.append(" (+"+BaseFunction->roundString(QVariant(BaseFunction->roundTo(double(APKList[index.row()].apkInfo.HiddenSize)/1024, 3)).toString(),".")+")");}
                    Unswer=QVariant(strSize+" MB");
                }
                break;
                default:
                break;
            }
            return(Unswer);
        }
    //else
        case Qt::DecorationRole:
        {
            if (index.column()==1)
            {
                if (!APKList[index.row()].apkIcon.isEmpty())
                {
                    QImage image(APKList[index.row()].apkIcon);
                    image.scaled(32,32);
                    QPixmap pixMap(APKList[index.row()].apkIcon);
                    //QPixmap pixMap(32,32);
                    //pixMap.scaled(32,32);
                    //pixMap.fromImage(image, Qt::ColorOnly);
                    return(pixMap);
                }
            }
        }
        default:
            {return(QVariant());}
    }
}

