/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Additional/systemtools.h"

systemtools::systemtools(QObject *parent) : QObject(parent)
{
#if defined(Q_OS_LINUX)
    OSType=1;
#elif defined(Q_OS_WIN)
    OSType=2;
    switch (QSysInfo::WindowsVersion)
    {
        case QSysInfo::WV_XP:
            {OSversion=1;}
        break;
        case QSysInfo::WV_2003:
            {OSversion=1;}
        break;
        case QSysInfo::WV_VISTA:
            {OSversion=2;}
        break;
        case QSysInfo::WV_WINDOWS7:
            {OSversion=2;}
        break;
        case QSysInfo::WV_WINDOWS8:
            {OSversion=2;}
        break;
        case QSysInfo::WV_WINDOWS8_1:
            {OSversion=2;}
        break;
        case QSysInfo::WV_WINDOWS10:
            {OSversion=2;}
        break;
        default:
            {OSversion=0;}
        break;
    }
#else
    OSType=0;
#endif
    //ZipPRC = new QProcess(this);
}

systemtools::~systemtools()
{

}

QString systemtools::cacheDir(QString appDir)
{
    QString cacheDir;
    switch (OSType)
    {
        case 1:
            {cacheDir=QDir::homePath()+QDir::separator()+".cache"+QDir::separator()+appDir;}
        break;
        case 2:
        {
            switch (OSversion)
            {
                case 0:
                    {cacheDir=QDir::homePath()+QDir::separator()+"."+appDir;}
                break;
                case 1:
                    {cacheDir=QDir::homePath()+QDir::separator()+"Local Settings"+QDir::separator()+"Application Data"+QDir::separator()+appDir;}
                break;
                case 2:
                    {cacheDir=QDir::homePath()+QDir::separator()+"AppData"+QDir::separator()+"Local"+QDir::separator()+appDir;}
                break;
                default:
                break;
            }
        }
        break;
        default:
            {cacheDir=QDir::homePath()+QDir::separator()+"."+appDir;}
        break;
    }
    return(cacheDir);
}

