/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Additional/correctionfunctions.h"

correctionFunctions::correctionFunctions(QObject *parent) : QObject(parent)
{

}

correctionFunctions::~correctionFunctions()
{

}

QString correctionFunctions::StringToDoubleToString(QString inputStr)
{
    QString outputStr;
    if (inputStr.toDouble()/1000>1)
    {
        if (QVariant(inputStr.toDouble()/1000).toString().count()-inputStr.count()>1)
        {
            if (inputStr.toDouble()/1000>1000)
                {outputStr=roundString(QVariant(BaseFunction.roundTo((double(int(inputStr.toDouble()/1000))/1000),3)).toString(),".",3);}
            else
            {
                inputStr.insert(1,".");
                outputStr=inputStr;
            }
        }
        else
        {
            if (inputStr.toDouble()/1000>1000)
                {outputStr=QVariant(BaseFunction.roundTo((double(int(inputStr.toDouble()/1000))/1000),3)).toString();}
            else
                {outputStr=QVariant(inputStr.toDouble()/1000).toString();}
        }
    }
    else
        {outputStr=inputStr;}
    return(outputStr);
}

QString correctionFunctions::roundString(QString inpStr, QString splItStr, int roundPosit)
{
    QString outStr;
    QString tmpStr;
    if (!splItStr.isEmpty())
        {tmpStr=inpStr.split(splItStr,QString::SkipEmptyParts).last();}
    else
        {tmpStr=inpStr;}
    if (tmpStr.count()>roundPosit)
        {tmpStr.remove(roundPosit,tmpStr.count()-roundPosit);}
    if (!splItStr.isEmpty())
        {outStr=inpStr.split(splItStr,QString::SkipEmptyParts).first()+splItStr+tmpStr;}
    else
        {outStr=tmpStr;}
    return(outStr);
}

