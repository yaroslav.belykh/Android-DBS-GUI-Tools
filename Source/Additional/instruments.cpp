/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Additional/instruments.h"

InstrumentsCL::InstrumentsCL(QObject *parent) : QObject(parent)
{

}

InstrumentsCL::~InstrumentsCL()
{

}

void InstrumentsCL::tableWigetClear(QTableWidget *currTable)
{
    int DeltaRow;
    DeltaRow=currTable->rowCount();
    if (DeltaRow>0)
    {
        for (int i=0;i<DeltaRow;i++)
            {currTable->removeRow(0);}
    }
}

