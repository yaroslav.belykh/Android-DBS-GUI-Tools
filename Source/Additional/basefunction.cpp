/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Additional/basefunction.h"

basefunction::basefunction(QObject *parent) : QObject(parent)
{

}

basefunction::~basefunction()
{

}

QStringList basefunction::grep(QStringList inpStrList, QString indexStr, bool xst )
{
    QStringList oupStrList;
    if (xst)
    {
        for (int i=0;i<inpStrList.count();i++)
        {
            if (inpStrList.at(i).indexOf(indexStr)!=-1)
                {oupStrList.append(inpStrList.at(i));}
        }
    }
    else
    {
        for (int i=0;i<inpStrList.count();i++)
        {
            if (inpStrList.at(i).indexOf(indexStr)==-1)
                {oupStrList.append(inpStrList.at(i));}
        }
    }
    return(oupStrList);
}

QString basefunction::awk(QString inpStr, QString spltStr, int partStr)
{
    QString outStr;
    outStr.clear();
    if (inpStr.indexOf(spltStr)!=-1)
    {
        QStringList tmpStrList;
        tmpStrList=inpStr.split(spltStr,QString::SkipEmptyParts);
        if (partStr!=-1 && partStr!=-10)
            {outStr=tmpStrList.at(partStr);}
        else
        {
            if (partStr==-1)
                {outStr=tmpStrList.first();}
            if (partStr==-10)
                {outStr=tmpStrList.last();}
        }
    }
    return(outStr);
}

double basefunction::roundTo(double inpValue, int inpCount)
{
    double outpValue;
    double tempVal;
    tempVal=inpValue*pow(10,inpCount);
    if (double(int(tempVal))+0.5==tempVal)
    {
        if (int(tempVal)%2==0)
            {outpValue=double(qFloor(tempVal))/pow(10,inpCount);}
        else
            {outpValue=double(qCeil(tempVal))/pow(10,inpCount);}
    }
    else
    {
        if (double(int(tempVal))+0.5>tempVal)
            {outpValue=double(qCeil(tempVal))/pow(10,inpCount);}
        else
            {outpValue=double(qFloor(tempVal))/pow(10,inpCount);}
    }
    return(outpValue);
}


QString basefunction::netMaskValue(int inpMask)
{
    QString outMask;
    QString netMask;
    netMask=QVariant(256-int(pow(2.0,(32-inpMask)))%8).toString();
    if (inpMask%8!=0)
    {
        switch (inpMask/8)
        {
            case 0:
                {outMask=netMask+".0.0.0";}
            break;
            case 1:
                {outMask="255."+netMask+".0.0";}
            break;
            case 2:
                {outMask="255.255."+netMask+".0";}
            break;
            case 3:
                {outMask="255.255.255."+netMask;}
            break;
            default:
            break;
        }
    }
    else
    {
        switch (inpMask/8)
        {
            case 1:
                {outMask="255.0.0.0";}
            break;
            case 2:
                {outMask="255.255.0.0";}
            break;
            case 3:
                {outMask="255.255.255.0";}
            break;
            default:
            break;
        }
    }
    return(outMask);
}

QString basefunction::StrList2Str(QStringList strList, QString sep, QString Scrng)
{
    QString Str;
    Str="";
    if (strList.count()>0)
    {
        Str.append(Scrng+strList.at(0)+Scrng);
        for (int i=1;i<strList.count();i++)
        {
            Str.append(sep+Scrng+strList.at(i)+Scrng);
        }
    }
    return(Str);
}

QString basefunction::makeSeparatorString(QString inputStr, QString separSymblol, int countLine)
{
    QString separat;
    int leftCont;
    int rightCount;
    separat="";
    leftCont=int(countLine/2)-int((inputStr.count()+2)/2);
    rightCount=(countLine-int(countLine/2))-(inputStr.count()+2-int((inputStr.count()+2)/2));
    for (int i=0;i<leftCont;i++)
        {separat.append(separSymblol);}
    separat.append(" "+inputStr+" ");
    for (int i=0;i<rightCount;i++)
            {separat.append(separSymblol);}
    separat.append("\n");
    return(separat);
}

QString basefunction::firstSymbolToUpper(QString inputStr)
{
    QString outputStr;
    if (!inputStr.isEmpty())
        {outputStr=inputStr.left(1).toUpper()+inputStr.mid(1);}
    return(outputStr);
}

QString basefunction::AllFirstSymbolToUpper(QString inputStr, QString inpSchr)
{
    QString outputStr;
    outputStr.clear();
    if (!inpSchr.isEmpty())
    {
        QStringList strList;
        strList=inputStr.split(inpSchr);
        for (int i=0;i<strList.count();i++)
        {
            outputStr.append(firstSymbolToUpper(strList.at(i)));
            if (i<strList.count()-1)
                {outputStr.append(inpSchr);}
        }
    }
    else
        {outputStr=firstSymbolToUpper(inputStr);}
    return(outputStr);
}

QString basefunction::roundString(QString inpStr, QString splItStr, int roundPosit)
{
    QString outStr;
        QString tmpStr;
        if (!splItStr.isEmpty())
            {tmpStr=inpStr.split(splItStr,QString::SkipEmptyParts).last();}
        else
            {tmpStr=inpStr;}
        if (tmpStr.count()>roundPosit)
            {tmpStr.remove(roundPosit,tmpStr.count()-roundPosit);}
        if (!splItStr.isEmpty())
            {outStr=inpStr.split(splItStr,QString::SkipEmptyParts).first()+splItStr+tmpStr;}
        else
            {outStr=tmpStr;}
        return(outStr);
}

void basefunction::deley(int millisecondsToWait)
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

QBitArray basefunction::bytesToBits(const QByteArray &bytes)
{
    QBitArray bits;
    bits.resize(bytes.size()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<bytes.count(); ++i)
        for(int b=0; b<8; ++b)
            bits.setBit(i*8+b, bytes.at(i)&(1<<(7-b)));
    return bits;
}


QByteArray basefunction::bitsToBytes(const QBitArray &bits)
{
    QByteArray bytes;
    bytes.resize(bits.size()/8);

    // Convert from QBitArray to QByteArray
    for(int b=0; b<bits.count(); ++b)
        bytes[b/8] = (bytes.at(b/8) | ((bits[b]?1:0)<<(b%8)));
    return bytes;
}
