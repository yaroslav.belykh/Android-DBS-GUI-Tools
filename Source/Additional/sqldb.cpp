/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/Additional/sqldb.h"

sqldb::sqldb(QObject *parent) : QObject(parent)
{

}

sqldb::~sqldb()
{

}

void sqldb::setCachePath(QString cachePath, bool isDebug)
{
    QFile sqldbFile;
    bool Ok;
    sqldbFile.setFileName(cachePath+QDir::separator()+"AdbGuiTools.sqlite");
    Ok=false;
    if (!sqldbFile.exists())
    {
        Ok=sqldbFile.open(QIODevice::WriteOnly);
        sqldbFile.close();
        if (!Ok)
            {QMessageBox::critical(0,QObject::tr("Failure create SQLite file!"),tr("File ")+cachePath+QDir::separator()+"AdbGuiTools.sqlite"+tr(" was not created."));}
    }
    cDebug=isDebug;
    sdb = QSqlDatabase::addDatabase("QSQLITE");
    sdb.setDatabaseName(cachePath+QDir::separator()+"AdbGuiTools.sqlite");
    if (sdb.open())
    {
        q = new QSqlQuery(sdb);
        if (Ok)
        {
            if (q->prepare("CREATE TABLE APKList (ID INTEGET, System_Name VARCHAR(255), Version VARCHAR(50), Name VARCHAR(50), Icon TEXT)"))
            {
                if (q->exec())
                {
                    sdb.close();
                }
                else
                {
                    qDebug() << q->lastError().text();
                    QMessageBox::critical(0,QObject::tr("Failure create SQLite table!"), tr("Cannot create table"));
                };
            }
            else
                {qDebug() << q->lastError().text();}
        }
    }
}

