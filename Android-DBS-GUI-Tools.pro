#**********************************************************************
 #Copyright 2016 Yaroslav Belykh (aka Yamah)
#
# This file is part of Android Developer (Bridge) Studio GUI Tools
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#**********************************************************************

#-------------------------------------------------
#
# Project created by QtCreator 2015-11-27T10:42:24
#
#-------------------------------------------------

QT       += core gui sql

CONFIG += thread

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Android-DBS-GUI-Tools
TEMPLATE = app


SOURCES +=  Source/Main/main.cpp\
	    Source/Main/mainwindow.cpp\
	    Source/Main/about.cpp\
	    Source/Widgets/diskinfo.cpp\
	    Source/Dialogs/adbtcpipconnect.cpp\
	    Source/Dialogs/newfilename.cpp\
	    Source/Additional/basefunction.cpp\
	    Source/Additional/systemtools.cpp \
	    Source/Base/adbprocess.cpp \
	    Source/Main/MainWindow/dofromstart.cpp \
	    Source/Main/MainWindow/devicestatus.cpp \
	    Source/Base/getinfo.cpp \
	    Source/Widgets/Additional/doublelabel.cpp \
	    Source/Main/MainWindow/showinfo.cpp \
	    Source/Main/MainWindow/clearall.cpp \
	    Source/Widgets/batteryinfo.cpp \
	    Source/Additional/correctionfunctions.cpp \
	    Source/Widgets/Additional/basescrollwiget.cpp \
	    Source/Widgets/siminfo.cpp \
	    Source/Additional/instruments.cpp \
	    Source/Widgets/Additional/basegridwidget.cpp \
	    Source/Widgets/netinfo.cpp \
            Source/Widgets/apklist.cpp \
            Source/Widgets/Additional/lineeditlabel.cpp \
            Source/Main/MainWindow/makewidgets.cpp \
            Source/Widgets/simcount.cpp \
            Source/Widgets/screenshot.cpp \
            Source/Threads/screenshotthread.cpp \
            Source/Models/apklistmodel.cpp \
    Source/Additional/sqldb.cpp \
    Source/Threads/apklistthread.cpp \
    Source/Widgets/filemanagertab.cpp \
    Source/Widgets/filemanager.cpp

HEADERS  += Headers/Main/mainwindow.h\
	    Headers/Main/structures.h\
	    Headers/Main/about.h\
	    Headers/Commons/structures.h\
	    Headers/Widgets/diskinfo.h\
	    Headers/Widgets/diskinfo.h\
	    Headers/Dialogs/adbtcpipconnect.h\
	    Headers/Dialogs/newfilename.h\
            Headers/Additional/basefunction.h\
            Headers/Additional/systemtools.h \
	    Headers/Base/adbprocess.h \
	    Headers/Base/structures.h \
	    Headers/Base/getinfo.h \
	    Headers/Widgets/Additional/doublelabel.h \
	    Headers/Widgets/batteryinfo.h \
	    Headers/Additional/correctionfunctions.h \
	    Headers/Widgets/Additional/basescrollwiget.h \
	    Headers/Widgets/siminfo.h \
	    Headers/Additional/instruments.h \
	    Headers/Widgets/Additional/basegridwidget.h \
	    Headers/Widgets/netinfo.h \
            Headers/Commons/Structures/net_st.h \
            Headers/Widgets/apklist.h \
            Headers/Commons/apklist.h \
            Headers/Widgets/Additional/lineeditlabel.h \
            Headers/Widgets/structures.h \
            Headers/Widgets/simcount.h \
            Headers/Widgets/screenshot.h \
            Headers/Threads/screenshotthread.h \
            Headers/Models/apklistmodel.h \
    Headers/Additional/sqldb.h \
    Headers/Threads/apklistthread.h \
    Headers/Widgets/filemanagertab.h \
    Headers/Widgets/filemanager.h

FORMS    += Forms/Main/mainwindow.ui\
	    Forms/Main/about.ui\
	    Forms/Widgets/diskinfo.ui\
	    Forms/Dialogs/adbtcpipconnect.ui\
	    Forms/Dialogs/newfilename.ui \
	    Forms/Widgets/Additional/doublelabel.ui \
	    Forms/Widgets/batteryinfo.ui \
	    Forms/Widgets/Additional/basescrollwiget.ui \
	    Forms/Widgets/siminfo.ui \
	    Forms/Widgets/Additional/basegridwidget.ui \
	    Forms/Widgets/netinfo.ui \
            Forms/Widgets/apklist.ui \
            Forms/Widgets/Additional/lineeditlabel.ui \
            Forms/Widgets/simcount.ui \
            Forms/Widgets/screenshot.ui \
    Forms/Widgets/filemanagertab.ui \
    Forms/Widgets/filemanager.ui

RESOURCES += Resources/data.qrc \
	     Resources/icons.qrc

TRANSLATIONS += Translations/ADSTool-ru_RU.ts
