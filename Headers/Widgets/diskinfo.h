/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef DISKINFO_H
#define DISKINFO_H

#include <QFrame>

namespace Ui {
class DiskInfoWGX;
}

class DiskInfoWGX : public QFrame
{
    Q_OBJECT

public:
    explicit DiskInfoWGX(QWidget *parent = 0);
    ~DiskInfoWGX();

    void setName(QString);
    void setSUFValue(QString, QString, QString);
    void setCachedValue(QString);
    void setValue(int, int);
    void setClear();

    //void setMaxSize(int, int);
    //void setMinSize(int, int);
    //void setCurSize(int, int);

private:
    Ui::DiskInfoWGX *ui;
};

#endif // DISKINFO_H
