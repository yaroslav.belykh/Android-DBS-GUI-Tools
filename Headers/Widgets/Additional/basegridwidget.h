/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#ifndef BASEGRIDWIDGET_H
#define BASEGRIDWIDGET_H

#include <QScrollArea>
#include <QMap>
#include <QSpacerItem>
#include "Headers/Widgets/diskinfo.h"

namespace Ui {
class BaseScrollWidgetGV;
}

class BaseScrollWidgetGV : public QScrollArea
{
    Q_OBJECT

public:
    explicit BaseScrollWidgetGV(QWidget *parent = 0);
    ~BaseScrollWidgetGV();

    void addItem(QString, QString, QString, QString, int, int, int row = -1, int cow = -1);
    void deleteAllItems(void);

    void addCloseItems(void);

private:
        Ui::BaseScrollWidgetGV *ui;

        QMap<QString, DiskInfoWGX*> diskSizeInfo;
        int posWGX;

};
#endif // BASEGRIDWIDGET_H
