/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef DOUBLELABEL_H
#define DOUBLELABEL_H

#include <QWidget>
#include <QFont>
#include <QSpacerItem>

namespace Ui {
class doubleLabel;
}

class doubleLabel : public QWidget
{
    Q_OBJECT

public:
    explicit doubleLabel(QWidget *parent = 0);
    ~doubleLabel();

    void setSize(int, int);
    void setSizeMaximum(int, int);
    void setLabelSizeMinimum(int);
    void setLabelSizeMaximum(int, int);
    void setFontSize(int);
    //void setWordWrap(bool isWW = true);

    void setValue(QString, QString);
    void changeValue(QString);

private:
    Ui::doubleLabel *ui;

    QFont font;

};

#endif // DOUBLELABEL_H
