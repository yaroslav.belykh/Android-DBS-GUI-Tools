/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef BASESCROLLWIGETVL_H
#define BASESCROLLWIGETVL_H

#include <QScrollArea>
#include <QMap>
#include <QSpacerItem>
#include <QHBoxLayout>
#include "Headers/Widgets/Additional/doublelabel.h"
#include "Headers/Widgets/Additional/lineeditlabel.h"

#include "Headers/Widgets/diskinfo.h"
#include "Headers/Widgets/siminfo.h"

namespace Ui {
class BaseScrollWigetVL;
}

class BaseScrollWigetVL : public QScrollArea
{
    Q_OBJECT

public:
    explicit BaseScrollWigetVL(QWidget *parent = 0);
    ~BaseScrollWigetVL();

    void addItem(QString, QString);
    void addLEItem(QString, QString, QString pValue2 = "");
    void addGSMItem(int, bool, QString, QString, QString, QString, QString, QString, bool, QString, QString, QString, QString, QString);
    void addMemoryInfo(QString, QString, QString, QString, QString, int, int, int*);
    void addHorizontalLayout(int defLayout = 0);

    void addCloseItem(void);

    void clearVariablesWigets(int defWgx =0);

private:
    Ui::BaseScrollWigetVL *ui;

    QMap<QString, doubleLabel*> mapItem;
    QMap<QString, lineEditLabel*> mapLEItem;
    QMap<int,simInfo*> SimInfoWGX;
    QMap<QString, DiskInfoWGX*> memSizeInfo;

    QHBoxLayout* memHLayout;
    QWidget* memHLayoutWgx;
};

#endif // BaseScrollWigetVL_H
