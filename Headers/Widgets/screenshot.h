/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <QWidget>
#include <QThread>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"
#include "Headers/Threads/screenshotthread.h"

namespace Ui {
class Screenshot;
}

class Screenshot : public QWidget
{
    Q_OBJECT

public:
    explicit Screenshot(QWidget *parent = 0);
    ~Screenshot();

    void setCMDPointer(tCMD*, tAdbApp*, tDebug*);
    void setObjectParam(TSummaryInfo*, QString);

    void makescreenshot(void);

    QSize getGViewMinSize(QSize, QSize);
    QSize getGViewMaxSize(QSize, QSize);

signals:
    void getSreenshot(QString, QString, bool, QString);

public slots:
    void threadStarted(void);
    void screenshotCreating(QString);

private slots:
    void on_pB_Screenshot_clicked();

    void on_pB_saveScrenshot_clicked();

private:
    Ui::Screenshot *ui;

    ScreenshotThread *ScrThread;
    QThread *thread;

    tCMD *procCmd;
    tAdbApp *adbApp;
    tDebug *doDebug;

    TSummaryInfo *ActiveDevice;
    QString tmpPath;

    QPixmap pixMap;
    QGraphicsScene scrsScene;

};

#endif // SCREENSHOT_H
