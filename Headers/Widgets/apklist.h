/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef APKLIST_H
#define APKLIST_H

#include <QWidget>
#include <QThread>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>

#include "Headers/Commons/apklist.h"
#include "Headers/Base/structures.h"

#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"

#include "Headers/Models/apklistmodel.h"
#include "Headers/Additional/basefunction.h"
#include "Headers/Additional/sqldb.h"

#include "Headers/Threads/apklistthread.h"

namespace Ui {
class apkList;
}

class apkList : public QWidget
{
    Q_OBJECT

public:
    explicit apkList(QWidget *parent = 0);
    ~apkList();

    void setCMDPointer(tCMD*, tAdbApp*, tDebug*);
    void setObjectParam(TSummaryInfo*, int, QString, QString*, sqldb*, basefunction*);

    void getAPKList(void);

signals:
    void makeWork(QString, QString, bool, int, QStringList, QStringList);
    void updateDeviceInfo(int);

public slots:
    void threadStarted(void);
    void setAPKcount(int);
    void setAPKInstallStatus(int, QString, int);
    void setAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int);

private slots:
    void on_pBAPKInstaller_clicked();

    void on_cB_APK_Section_Device_currentIndexChanged(int index);

    void on_cB_APK_Section_Type_currentIndexChanged(int index);

    void on_cB_APK_Section_Status_currentIndexChanged(int index);

    void on_pBAPKRestore_clicked();

    void on_pBAPKUninstaller_clicked();

    void on_pBAPKBackuper_clicked();

    void on_chB_Install_Replace_toggled(bool checked);

    void on_chB_Install_Doungrade_toggled(bool checked);

    void on_chB_BackUp_System_toggled(bool checked);

    void on_chB_Backup_All_toggled(bool checked);

private:
    Ui::apkList *ui;

    tCMD *procCmd;
    tAdbApp *adbApp;
    tDebug *doDebug;

    tAPKlist apkListWGX;
    tApkListSort apkSort;

    ApkListThread *APKListThread;
    QThread *thread;
    basefunction *BaseFunction;

    QStringList SelectedAPK;

    TSummaryInfo *ActiveDevice;
    QMap<QString, tAPK> APKList;
    int devIndex;

    apkListModel *APKListModel;
    QSortFilterProxyModel *SortModel;
    QItemSelectionModel *ItemSelectionModel;

    QString tmpPath;
    QString *cachePath;

    int currentWork;
    QStringList workParam;
    QStringList workList;

    QMap<int, QString> workPackagesStatus;

    inline bool checkAPStatusK(tAPK*, int , int, int);
    inline bool isDeviceAPK(tAPK*, int index = 0);
    inline bool isSystemAPK(tAPK*, int index = 0);
    inline bool isEnableAPK(tAPK*, int index = 0);
    inline bool isUninstAPK(tAPK*, int index = 0);

    QStringList getSelectedAPK(void);

    inline void makeConnect(int currValue = 100, int curreWork = 0, bool addStatus = true);

};

#endif // APKLIST_H
