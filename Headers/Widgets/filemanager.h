/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QWidget>

#include <Headers/Widgets/filemanagertab.h>

#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"

namespace Ui {
class FileManager;
}

class FileManager : public QWidget
{
    Q_OBJECT

public:
    explicit FileManager(QWidget *parent = 0);
    ~FileManager();

    void setCMDPointer(tCMD*, tAdbApp*, tDebug*);
    void setObjectParam(TSummaryInfo*);

private:
    Ui::FileManager *ui;

    FileManagerTab *FM1;
    FileManagerTab *FM2;

    QThread *thread;

    TSummaryInfo *ActiveDevice;

    tCMD *procCmd;
    tAdbApp *adbApp;
    tDebug *doDebug;
};

#endif // FILEMANAGER_H
