/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#ifndef NETINFO_H
#define NETINFO_H

#include <QWidget>
#include <QLabel>
#include <QTimer>

#include "Headers/Base/structures.h"
#include "Headers/Commons/Structures/net_st.h"
#include "Headers/Additional/instruments.h"
#include "Headers/Additional/basefunction.h"

#include "Headers/Main/structures.h"
#include "Headers/Base/getinfo.h"
#include "Headers/Widgets/Additional/basescrollwiget.h"

namespace Ui {
class NetInformation;
}

class NetInformation : public QWidget
{
    Q_OBJECT

public:
    explicit NetInformation(QWidget *parent = 0);
    ~NetInformation();

    void setObjectParam(TSummaryInfo*, getInfo*, basefunction*);

    void getNetWorkInfo(void);
    void showNetWorkInfo(bool isNew = true);

    void addNetIfInfo(TNetIf);
    void setLedStatus(bool, int Led = 0);
    void setHostNames(QString, QString, QString);
    void clear();

signals:

public slots:
    void CallDevInfo(void);
    void CallNetCfgInfo(void);

private slots:
    void on_pb_wifiOnOff_clicked();

private:
    Ui::NetInformation *ui;

    basefunction *BaseFunction;
    getInfo *getDevInfo;

    QTimer *Timer;

    TSummaryInfo *ActiveDevice;

    BaseScrollWigetVL *NetInfoWGX;

    InstrumentsCL *Instruments;

    int DeltaRow;

    inline int IFInfoTableRowBuild(TNetIf, int);
    inline void ledOnOff(QLabel*, bool);
};

#endif // NETINFO_H
