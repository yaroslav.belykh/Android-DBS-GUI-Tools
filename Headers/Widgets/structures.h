/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "Headers/Widgets/batteryinfo.h"
#include "Headers/Widgets/diskinfo.h"
#include "Headers/Widgets/netinfo.h"
#include "Headers/Widgets/simcount.h"
#include "Headers/Widgets/screenshot.h"
#include "Headers/Widgets/apklist.h"
#include "Headers/Widgets/filemanager.h"

#include "Headers/Widgets/Additional/basescrollwiget.h"
#include "Headers/Widgets/Additional/basegridwidget.h"

typedef struct vInfiWGX
{
    BaseScrollWigetVL *giWGX;
    BaseScrollWidgetGV *diWGX;
    BatteryInformation *biWGX;
    NetInformation *niWGX;
    BaseScrollWigetVL *hiWGX;

    Screenshot *ssWGX; // This is not wermaht members;

    FileManager *fmWGX;
    apkList *alWGX;

    BaseScrollWigetVL *SimInfoWGX;
    simCount *SimCountWGX;

    bool WGXb;
} tInfoWGX;


#endif // STRUCTURES_H
