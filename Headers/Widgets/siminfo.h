/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef SIMINFO_H
#define SIMINFO_H

#include <QFrame>

namespace Ui {
class simInfo;
}

class simInfo : public QFrame
{
    Q_OBJECT

public:
    explicit simInfo(QWidget *parent = 0);
    ~simInfo();

    void setSimName(int, bool, bool, QString);
    void setSimInfo(QString, QString, QString, QString, QString, QString);
    void setPinPukInfo(QString, QString, QString, QString);

private:
    Ui::simInfo *ui;
};

#endif // SIMINFO_H
