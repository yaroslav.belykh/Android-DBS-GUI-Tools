/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef SCREENSHOTTHREAD_H
#define SCREENSHOTTHREAD_H

#include <QObject>

#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"

class ScreenshotThread : public QObject
{
    Q_OBJECT
public:
    ScreenshotThread(QObject *parent, tCMD*, tAdbApp*, tDebug*);
    ~ScreenshotThread();

signals:
    void iamstarted(void);
    void screenshotCreating(QString);
    void finished();

public slots:
    void process(void);
    void createScreenshot(QString, QString, bool, QString);

private:
    adbprocess *appProcess;
    tCMD *adbCmd;
    tAdbApp *adbApp;
};

#endif // SCREENSHOTTHREAD_H
