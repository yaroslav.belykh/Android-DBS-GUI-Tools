/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef APKLISTTHREAD_H
#define APKLISTTHREAD_H

#include <QObject>
#include <QFile>

#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"

class ApkListThread : public QObject
{
    Q_OBJECT
public:
    ApkListThread(QObject *parent, tCMD*, tAdbApp*, tDebug*);
    ~ApkListThread();

    void getInstalledAPKInfo(QString, QString, bool);
    void installAPK(const QStringList, QStringList, bool, QString, QString, bool);
    void backupAPK(const QStringList, QStringList, bool, QString, QString, bool);

signals:
    void iamstarted(void);
    void setAPKcount(int);
    void giveMapAPKList(QString, bool, QString, QString, QString, QString, QString, int, int, bool, bool, int);
    void giveAPKInstallStatus(int, QString, int);
    void finished();

public slots:
    void process(void);
    void doWork(QString, QString, bool, int, QStringList, QStringList);

private:
    basefunction BaseFunction;
    adbprocess *appProcess;
    tCMD *adbCmd;
    tAdbApp *adbApp;

    inline int getDUValiue(QString, QString, QString, bool);
};

#endif // APKLISTTHREAD_H
