/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QString>

#include <QThread>
#include <QTemporaryDir>
#include <QSettings>
#include <QTranslator>
#include <QTimer>

#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStatusBar>
#include <QGraphicsScene>
#include <QTemporaryDir>
#include <QDir>

#include <bitset>

#include "Headers/Base/structures.h"
#include "structures.h"

#include "Headers/Main/about.h"
#include "Headers/Dialogs/adbtcpipconnect.h"
#include "Headers/Dialogs/newfilename.h"
#include "Headers/Additional/basefunction.h"
#include "Headers/Base/adbprocess.h"
#include "Headers/Base/getinfo.h"
//#include "Headers/Additional/instruments.h"

#include "Headers/Additional/systemtools.h"
#include "Headers/Additional/sqldb.h"

#include "Headers/Widgets/structures.h"

#include "Headers/Widgets/Additional/doublelabel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void getStatusOnClick(void);

    void makeStatusWigets(void);

    void showInfo(int);
    void showGeneralInfo(BaseScrollWigetVL*, QVBoxLayout*);
    void shoWBatteryInfo(BatteryInformation*, QVBoxLayout*);
    void showDiskInfo(TSummaryInfo*, BaseScrollWidgetGV*, QVBoxLayout*);
    void showSimInfo(int, simCount*, BaseScrollWigetVL*);
    void showNetCfgInfo(NetInformation*);
    void showHardwareInfo(tMemory*, BaseScrollWigetVL*, QVBoxLayout*);
    void showScreenShotInfo(Screenshot*, QVBoxLayout*);
    void showAPKInfo(apkList*, QVBoxLayout*);
    void showFileManager(FileManager*, QVBoxLayout*, bool reFrash = true);

    void showStatusInfo();

public slots:
    void closeAboutWindow(void);
    void closeTCPADBdWindow(void);
    void updateDeviceInfo(int);
    void doConnectTCP(QString);

signals:

private slots:

    void doTcpConnect();
    void OpenAboutQt();
    void OpenAbout();
    void doExit();

    void on_pB_refresLiast_clicked();

    void on_DeviceList_currentIndexChanged(int index);

    void on_tb_Status_clicked();

    void on_pB_Reboot_clicked();

    void on_pB_Shutdown_clicked();

    void on_pB_Recovery_clicked();

    void on_pB_Fastboot_clicked();

private:
    Ui::MainWindow *ui;

    basefunction BaseFunction;
    about *AboutDialog;
    newFileName *NewFileNameDialog;
    AdbTcpConnect *ConnectADBdDialog;
    adbprocess *appProcess;
    getInfo *getDevInfo;
    //InstrumentsCL *Instruments;

    tAdbApp adbApp;
    tCMD adbCmd;
    tAndroidIcon AndroidIcon;

    QString AppName;

    systemtools SysTools;
    sqldb Sql;

    QMap<int, tInfoWGX> InfoWGX;
    QMap<int, TSummaryInfo> ActiveDevices;
    int wisibleIndex;
    QMap<int, TDevAction> DeviceWgxStatistic;
    QMap<int, TWorkMap> WorkMap;
    int WorkIndex;

    QSettings* settings;

    tDebug doDebug;

    QStringList tcpConnected;

    tAndroidCount androidCount;
    QMap<QString,doubleLabel*> StatusBarCount;

    QTemporaryDir tmpDir;

    QTimer timer;

    QString cachePath;

/* Private functions */
    //inline void inStart(void);
    void defCmd(void);
    inline Tadb defCmdADB(void);
    inline Tadbfb defCmdFastboot(void);
    inline tAndroidIcon defIcons(void);
    inline tAdbApp defApp(void);
    inline void setAppPath(void);
    inline void saveConfigAppPath(void);
    inline void setCacheDir(void);

    inline void makeWgxGInfo(BaseScrollWigetVL*, TSysInfo*);
    inline void makeWgxBInfo(BatteryInformation*, TSummaryInfo*);
    inline void makeWgxNInfo(NetInformation*, TSummaryInfo*);
    inline void makeWgxHInfo(BaseScrollWigetVL*, TSummaryInfo*);
    inline void makeWgxSim(int*, TGSmInfo*, simCount*, BaseScrollWigetVL*);
    inline void makeWgxScrShot(Screenshot*, TSummaryInfo*);
    inline void makeWgxApkInfo(apkList*, int, TSummaryInfo*);
    inline void makeWgxFileManager(FileManager*, TSummaryInfo*);

    inline void DeviceStringBuild(QStringList, bool isADB = true);
    inline QStringList DeviceStringAdd(QStringList);

    void closeStatus(int);
    void clearStatus(int);
    void clearAllStatus();

    void AndroidCountClear(void);

    void setLayouts(void);

    inline void createStatusBar();

    inline TDiskSpaceInt getIntDIValue(TDiskSpace);

    inline void changeDeviceStatus(int, int timerTimeOut = 20000, int CallAcction = 0);
    inline void removeDeviceFromList(int);
};

#endif // MAINWINDOW_H
