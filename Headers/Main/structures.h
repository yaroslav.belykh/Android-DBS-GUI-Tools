/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef STRUCTURES_MAINWINDOW
#define STRUCTURES_MAINWINDOW

#include "Headers/Commons/structures.h"
#include "Headers/Commons/Structures/net_st.h"
#include "Headers/Commons/apklist.h"
#include <QDir>
#include <QLineEdit>
#include <QTableWidget>
#include <QComboBox>
#include <QVariant>

typedef struct ProductInfo {
    QString prod_Board;
    QString prod_Brand;
    QString prod_Device;
    QString prod_Manufacture;
    QString prod_Model;
    QString prod_Name;
    QString prod_Language;
    QString prod_Region;
    QString SN;
    QString GSM_SN;
    QString GSMVersionBB;
    QString device_SN;
    QString boot_SN;
    QString build_Type;
    QString build_Product;
    QString build_Version;
    QString build_SdkVersion;
    QString build_DisplayID;
    QString build_ID;
    QString build_SKU;
    QString build_PDA;
    QString build_CustomBuildVerion;
    QString build_VersionIncremental;
    QString build_HidVersion;
    QString build_InnerVersion;
    QString build_hwVersion;
    QString ril_sn;
    QString ExternalSD;
    QString InternalSD;
} TProductInfo;

typedef struct GsmSimInfo {
    QString GSMOperator;
    QString SimOperator;
    QString SimOperatorBN;
    QString Country;
    QString SimCountry;
    bool isRoaming;
    bool RoamingNeeded;
    QString SimType;
    QString rPin1;
    QString rPin2;
    QString rPuk1;
    QString rPuk2;
    QString BaseBand;
    QString NetworkType;
    QString CSNetworkType;
    QString SimState;
    bool phbReady;
    QString StkAppName;
    qlonglong NitzTime;
    QString iccid;
} TGsmSimInfo;

typedef struct vIMEI {
    QString TypeModule;
    QString IMEI;
} Timei;

typedef struct GSMInfo {
    int InsertSim;
    QMap<int, TGsmSimInfo> GSMSim;
} TGSmInfo;

typedef struct DevSysInfo {
    TProductInfo ProductNames;
    TNetdInfo ProductNetInfo;
} TSysInfo;

typedef struct DevButteryInfo {
    QString bat_Name;
    QString bat_Type;
    QString bat_Status;
    QString bat_Health;
    QString bat_Technology;
    QString bat_Capacity;
    QString bat_Scale;
    QString bat_Voltage;
    QString bat_Voltage_Min;
    QString bat_Voltage_Max;
    QString bat_Temp;
    QString bat_ChargeVoltage;
    int usb;
    int ac;
    int wireless;
} TBatteryInfo;

typedef struct vCPU {
    QString Processor;
    QString Kernels;
    QString KernelsOnL;
    QString cpuArch;
    QString ProcRev;
    QString HardWare;
    QString HardRev;
    QString Features;
    QString CpuSerials;
    QString cpuFreq;
} tCPU;

typedef struct vMemory {
    int TotalMem;
    int FreeMem;
    int CachedMem;
    int TotalSwap;
    int FreeSwap;
    int CachedSwap;
} tMemory;

typedef struct vFBpartition {
    QString partType;
    QString partSize;
} tPatrFB;

typedef struct v_Dspl {
    QString ResolurionX;
    QString ResolurionY;
} tDisplayPS;

typedef struct v_Camera {
    QString Facing;
    QString Orientation;
    bool isOpen;
} tCamera;

typedef struct v_Display {
    tDisplayPS appDisplay;
    tDisplayPS realDisplay;
    tDisplayPS LappDisplay;
    tDisplayPS SappDisplay;
    QString fpsDispley;
    QString density;
    tDisplayPS densityXY;
    QString DisplayName;
    QString rotation;
} tDisplay;

typedef struct DevDiskSpace  {
    QString dfSize;
    QString dfUsed;
    QString dfFree;
} TDiskSpace;

typedef struct V_CameraHW
{
    QString CameraModule;
    QString Vendor;
    int camDevices;
    QMap<int, tCamera> Camera;
} tCameraHW;

typedef struct devFBSummaryInfo {
    QMap<QString, tPatrFB> Partitions;
    QString Kernel;
    QString Product;
    QString Version;
} TDevFB;

typedef struct vHardWare {
    int SimCount;
    QMap<int, Timei> IMEI;
    tCPU cpu;
    tMemory Memory;
    QMap<int, tDisplay> DispleyHW;
    tCameraHW cameraHW;
} tHardWare;

typedef struct DevDiskSpaceInt  {
    double dfSize;
    double dfUsed;
    double dfFree;
} TDiskSpaceInt;

typedef struct DevSummaryInfo {
    QString DevNumber;
    QString DevPath;
    QString DevStatus;
    bool adbMode;
    bool overTCP;
    bool unicDevNumber;
    TBatteryInfo DevBattery;
    QMap<QString, TDiskSpace> DiskSpace;
    QMap<QString, TNetIf> netIF;
    TGSmInfo Gsm;
    TSysInfo DevInfo;
    TDevFB DevFBInfo;
    tHardWare HWInfo;
    //QMap<QString, tAPK> APKList;
    tAPKlist APKListInfo;
    QString Sreenshot;
    QString TempPath;
} TSummaryInfo;

typedef struct v_LastFM {
    bool isDevice;
    QString curPath;
    QString curDir;
} tLastFM;

typedef struct DevAcctionStatistic {

    bool activeLTable;
    QMap<bool, tFM> FileM;
    QMap<bool, tLastFM> lastPath;
    bool bP_FM1_Vis;
    int pB_FM1_Value;
    QString tmpDir;
    bool existScreenShot;
} TDevAction;

typedef struct vAndroidCount
{
    int allCount;
    int deviceCount;
    int emulatorCount;
    int fastbootCount;
    int offlineCount;
    int recoveryCount;
    int tcpCount;
    int unauthorizedCount;
    int unknownCount;
    int usbCount;
} tAndroidCount;

typedef struct VedroIconSet {
    QIcon iFastBoot;
    QIcon iNormal;
    QIcon iPreload;
    QIcon iRecovery;
    QIcon iSystem;
    QIcon iUSB;
    QIcon iWIFI;
} tAndroidIcon;

typedef struct WorkMap {
    int Acction;
    QString devNum;
    QString devPath;
    bool unicNum;
    int index;
    QStringList param;
    QStringList workObjects;
} TWorkMap;

typedef struct FItemPixmapWidget {
    QList<QLabel*> filesLabesPointers;
} tFileItemPixmap;

#endif // STRUCTURES_MAINWINDOW
