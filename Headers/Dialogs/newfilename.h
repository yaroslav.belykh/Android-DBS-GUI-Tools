/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef NEWFILENAME_H
#define NEWFILENAME_H

#include <QDialog>
#include <QAbstractButton>
#include <QMessageBox>

namespace Ui {
class newFileName;
}

class newFileName : public QDialog
{
    Q_OBJECT

public:
    explicit newFileName(QWidget *parent = 0);
    ~newFileName();

    void getInputParams(int, QString, QString, int, bool);
    //QString deniedSymbols(QString);
    void setFileNameString(void);

public slots:

signals:
    void sendFileNameString(int, int, bool, QString, QString);

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);

    void on_lineEdit_textEdited(const QString &arg1);

private:
    Ui::newFileName *ui;

    int returnAcction;
    QString nFileName;
    QString oFileName;
    int index;
    bool lTab;
    QStringList deniedSymbolsList;
};

#endif // NEWFILENAME_H
