/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef GETINFO_H
#define GETINFO_H

#include <QObject>
#include <QString>
#include <QMessageBox>
#include "Headers/Main/structures.h"
#include "Headers/Base/adbprocess.h"
#include "Headers/Additional/basefunction.h"

class getInfo : public QObject
{
    Q_OBJECT
public:
    //explicit getInfo(QObject *parent = 0);
    getInfo(tCMD*, adbprocess*, basefunction*, QObject *parent = 0);

    void getNameDevSysInfo(TSummaryInfo*);
    void getBatteryInfo(TSummaryInfo*);
    void getDiskSpaceInfo(TSummaryInfo*);
    void getNetCfgInfo(TSummaryInfo*);
    void GetDisplayInfo(TSummaryInfo*);
    void GetCameraInfo(TSummaryInfo*);

    void switchWifiOnOff(TSummaryInfo*, QStringList);

signals:

public slots:

private:
    adbprocess* appProcess;
    basefunction *BaseFunction;
    tCMD* appCmd;


    QString splitNeedInfoFromRawInfo(QStringList, QString, QString splitStr = " ", QString OtSplitStr = "=");
    inline int getPowerCoonectionInfo(QString);
    inline int splitMemInfo(QStringList, QString);
    TDiskSpace splitDFLine(QStringList, QString, bool sdEx = true, QString DevPath = "");
    inline void splitIfInfo(QStringList, QString, QString, int, TSummaryInfo*);

    inline QStringList getDevSysInfoRaw(TSummaryInfo*);
    inline QStringList getBatteryInfoRaw(TSummaryInfo*);
    inline QStringList getBatteryInfoCRaw(TSummaryInfo*);
    inline QStringList getDiskSpaceInfoRaw(TSummaryInfo*);
    inline QStringList getNetCfgInfoRaw(TSummaryInfo*);
    inline QStringList getInfobyCat(TSummaryInfo*, QString);
    inline QStringList getGSMModulesServicesInfo(TSummaryInfo*, QString, int);

    inline QString getGSMNetworkType(int);

};

#endif // GETINFO_H
