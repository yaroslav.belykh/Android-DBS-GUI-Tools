/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef STRUCTURES
#define STRUCTURES

#include <QStringList>

typedef struct adb_st
{
    QStringList amAcction;
    QStringList backup;
    QStringList connectADB;
    QStringList cat;
    QStringList copy;
    QStringList devices;
    QStringList diskspace;
    QStringList diskUsage;
    QStringList dumpsys;
    QStringList fdisk;
    QStringList getState;
    QStringList instAPK;
    QStringList keyevent;
    QStringList listDir;
    QStringList listDirL;
    QStringList lsAPK;
    QStringList mkdir;
    QStringList mount;
    QStringList mv;
    QStringList netcfg;
    QStringList pm;
    QStringList pull;
    QStringList push;
    QStringList reboot;
    QStringList rebootBootloader;
    QStringList rebootRecovery;
    QStringList rm;
    QStringList restore;
    QStringList root;
    QStringList screenshot;
    QStringList shellCMD;
    QStringList shutdown;
    QStringList smsSender;
    QStringList svc;
    QStringList sysinfo;
    QStringList sync;
    QStringList tcpip;
    QStringList uninstAPK;
    QStringList unroot;
    QStringList usb;
    QStringList start;
    QStringList stop;
    QStringList version;
    QStringList help;
} Tadb;

typedef struct fb_st
{
    QStringList devices;
    QStringList update;
    QStringList flashAll;
    QStringList flash;
    QStringList reboot;
    QStringList rebootBootloader;
    QStringList getvar;
    QStringList help;
} Tadbfb;

typedef struct app_st
{
    QString adb;
    QString fastboot;
    QString aapt;
} tAdbApp;

typedef struct cmd_st
{
    Tadb adb;
    Tadbfb fastboot;

}tCMD;

typedef struct vDebug
{
    bool adbDebug;
    bool classDebug;
} tDebug;

#endif // STRUCTURES

