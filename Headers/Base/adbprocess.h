/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef ADBPROCESS_H
#define ADBPROCESS_H

#include <QObject>
#include <QProcess>
#include <QFileDialog>
#include <QMessageBox>
#include <QList>

#include "Headers/Base/structures.h"
#include "Headers/Additional/basefunction.h"

#if not defined(Q_OS_WIN)
#include <QDebug>
#endif

class adbprocess : public QObject
{
    Q_OBJECT
public:
    explicit adbprocess(QObject *parent = 0);
    ~adbprocess();

    QStringList adbCMD(QStringList, QStringList, QString DevNumber = "", QString DevPath = "", bool unicDevNumber = true, bool printDebugVal = true);
    QStringList fbCMD(QStringList, QStringList, QString DevNumber = "", QString DevPath = "", bool unicDevNumber = true, bool printDebugVal = true);

    void setCMDPointer(tCMD*, tAdbApp*, bool printDebug = false, QString ParName = "Main");

signals:

public slots:

private:

    tCMD *procCmd;
    tAdbApp *adbApp;

    QProcess *adbProc;

    QString parentName;

    QString StrList2Str(QStringList);
    bool isDebug;
};

#endif // ADBPROCESS_H
