/*********************************************************************
 Copyright 2015 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef BASEFUNCTION_H
#define BASEFUNCTION_H

#include <QObject>
#include <QList>
#include <QStringList>
#include <QtCore/qmath.h>
#include <QVariant>
#include <QTime>
#include <QAbstractEventDispatcher>
#include <QCoreApplication>
#include <QDir>
#include <QByteArray>
#include <QBitArray>

class basefunction : public QObject
{
    Q_OBJECT
public:
    explicit basefunction(QObject *parent = 0);
    ~basefunction();

    QStringList grep(QStringList, QString, bool xst = true);
    QString awk(QString, QString, int);
    QString StrList2Str(QStringList, QString sep = " ", QString Scrng = "");
    QString makeSeparatorString(QString, QString separSymblol = "#", int countLine = 40);
    QString firstSymbolToUpper(QString);
    QString AllFirstSymbolToUpper(QString, QString);
    QString roundString(QString, QString, int roundPosit = 3);
    double roundTo(double, int);
    void deley(int);

    QString netMaskValue(int);

    QBitArray bytesToBits(const QByteArray &bytes);
    QByteArray bitsToBytes(const QBitArray &bits);

signals:

public slots:
};

#endif // BASEFUNCTION_H
