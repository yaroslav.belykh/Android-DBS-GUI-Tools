/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef APK_H
#define APK_H

#include <QStringList>

typedef struct InfoAllAPK {
    QString codePath;
    QString resourcePath;
    QString nativeLibraryPath;
    QString dataPath;
    QString APKversion;
    bool odex;
    int SizeAPK;
    int SizeAPKlib;
    int sizeData;
    int sizeSDCardData;
} TInfoAPK;

typedef struct lsAllAPK {
    bool isSystem;
    QString codePath;
    QString Devices;
    QString UID;
    QString dataPath;
    QString APKversion;
    int UsedSize;
    int HiddenSize;
    bool isEnable;
    bool isUninstall;
    QString ProgramName;
    QString IconPath;
} tAPK;

typedef struct v_APKList {
    int usedHiddenPacckahesSpace;
    int TotallyPackagesSpace;
    int curVpbalue;
    int UpdSystem;
    bool inInfoAPKUpdate;
    int APKCount;
    int APKInstallCount;
    bool intOK;
} tAPKlist;

typedef struct vApkModel
{
    QString apkName;
    QString visibleName;
    QString apkIcon;
    tAPK apkInfo;
} tAPKModel;

typedef struct vApkListSort
{
    int aDevice;
    int aType;
    int aStatus;
} tApkListSort;



#endif // APK_H
