/*********************************************************************
 Copyright 2016 Yaroslav Belykh (aka Yamah)

 This file is part of Android Developer (Bridge) Studio GUI Tools

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef STRUCTURE_H
#define STRUCTURE_H

//#include <QMainWindow>
#include <QMap>
#include <QMetaType>
#include <QTime>
#include <QLabel>
#include <QUuid>
#include <QDesktopServices>
#include <QStringList>

#include <QDebug>

typedef struct v_fileType {
    bool exists;
    bool isHidden;
    bool isDir;
    bool isFile;
    bool isReadable;
    bool isWritable;
    bool isExecutable;
    bool isSymLink;
} tFileType;


typedef struct FM_FileItem{
    QString nameFItem;
    QString canonicalNameFItem;
    tFileType typeFItem;
    qint64 sizeFItem;
    QString timeFItem;
    QString extentionFItem;
    int mType;
}tFMLItem;

typedef struct v_FM{
    bool isDevice;
    QString curPath;
    //QString curDir;
    QMap<QString, tFMLItem> FList;
    QStringList fileItemSelection;
    QString LastFItem;
    bool isRefresh;
}tFM;

typedef struct VWIFI {
    int id;
    QString SSID;
    int Prio;
} twifi;

typedef struct vWIFIscan {
    QString BSSID;
    QString SSID;
    QString Freq;
    QString RSSI;
    QString Flags;
} tWIFIScan;

#endif // STRUCTURE_H

